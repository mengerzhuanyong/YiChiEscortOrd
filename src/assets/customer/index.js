/**
 * 悬赏任务平台 - 系统资源 - 用户自定义
 * http://menger.me
 * @大梦
 */

'use strict';

export const CustomerSources = {

    /**
     * @Icons
     */
    icon_win: require('./icon/icon_win.png'),
    icon_time_logo1: require('./icon/icon_time_logo1.png'),
    icon_notice_title: require('./icon/icon_notice_title.png'),
    icon_plus: require('./icon/icon_plus.png'),
    icon_notice_bell: require('./icon/icon_notice_bell.png'),
    icon_notice_horn: require('./icon/icon_notice_horn.png'),

    icon_nav_index1: require('./icon/icon_nav_index1.png'),
    icon_nav_index2: require('./icon/icon_nav_index2.png'),
    icon_nav_index3: require('./icon/icon_nav_index3.png'),
    icon_nav_index4: require('./icon/icon_nav_index4.png'),

    icon_nav_order1: require('./icon/icon_nav_order1.png'),
    icon_nav_order2: require('./icon/icon_nav_order2.png'),
    icon_nav_order3: require('./icon/icon_nav_order3.png'),
    icon_nav_order4: require('./icon/icon_nav_order4.png'),
    icon_nav_order5: require('./icon/icon_nav_order5.png'),
    
    icon_grid: require('./icon/icon_grid.png'),
    icon_text: require('./icon/icon_text.png'),
    icon_rank: require('./icon/icon_rank.png'),

    icon_user_vip: require('./icon/icon_user_vip.png'),
    icon_user_car: require('./icon/icon_user_car.png'),
    icon_user_wallet: require('./icon/icon_user_wallet.png'),
    icon_user_adviser: require('./icon/icon_user_adviser.png'),
    icon_user_customer: require('./icon/icon_user_customer.png'),
    icon_user_profile: require('./icon/icon_user_profile.png'),
    icon_user_setting: require('./icon/icon_user_setting.png'),
    icon_arrow_logo1: require('./icon/icon_arrow_logo1.png'),
    icon_arrow_logo2: require('./icon/icon_arrow_logo2.png'),
    icon_tel_logo1: require('./icon/icon_tel_logo1.png'),

    icon_warring_logo1: require('./icon/icon_warring_logo1.png'),
    icon_password_logo1: require('./icon/icon_password_logo1.png'),
    icon_don_not_have_car: require('./icon/icon_don_not_have_car.png'),
    icon_yc_logo1: require('./icon/icon_yc_logo1.png'),
    icon_choice_car: require('./icon/icon_choice_car.png'),
    icon_time_card_logo1: require('./icon/icon_time_card_logo1.png'),
    icon_time_card_logo2: require('./icon/icon_time_card_logo2.png'),
    icon_time_card_logo3: require('./icon/icon_time_card_logo3.png'),

    icon_login_icon1: require('./icon/icon_login_icon1.png'),
    icon_login_icon2: require('./icon/icon_login_icon2.png'),
    icon_login_icon3: require('./icon/icon_login_icon3.png'),

    icon_car_type_logo1: require('./icon/icon_car_type_logo1.png'),
    icon_car_type_logo2: require('./icon/icon_car_type_logo2.png'),
    icon_car_type_logo3: require('./icon/icon_car_type_logo3.png'),

    icon_driver_exit: require('./icon/icon_driver_exit.png'),
    icon_driver_map: require('./icon/icon_driver_map.png'),
    icon_user_logo: require('./icon/icon_user_logo.png'),
    icon_company_logo: require('./icon/icon_company_logo.png'),

    icon_level_logo1: require('./icon/icon_level_logo1.png'),
    icon_level_logo2: require('./icon/icon_level_logo2.png'),
    icon_level_logo3: require('./icon/icon_level_logo3.png'),
    icon_level_logo4: require('./icon/icon_level_logo4.png'),
    icon_level_logo5: require('./icon/icon_level_logo5.png'),
    icon_level_text_bg: require('./icon/icon_level_text_bg.png'),
    icon_up_img: require('./icon/icon_up_img.png'),
    
    icon_marker1: require('./icon/icon_marker1.png'),
    icon_marker2: require('./icon/icon_marker2.png'),
    icon_marker3: require('./icon/icon_marker3.png'),
    icon_marker4: require('./icon/icon_marker4.png'),
    icon_marker5: require('./icon/icon_marker5.png'),
    icon_marker6: require('./icon/icon_marker6.png'),
    icon_marker7: require('./icon/icon_marker7.png'),
    icon_marker8: require('./icon/icon_marker8.png'),

    /**
     * @Images
     */
    img_banner: require('./img/img_banner.png'),
    img_logo_index: require('./img/img_logo_index.png'),
    img_bg_block_content: require('./img/img_bg_block_content.png'),
    img_login_bg: require('./img/img_login_bg.png'),
    img_driver_card_bg: require('./img/img_driver_card_bg.png'),
    img_wallet_bg: require('./img/img_wallet_bg.png'),
    img_price_background: require('./img/img_price_background.png'),
    img_withdraw_bg: require('./img/img_withdraw_bg.png'),
    img_nopicture1: require('./img/img_nopicture1.png'),
    img_vip_bg1: require('./img/img_vip_bg1.png'),
    img_vip_bg2: require('./img/img_vip_bg2.png'),

    img_uploadimg1: require('./img/img_uploadimg1.png'),
    img_uploadimg2: require('./img/img_uploadimg2.png'),
    img_uploadimg3: require('./img/img_uploadimg3.png'),
    img_uploadimg4: require('./img/img_uploadimg4.png'),
    img_uploadimg5: require('./img/img_uploadimg5.png'),
    img_uploadimg6: require('./img/img_uploadimg6.png'),
    img_uploadimg7: require('./img/img_uploadimg7.png'),
    img_uploadimg8: require('./img/img_uploadimg8.png'),
};