/**
 * 悬赏任务平台 - 系统资源 - 系统默认
 * http://menger.me
 * @大梦
 */

'use strict';

export const DefaultSources = {
    /**
     * @JSON [动画的JSON文件]
     */
    json_loading: require('./json/loading.json'),
    json_cycle_animation: require('./json/cycle_animation.json'),
    json_list_header_loading: require('./json/list_header_loading.json'),
    /**
     * @Common [通用]
     */
    img_no_nerwork: require('./img/img_no_nerwork.png'),
    img_no_record: require('./img/img_no_record.png'),
    icon_nav_left: require('./icon/icon_nav_left.png'),
    icon_arrow_right: require('./icon/icon_arrow_right.png'),
    icon_alipay_logo: require('./icon/icon_alipay_logo.png'),
    icon_wechat_logo: require('./icon/icon_wechat_logo.png'),
    icon_result_fail: require('./icon/icon_result_fail.png'),
    /**
     * @Toast [轻提示]
     */
    icon_toast_fail: require('./icon/icon_toast_fail.png'),
    icon_toast_loading: require('./icon/icon_toast_loading.png'),
    icon_toast_success: require('./icon/icon_toast_success.png'),
    icon_toast_warn: require('./icon/icon_toast_warn.png'),
    /**
     * @KeyBoard [数字键盘]
     */
    icon_keyBoard_del: require('./icon/icon_keyBoard_del.png'),
    icon_keyBoard_left: require('./icon/icon_keyBoard_left.png'),

    /**
     * @Share [分享]
     */
    icon_pay_wechat: require('./icon/icon_pay_wechat.png'),
    icon_pay_alipay: require('./icon/icon_pay_alipay.png'),
    icon_share_we_chat: require('./icon/icon_share_we_chat.png'),
    icon_share_time_lime: require('./icon/icon_share_time_lime.png'),
    icon_share_link: require('./icon/icon_share_link.png'),

    /**
     * @CHAT [聊天组件]
     */
};