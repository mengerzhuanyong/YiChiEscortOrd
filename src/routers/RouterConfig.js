'use strict';
import {
    createStackNavigator
} from 'react-navigation-stack';
import {
    createBottomTabNavigator
} from 'react-navigation-tabs';
import {
    tabOptions,
    transitionConfig
} from './RouterTool';
import {
    TabBottomBar
} from '../components';

// Demo
import {
    DemoBaiduFace,
    DemoAlert,
    DemoButton,
    DemoContainer,
    DemoImageView,
    DemoRow,
    DemoList,
    DemoOverlay,
    DemoPicker,
    DemoToast,
    DemoTheme,
    Example,
    DemoSegmented,
    DemoCard,
    DemoWebBrowser,
    DemoPopover,
    DemoPermissions,
    DemoForm,
    DemoUpload,
    DemoCityPicker,
    DemoCityPicker2,
    DemoTimePicker,
} from '../modules';

// 公共 - 城市选择
import PickerCity from '../page/common/PickerCity';
// 公共 - webpage
import WebPage from '../page/webview/WebPage';
// 公共 - 搜索
import Search from '../page/common/Search';
// 协议
import Agreement from '../page/login/Agreement';

/** 登录 */
// 登录
import Login from '../page/login/Login';
// 注册
import Register from '../page/login/Register';
// 忘记密码
import Forget from '../page/login/Forget';

/** 首页 */
// 首页
import Home from '../page/home/Home';
// 运费测算
import Calculation from '../page/home/Calculation';

/** 货源 */
// 货源大厅
import Supply from '../page/supply/Supply';
// 货源大厅 - 筛选
import SupplyFilter from '../page/supply/SupplyFilter';
// 货源详情
import SupplyDetail from '../page/supply/SupplyDetail';
// 申请接单
import SupplyApply from '../page/supply/SupplyApply';
// 申请接单订单详情
import SupplyOrderDetail from '../page/supply/SupplyOrderDetail';
// 更多车辆
import MoreCars from '../page/supply/MoreCars';
// 动画
import Animated from '../page/supply/Animated';

/** 资讯 */
// 行情资讯
import News from '../page/news/News';
// 资讯详情
import NewsDetail from '../page/news/NewsDetail';

/** 我的订单 */
// 个人中心 - 我的订单（列表）
import Order from '../page/order/Order';
// 个人中心 - 订单详情
import OrderDetail from '../page/order/OrderDetail';
// 个人中心 - 订单详情 - 订单时间轴
import OrderTimeLine from '../page/order/OrderTimeLine';
// 个人中心 - 订单详情 - 更多车辆
import OrderMoreCars from '../page/order/OrderMoreCars';
// 删除车辆
import DeleteCar from '../page/order/DeleteCar';
// 更换车辆
import ChangeCar from '../page/order/ChangeCar';
// 电子运单
import ElectronicsOrder from '../page/order/ElectronicsOrder';
// 承运商看司机订单
import OrderDriverDetail from '../page/order/OrderDriverDetail';
// 运输中的承运商看司机订单
import OrderInTransitDetail from '../page/order/OrderInTransitDetail';

/** 司机 */
// 司机入口
import Driver from '../page/driver/Driver';
// 我的订单
import DriverOrder from '../page/driver/DriverOrder';
// 我的订单 - 订单详情
import DriverOrderDetail from '../page/driver/DriverOrderDetail';
// 过磅单上传
import UploadPic from '../page/driver/UploadPic';
// 电子运单
import ElectronicsDriverOrder from '../page/driver/ElectronicsDriverOrder';

/** 地图 */
// 地图
import MapComponent from '../page/map/MapComponent';
// 地图web
import MapComponentWeb from '../page/map/MapComponentWeb';
// 司机地图
import DriverMap from '../page/map/DriverMap';
// 司机地图web
import DriverMapWeb from '../page/map/DriverMapWeb';

/** 我的 */
// 个人中心
import Mine from '../page/mine/Mine';
// 关于我们
import AboutUs from '../page/mine/AboutUs';
// 支付密码
import PayPassword from '../page/mine/PayPassword';
// 修改个人信息
import ChangeUserInfo from '../page/mine/ChangeUserInfo';
// 修改密码验证
import ChangePassword from '../page/mine/ChangePassword';
// 修改密码
import ChangePayPassword from '../page/mine/ChangePayPassword';
// 车辆管理
import ManageCar from '../page/manageCar/ManageCar';
// 修改车辆
import EditCar from '../page/manageCar/EditCar';
// 钱包
import Wallet from '../page/wallet/Wallet';
// 充值
import Recharge from '../page/wallet/Recharge';
// 提现
import Withdraw from '../page/wallet/Withdraw';
// 会员中心
import Vip from '../page/vip/Vip';
// 会员等级
import Level from '../page/vip/Level';
// 分享订单
import ShareOrder from '../page/vip/ShareOrder';

/** 认证 */
// 供货商认证
import ApproveChoice from '../page/approve/ApproveChoice';
import ApproveCompany from '../page/approve/ApproveCompany';
import ApproveUser from '../page/approve/ApproveUser';

/** 系统 */
// 检查更新
import CheckUpdates from '../page/system/CheckUpdates';
// 我的消息
import SystemMessage from '../page/system/SystemMessage'; 
// 设置
import Setting from '../page/system/Setting';

const TabNavigatorRouter = {
    Home: {screen: Home,navigationOptions: tabOptions({
            title: '首页',
            normalIcon: Images.icon_tabbar_home,
            selectedIcon: Images.icon_tabbar_home_cur
        }),
    },
    Supply: {screen: Supply,navigationOptions: tabOptions({
            title: '货源大厅',
            normalIcon: Images.icon_tabbar_supply,
            selectedIcon: Images.icon_tabbar_supply_cur
        }),
    },
    Driver: {screen: Driver,navigationOptions: tabOptions({
            title: '司机入口',
            normalIcon: Images.icon_tabbar_driver,
            selectedIcon: Images.icon_tabbar_driver_cur,
        }),
    },
    News: {screen: News,navigationOptions: tabOptions({
            title: '行情资讯',
            normalIcon: Images.icon_tabbar_news,
            selectedIcon: Images.icon_tabbar_news_cur
        }),
    },
    Mine: {screen: Mine,navigationOptions: tabOptions({
            title: '我的',
            normalIcon: Images.icon_tabbar_mine,
            selectedIcon: Images.icon_tabbar_mine_cur
        }),
    },
};

const TabNavigatorConfig = {
    initialRouteName: 'Home',
    tabBarOptions: {
        showIcon: true,
        indicatorStyle: {
            height: 0
        },
        inactiveTintColor: '#999',
        activeTintColor: Predefine.themeColor,
        style: {
            backgroundColor: '#fff',
        },
        tabStyle: {
            margin: 2,
        },
        keyboardHidesTabBar: false,
    },
    lazy: true, //懒加载
    tabBarComponent: TabBottomBar,
};

const TabNavigator = createBottomTabNavigator(
    TabNavigatorRouter,
    TabNavigatorConfig,
);

const StackNavigatorRouter = {
    // Demo
    DemoBaiduFace: {screen: DemoBaiduFace},
    DemoAlert: {screen: DemoAlert},
    DemoButton: {screen: DemoButton},
    DemoContainer: {screen: DemoContainer},
    DemoImageView: {screen: DemoImageView},
    DemoRow: {screen: DemoRow},
    DemoList: {screen: DemoList},
    DemoOverlay: {screen: DemoOverlay},
    DemoPicker: {screen: DemoPicker},
    DemoToast: {screen: DemoToast},
    DemoTheme: {screen: DemoTheme},
    Example: {screen: Example},
    DemoSegmented: {screen: DemoSegmented},
    DemoCard: {screen: DemoCard},
    DemoWebBrowser: {screen: DemoWebBrowser},
    DemoPopover: {screen: DemoPopover},
    DemoPermissions: {screen: DemoPermissions},
    DemoForm: {screen: DemoForm},
    DemoUpload: {screen: DemoUpload},
    DemoCityPicker: {screen: DemoCityPicker},
    DemoCityPicker2: {screen: DemoCityPicker2},
    DemoTimePicker: {screen: DemoTimePicker},

    // 公共
    Tab: {screen: TabNavigator},
    WebPage: {screen: WebPage},
    PickerCity: {screen: PickerCity},
    Search: {screen: Search},
    Agreement: {screen: Agreement},

    // login
    Login: {screen: Login},
    Register: {screen: Register},
    Forget: {screen: Forget},
    ApproveChoice: {screen: ApproveChoice},
    ApproveCompany: {screen: ApproveCompany},
    ApproveUser: {screen: ApproveUser},
    
    // Home
    Home: {screen: Home},
    Calculation: {screen: Calculation},

    // Supply
    Supply: {screen: Supply},
    SupplyFilter: {screen: SupplyFilter},
    SupplyDetail: {screen: SupplyDetail},
    SupplyApply: {screen: SupplyApply},
    SupplyOrderDetail: {screen: SupplyOrderDetail},
    MoreCars: {screen: MoreCars},
    Animated: {screen: Animated},

    //News
    News: {screen: News},
    NewsDetail: {screen: NewsDetail},

    //Order
    Order: {screen: Order},
    OrderDetail: {screen: OrderDetail},
    OrderTimeLine: {screen: OrderTimeLine},
    OrderMoreCars: {screen: OrderMoreCars},
    DeleteCar: {screen: DeleteCar},
    ChangeCar: {screen: ChangeCar},

    // Driver
    Driver: {screen: Driver},
    DriverOrder: {screen: DriverOrder},
    DriverOrderDetail: {screen: DriverOrderDetail},
    ElectronicsOrder: {screen: ElectronicsOrder},
    OrderDriverDetail: {screen: OrderDriverDetail},
    OrderInTransitDetail: {screen: OrderInTransitDetail},
    UploadPic: {screen: UploadPic},
    ElectronicsDriverOrder: {screen: ElectronicsDriverOrder},

    // Map
    MapComponent: {screen: MapComponent},
    MapComponentWeb: {screen: MapComponentWeb},
    DriverMap: {screen: DriverMap},
    DriverMapWeb: {screen: DriverMapWeb},

    // Mine
    Mine: {screen: Mine},
    AboutUs: {screen: AboutUs},
    PayPassword: {screen: PayPassword},
    ChangeUserInfo: {screen: ChangeUserInfo},
    ChangePassword: {screen: ChangePassword},
    ChangePayPassword: {screen: ChangePayPassword},
    ManageCar: {screen: ManageCar},
    EditCar: {screen: EditCar},
    Wallet: {screen: Wallet},
    Recharge: {screen: Recharge},
    Withdraw: {screen: Withdraw},
    ShareOrder: {screen: ShareOrder},
    Vip: {screen: Vip},
    Level: {screen: Level},

    // 检查更新
    CheckUpdates: {screen: CheckUpdates},
    // 系统消息
    SystemMessage: {screen: SystemMessage},
    // 设置
    Setting: {screen: Setting},
};

const StackNavigatorConfig = {
    initialRouteName: 'Tab',
    initialRouteParams: {},
    cardStyle: {},
    cardShadowEnabled: true,
    cardOverlayEnabled: true,
    transitionConfig: transitionConfig,
    defaultNavigationOptions: {
        header: null,
        gesturesEnabled: true,
    },
    // react-navigation-stack@alpha
    // defaultNavigationOptions: ({ navigation }) => {
    //   return {
    //     cardStyle: {},
    //     cardShadowEnabled: true,
    //     cardOverlayEnabled: true,
    //     headerShown: false,
    //     animationEnabled: true,
    //     gestureEnabled: true,
    //     gestureResponseDistance: {
    //       horizontal: 25,
    //       vertical: 135,
    //     },
    //     ...transitionConfig(navigation),
    //   };
    // },
};

const StackNavigator = createStackNavigator(
    StackNavigatorRouter,
    StackNavigatorConfig,
);

export {StackNavigator};
