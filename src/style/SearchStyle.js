/**
 * 易驰护运 - SearStyle
 * http://menger.me
 * @大梦
 */

'use strict';

export default {
    container: {
        flex: 1,
    },

    // Content
    content: {
        flex: 1,
    },
    searchView: {
        height: 30,
        marginLeft: 5,
        marginRight: 10,
        borderRadius: 15,
        paddingHorizontal: 5,
        backgroundColor: '#fff'
    },
    searchIconStyle: {
        fontSize: 16,
        color: '#999',
        marginHorizontal: 5,
    },
    searchInputStyle: {
        flex: 1,
        paddingVertical: 0,
    },
    searchInputBtnStyle: {
        height: 30,
        paddingVertical: 0,
        paddingHorizontal: 0,
        backgroundColor: 'transparent',
    },
    searchBtnItem: {},
    searchBtnItemTitle: {
        fontSize: 14,
        color: '#fff',
    },

    searchHotCityView: {
    },
    contentTitleView: {
        marginTop: 30,
        marginBottom: 10,
        paddingHorizontal: 15,
    },
    contentTitleIcon: {
        fontSize: 16,
        color: '#333',
        marginRight: 5,
    },
    contentTitleStyle: {
        fontSize: 14,
        color: '#333',
    },
    searchHotCityCon: {
        paddingHorizontal: 10,
    },
    hotCityItem: {
        height: 25,
        minWidth: 70,
        borderRadius: 25,
        marginBottom: 10,
        marginHorizontal: 5,
        paddingHorizontal: 15,
        backgroundColor: '#ebebeb',
    },
    hotCityItemTitle: {
        fontSize: 13,
        color: '#666',
    },

}