/**
 * 易驰护运 - MineStyle
 * http://menger.me
 * @大梦
 */

'use strict';

export default {
    container: {
        flex: 1,
    },

    // Content
    content: {
        flex: 1,
    },

    userInfoView: {
        paddingTop: 20,
        paddingBottom: 60,
        paddingHorizontal: 15,
        backgroundColor: Predefine.themeColor,
    },
    userAvatarView: {},
    userAvatarStyle: {
        width: 76,
        height: 76,
        borderRadius: 38,
        backgroundColor: '#fff',
    },
    userLevelStyle: {
        height: 22,
        minWidth: 70,
        marginTop: -10,
        borderRadius: 11,
        // paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: 'rgba(255,255,255,.8)',
    },
    userLevelTitleStyle: {
        fontSize: 13,
        color: Predefine.themeColor,
    },
    userInfoDetailView: {
        flex: 1,
        marginLeft: 15,
    },
    userInfoDetailLeftView: {
        flex: 1,
    },
    userNameStyle: {
        fontSize: 18,
        color: '#fff',
        fontWeight: '700',
    },
    companyInfo: {
        marginTop: 20,
    },
    companyNameStyle: {
        fontSize: 14,
        color: '#fff',
    },
    userInfoBtnStyle: {
        paddingVertical: 2,
        paddingHorizontal: 8,
        backgroundColor: 'rgba(255,255,255,.8)',
    },
    userInfoBtnTitleStyle: {
        fontSize: 13,
        color: Predefine.orangeColor,
    },
    userInfoArrowIconStyle: {
        width: 22,
        height: 22,
        tintColor: '#fff',
    },
    navContentStyle: {
        marginBottom: 20,
    },
    orderNavContentStyle: {
        marginTop: -40,
        borderRadius: 8,
        paddingVertical: 10,
        marginHorizontal: 15,
        backgroundColor: '#fff',
    },
    orderNavContentTitleView: {
    },
    orderNavContentTitleConStyle: {
        paddingVertical: 5,
    },
    orderNavContentTitleStyle: {
        fontSize: 16,
        color: Predefine.themeColor,
    },
    orderNavContentTitleDetailStyle: {

    },
    orderNavBtnItem: {
        flex: 1,
        paddingHorizontal: 0,
        backgroundColor: 'transparent',
    },
    orderNavBtnItemIcon: {
        width: 30,
        height: 30,
        marginBottom: 5,
    },
    orderNavBtnItemTitle: {
        fontSize: 12,
        color: '#333',
    },
    orderNavBtnItemBadge: {
        top: 10,
        right: 10,
        padding: 0,
        position: 'absolute',
    },
    navItemStyle: {},
    navItemIconStyle: {
        width: 20,
        height: 20,
    },
    navItemTitleStyle: {

    },
}