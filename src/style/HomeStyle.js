/**
 * 易驰护运 - HomeStyle
 * http://menger.me
 * @大梦
 */

'use strict';

export default {
    container: {
        flex: 1,
    },

    // NavigationBar
    navigationBarStyle: {},
    navBarLeftAction: {},
    indexLogoStyle: {
        width: 100,
        height: 30,
    },
    navBtnItemStyle: {
        marginTop: 2,
        paddingVertical: 0,
        paddingHorizontal: 0,
        backgroundColor: 'transparent'
    },
    navBtnIconStyle: {
        fontSize: 15,
        color: '#fff',
        marginLeft: 2,
    },
    navBtnTitleStyle: {
        fontWeight: '700',
    },


    // Content
    content: {
        flex: 1,
    },
    listHeaderContent: {

    },
    listHeaderItemContent: {
        marginTop: 10,
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        justifyContent: 'space-between',
    },
    itemContentIconStyle: {
        width: 4,
        height: 20,
        backgroundColor: Predefine.themeColor,
    },
    itemContentTitleStyle: {
        fontSize: 16,
        color: '#333',
        marginLeft: 10,
        fontWeight: '700',
    },

    todatDataItemView: {
        flex: 1,
    },
    verticalSeparatorStyle: {
        width: 1,
        height: 25,
        backgroundColor: Predefine.themeColor,
    },
    todatDataItemContent: {
        flex: 1,
    },
    todatDataItemValue: {
        fontSize: 15,
        fontWeight: '700',
        color: Predefine.themeColor,
    },
    todatDataItemTitle: {
        fontSize: 14,
        marginTop: 5,
        color: '#333',
    },
    navigateItemStyle: {
        // flex: 1,
        width: '25%',
        paddingVertical: 0,
        paddingHorizontal: 0,
        backgroundColor: 'transparent',
    },
    navigateIconStyle: {
        tintColor: null,
    },
    navigateTitleStyle: {
        fontSize: 14,
        color: '#333',
    },

    hotNewsContent: {
        backgroundColor: '#f00'
    },

    separatorStyle: {
        backgroundColor: '#ddd',
    },
}