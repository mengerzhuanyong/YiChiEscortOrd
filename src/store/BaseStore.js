/**
 * 悬赏任务平台 - BaseStore
 * http://menger.me
 * @大梦
 */

'use strict';

import { action, observable, configure, runInAction, toJS } from 'mobx'

configure({ enforceActions: 'observed' });

export default class BaseStore {

    constructor(params) {
        this.userInfo = {};
        this.oldToken = '';
        this.oldUserInfo = {};
        this.loading = false;
        this.error = {
            isError: '',
            errorMsg: '',
        };
        this.currentIdentity = Constants.PERSONAL;
    }

    @observable error;
    @observable userInfo;
    @observable oldToken;
    @observable oldUserInfo;
    @observable loading = false;
    @observable currentIdentity;


    @action
    getJSDataSources(dataSources) {
        return toJS(dataSources);
    }

    @action
    getRequest = async (url, query) => {
        this.loading = true;
        const result = await Services.get(url, query);
        runInAction(() => {
            this.loading = false;
            if (result && result.code === StatusCode.TOKEN_EXPIRED_CODE) {
                ToastManager.message(result.msg);
                this.cleanUserInfo();
                return RouterHelper.reset('', 'Login');
            }
        });
        return result;
    };

    @action
    postRequest = async (url, data) => {
        this.loading = true;
        const result = await Services.post(url, data);
        runInAction(() => {
            this.loading = false;
            if (result && result.code === StatusCode.TOKEN_EXPIRED_CODE) {
                ToastManager.message(result.msg);
                this.cleanUserInfo();
                return RouterHelper.reset('', 'Login');
            }
        });
        return result;
    };

    @action
    changeCurrentIdentity = (identity) => {
        this.currentIdentity = identity;
    };

    @action // 存司机信息
    saveDriverInfo = (userInfo) => {
        // 先存储数据
        StorageManager.save(Constants.USER_INFO_OLD_KEY, this.userInfo);
        // 然后赋值
        this.userInfo = userInfo;
        global.token = userInfo.token;
        let result = StorageManager.save(Constants.USER_INFO_KEY, userInfo);
        let first_open = StorageManager.save(Constants.FIRST_OPEN, false);
    };

    @action // 清空司机信息还原为之前承运商的信息
    cleanDriverInfo = () => {
        let oldUserInfo = {};
        StorageManager.load(Constants.USER_INFO_OLD_KEY).then((response) => {
            if (response.code === StatusCode.SUCCESS_CODE) {
                oldUserInfo = response.data
                this.userInfo = oldUserInfo;
                global.token = response.data.token
                let result = StorageManager.save(Constants.USER_INFO_KEY, oldUserInfo);
            }
        });
    }

    @action // 存用户信息
    saveUserInfo = (userInfo) => {
        this.userInfo = userInfo;
        global.token = userInfo.token;
        // global.role = userInfo.role;
        let result = StorageManager.save(Constants.USER_INFO_KEY, userInfo);
        let first_open = StorageManager.save(Constants.FIRST_OPEN, false);
    };

    @action
    cleanUserInfo = () => {
        let obj = {};
        global.token = '';
        // global.role = '';
        this.userInfo = obj;
        let result = StorageManager.remove(Constants.USER_INFO_KEY);
    }

}