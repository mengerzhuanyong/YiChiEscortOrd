'use strict';

export default {
    /**
     * @全局的颜色
     */
    overallColor: '#333',
    themeColor: '#00bfcc',
    warnColor: '#fdbc2a',
    orangeColor: '#ff5100',
};