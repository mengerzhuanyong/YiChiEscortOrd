/**
 * 悬赏任务平台 - 系统常量 - 用户自定义
 * http://menger.me
 * @大梦
 */
const VERSION_DATA = require('../../../app.json');

'use strict';

export default {
    /**
     * @版本
     */
    APP_PLATFORM: 1,
    VERSION_CODE: VERSION_DATA.version_code,
    VERSION_NAME: VERSION_DATA.version_name,

    /**
     * @微信支付的appID
     */
    WECHAT_APPID: 'wx058f74c3a8a4c62c',

    /**
     * @阿里支付Scheme
     */
    ALIPAY_SCHME: 'alipay2021001150630403',

    /**
     * @首次打开APP
     */
    FIRST_OPEN: 'FIRSTOPEN',

    /**
     * @首次展示隐私政策
     */
    SHOW_PRIVACY_POLICY: 'SHOWPRIVACYPOLICY',
    
    /**
     * @存储用户信息
     */
    USER_INFO_KEY: 'USERINFOKEY',
    USER_INFO_OLD_KEY: 'USERINFOOLDKEY',
    
    // 注册成功
    REGISTER_SUCCESS: 'register_success',
    // 注册失败
    REGISTER_FAILED: 'register_failed',

    /**
     * @支付平台
     */
    // 支付宝
    PAY_ALIPAY: 1,
    PAY_ALIPAY_TEXT: 'alipay',
    // 微信
    PAY_WECHAT: 2,
    PAY_WECHAT_TEXT: 'weixin',
    // 平台币
    PAY_COIN: 3,
    PAY_COIN_NAME: '平台币',

    /**
     * @页面类型
     */
    // 普通页面
    PAGE_NORMAL: 'page_normal',
    // 编辑页面
    PAGE_EDIT: 'page_edit',
    // 管理页面
    PAGE_MANAGER: 'page_manager',

    /**
     * @验证码类型
     */
    // 注册
    CODE_REGISTER: 1,
    // 找回
    CODE_RECOVER: 2,
    // 修改
    CODE_MODIFY_PHONE: 3,

    // 系统公告
    SYS_NOTICE: 'SYS_NOTICE',
    // 系统消息
    SYS_MESSAGE: 'SYS_MESSAGE',

    // 承运商
    ALIAS_CARRIER: 'carrier_',
    // 司机
    ALIAS_DRIVER: 'driver_',

    // 待开始
    VEHICLE_ORDER_STATUS_START: 1,
    // 待装货
    VEHICLE_ORDER_STATUS_LOAD: 2,
    // 装货中
    VEHICLE_ORDER_STATUS_LOADING: 3,
    // 待送达
    VEHICLE_ORDER_STATUS_DELIVERY: 4,
    // 卸货中
    VEHICLE_ORDER_STATUS_UNLOAD: 5,
    // 已送达
    VEHICLE_ORDER_STATUS_END: 6,

    /**
     * @车辆管理Line状态
     */
    INPUT: 'input',
    CHOICE: 'choice',
    IMG: 'img',
    TIME: 'time',
    TITLE: 'title',
    NULL: 'null',
};