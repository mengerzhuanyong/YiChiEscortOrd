/**
 * 易驰护运 - NewsDetail
 * http://menger.me
 * 行情资讯
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard} from 'react-native'
import Webview from 'react-native-webview';
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class NewsDetail extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {

        };
    }

    render() {
        let {item} = this.params;
        return (
            <PageContainer style={styles.container}>
                <NavigationBar title={'行情资讯'}/>
                <Webview
                    startInLoadingState={true}
                    source={{uri: item.url}}
                    style={styles.webContainer}
                />
            </PageContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f2f2f2',
    },
});
