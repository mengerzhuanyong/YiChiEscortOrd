/**
 * 易驰护运 - News
 * http://menger.me
 * 行情资讯
 * @桓桓
 */

'use strict';

import React from 'react'
import {StyleSheet, Text, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard} from 'react-native'
import {
    ListView,
    BannerComponent,
} from '../../components'
import NewsItem from '../../components/customer/Item/NewsItem'
import LoadingHint from '../../components/default/Loading/LoadingHint'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class News extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            dataSource: [],
            banner_index: [],
        };
        this.page = 1;
        this.limit = 10;
    }

    componentDidMount() {
        this._onRefresh()
    };

    requestData = async () => { // 数据列表
        let {dataSource} = this.state;
        let url = ServicesApi.NEW_NEWS_LIST;
        let data = {
            page: this.page,
            limit: this.limit,
        };
        try {
            const result = await Services.post(url, data);
            let dataSourceTemp = dataSource.slice();
            let dataList = [];
            if (result.code === StatusCode.SUCCESS_CODE) {
                dataList = result.data.dataList;
                if (parseInt(data.page) === 1) {
                    dataSourceTemp = dataList
                } else {
                    if (dataList.length !== 0) {
                        dataSourceTemp = dataSourceTemp.concat(dataList);
                    }
                }
            }
            this.setState({
                dataSource: dataSourceTemp,
                loading: false
            });
            this._onStopLoading(dataList.length < this.limit);
        } catch (error) {
            this._onStopLoading(true);
        }
    };

    requestBannerData = async () => {
        let url = ServicesApi.NEWS_BANNER_LIST;
        let data = {};
        let result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({banner_index: result.data});
        }
    }

    _onStopLoading = (status) => {
        this.setState({ready: true});
        this._listRef && this._listRef.stopRefresh();
        this._listRef && this._listRef.stopEndReached({allLoad: status});
    };

    renderItem = ({item, index}) => {
        return <NewsItem item={item} {...this.props}/>
    };

    _captureRef = (v) => {
        this._listRef = v
    };

    _keyExtractor = (item, index) => {
        return `item_${index}`
    };

    _onRefresh = () => {
        this.page = 1;
        this.requestData()
        this.requestBannerData()
    };

    _onEndReached = () => {
        this.page++;
        this.requestData()
        this.requestBannerData()
    };

    renderItemSeparator = (info) => {
        return null;
    };

    _renderListHeaderComponent = (state) => {
        let {banner_index} = state;
        return (
            <View styles={styles.listHeaderContent}>
                <BannerComponent
                    autoplay={true}
                    autoplayTimeout={6}
                    data={banner_index}/>
            </View>
        );
    };

    render() {
        let {dataSource, loading} = this.state;
        return (
            <PageContainer style={styles.container} loading={loading}>
                <NavigationBar
                    title={'行情资讯'}
                    renderLeftAction={null}
                />
                {
                    loading == true
                        ?
                        <LoadingHint style={styles.loading} loading={loading}/>
                        :
                        <ListView
                            style={styles.listView}
                            initialRefresh={false}
                            data={dataSource}
                            ref={this._captureRef}
                            onRefresh={this._onRefresh}
                            renderItem={this.renderItem}
                            keyExtractor={this._keyExtractor}
                            onEndReached={this._onEndReached}
                            ItemSeparatorComponent={this.renderItemSeparator}
                            ListHeaderComponent={() => this._renderListHeaderComponent(this.state)}
                        />
                }
            </PageContainer>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f2f2f2',
    },
});
