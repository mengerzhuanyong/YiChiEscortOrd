/**
 * 易驰护运 - SystemMessage
 * http://menger.me
 * @大梦
 */

'use strict';

import React, {PureComponent} from 'react'
import {
    Text,
    View,
    CameraRoll,
    StyleSheet,
} from 'react-native'
import {ListView, SystemMessageItem} from '../../components'

export default class SystemMessage extends PureComponent {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            dataSources: [],
        };
        this.page = 1;
        this.limit = 10;
    }

    componentDidMount() {
        this._requestDataSources();
        this._onStopLoading();
    }

    componentWillUnmount() {
        this.timerArray = [this.stopLoadingTimer];
        ClearTimeOut(this.timerArray);
    }

    _requestDataSources = async () => {
        let {dataSources} = this.state;
        let url = ServicesApi.INDEX_MESSAGE_LIST;
        let data = {
            page: this.page,
            limit: this.limit,
        };
        try {
            const result = await Services.post(url, data);
            let dataSourceTemp = dataSources.slice();
            let dataList = [];
            if (result.code === StatusCode.SUCCESS_CODE) {
                dataList = result.data.dataList || [];
                if (parseInt(data.page) === 1) {
                    dataSourceTemp = dataList
                } else {
                    if (dataList.length !== 0) {
                        dataSourceTemp = dataSourceTemp.concat(dataList);
                    }
                }
            }
            this.setState({
                dataSources: dataSourceTemp,
                loading: false,
            });
            this._onStopLoading(dataList.length < this.limit);
        } catch (error) {
            this._onStopLoading(true);
        }
    };

    // 下拉刷新
    _onRefresh = () => {
        this.page = 1;
        this._requestDataSources();
    };

    // 结束加载
    _onStopLoading = (status = true) => {
        this.stopLoadingTimer = setTimeout(() => this.setState({ready: true}), 500);
        this._listRef && this._listRef.stopRefresh();
        this._listRef && this._listRef.stopEndReached({allLoad: status});
    };

    // 上拉加载
    _onEndReached = () => {
        this.page++;
        this._requestDataSources();
    }

    // 定义 refs
    _captureRef = (v) => {
        this._listRef = v
    };

    // 生成 Key
    _keyExtractor = (item, index) => {
        return `item_${index}`
    };

    _renderItem = ({item, index}) => {
        return (
            <SystemMessageItem
                item={item}
                callbackQureyMes={() => this.callbackQureyMes()}
                onPress={() => this.readMessage(item)}
                style={styles.textStyle}
            />
        );
    };

    readMessage = async (item) => {
        let {onCallBack} = this.params;
        let url = ServicesApi.INDEX_READ;
        let data = {
            id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.message(result.msg);
            onCallBack && onCallBack()
            this._onRefresh();
        }
    }

    /**
     * 返回查询消息数量
     */
    callbackQureyMes = () => {


        RouterHelper.goBack()
    }

    render() {
        let {loading, dataSources} = this.state;
        let pageTitle = this.params.pageTitle || '我的消息';
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={pageTitle}/>
                <ListView
                    data={dataSources}
                    ref={this._captureRef}
                    extraData={this.state}
                    style={styles.listContent}
                    onRefresh={this._onRefresh}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                    onEndReached={this._onEndReached}
                />
            </PageContainer>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});