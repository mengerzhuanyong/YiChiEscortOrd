/**
 * 易驰护运 - Mine
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react';
import {StyleSheet, Text, ScrollView, TouchableOpacity, View} from 'react-native';
import {
    Badge,
    TagItem,
    ListRow,
    ListHeaderLoading,
} from '../../components'
import { MineStyle } from '../../style'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class Mine extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            navArray: [
                {title: '修改密码', icon: Images.icon_user_customer, component: 'ChangePassword'},
                // {title: '清除缓存', icon: Images.icon_user_profile, component: 'SystemMessage'},
                // {title: '关于我们', icon: Images.icon_user_profile, component: 'AboutUs'},
            ],
        }
    }

    // 增加CallBack的跳转方法
    _onPressToNavigate = (pageTitle, component, params) => {
        let {loginStore} = this.props;
        RouterHelper.navigate(pageTitle, component, {
            ...params,
            onCallBack: () => loginStore.getLatestUserInfo(),
        });
    };

    componentDidMount() {

    }

    // 退出登录
    _onPressLogout = () => {
        let {loginStore} = this.props;
        let params = {
            title: '温馨提醒',
            detail: '您确定要退出吗？',
            actions: [
                {title: '取消'},
                {
                    title: '确认',
                    titleStyle: {color: Predefine.themeColor},
                    onPress: () => {
                        loginStore.doLogout()
                        // RouterHelper.navigate('', 'Login')
                        RouterHelper.reset('', 'Tab')
                    }
                },
            ]
        };
        AlertManager.show(params);
    };

    _renderNavigationContent = (navArray) => {
        let {loginStore} = this.props;
        if (navArray.length < 1) return null;
        let content = navArray.map((item, index) => {
            return (
                <ListRow
                    key={index}
                    title={item.title}
                    // icon={item.icon}
                    style={[styles.navItemStyle, item.margin && Predefine.MB10]}
                    iconStyle={styles.navItemIconStyle}
                    titleStyle={styles.navItemTitleStyle}
                    bottomSeparator={(item.margin || index == navArray.length - 1) ? 'none' : 'indent'}
                    onPress={() => {
                        if (item.component === 'Login') {
                            let params = {
                                title: '温馨提醒',
                                detail: '切换身份将退出当前账号，您确定要退出吗？',
                                actions: [
                                    {title: '取消'},
                                    {title: '确认', onPress: () => loginStore.doLoginOut()},
                                ]
                            };
                            AlertManager.show(params);
                        } else {
                            RouterHelper.navigate(item.title, item.component, {
                                page_flag: Constants.NORMAL,
                                ...item.props,
                            });
                        }
                    }}
                />
            );
        });
        return content;
    };

    render() {
        let pageTitle = this.params.pageTitle || '设置';
        let {navArray} = this.state;
        return (
            <PageContainer style={styles.container}>
                <NavigationBar title={pageTitle}/>
                <ScrollView style={styles.content}>
                    <View style={[styles.navContentStyle]}>
                        {this._renderNavigationContent(navArray)}
                    </View>
                    {/*<Button*/}
                    {/*    title={'退出登录'}*/}
                    {/*    style={styles.btnItemStyle}*/}
                    {/*    titleStyle={styles.btnTitleStyle}*/}
                    {/*    onPress={this._onPressLogout}*/}
                    {/*/>*/}
                </ScrollView>
            </PageContainer>
        );
    }
}

const styles = StyleSheet.create({
    ...MineStyle,
    btnItemStyle: {
        marginVertical: 30,
        marginHorizontal: 15,
        backgroundColor: Predefine.themeColor,
    },
    btnTitleStyle: {
        fontSize: 16,
    },
});
