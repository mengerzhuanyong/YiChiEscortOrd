/**
 * 易驰护运 - DriverOrder
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import { 
    Text, 
    View, 
    Keyboard, 
    StyleSheet, 
    ScrollView, 
    BackHandler,
    ImageBackground, 
    TouchableOpacity, 
} from 'react-native'
import {
    IconFont,
    ListView,
    VerticalLine,
    HorizontalLine,
    BannerComponent,
    HotNewsComponent,
    HomeResourceItem,
    ListHeaderLoading,
    SegmentedView,
    SegmentedScene,
    Label,
} from '../../components'
import {inject, observer} from 'mobx-react'
import { Geolocation } from 'react-native-baidu-map'
import DriverOrderList from '../../components/customer/Driver/DriverOrderList'

@inject('loginStore')
@observer
export default class DriverOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSources: [
                {id: 1, name: '待开始',},
                {id: 2, name: '待装货',}, 
                {id: 3, name: '入场装货',},
                {id: 4, name: '待送达',},
                {id: 5, name: '入场卸货',},
                {id: 6, name: '已完成',},
            ],
            initialPage: 0,

            longitude: '',
            latitude: '',
        }
    }

    componentDidMount () {
        this.getCurrentPosition();
    }

    // 请求位置
    getCurrentPosition = () => {
        Geolocation.getCurrentPosition()
        .then(data => {
            console.log('地理位置', data)
            this.setState({
                longitude: data.longitude,
                latitude: data.latitude,
            }, () => this.requestData())
        })
        .catch(e => {console.log('error---->', e)})
    }

    requestData = async() => {
        let {longitude, latitude} = this.state;
        let url = ServicesApi.VEHICLE_COORDINATE_LIST;
        let data = {
            longitude,
            latitude,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            // 就发个地址啥都没有
        }
    }

    onBackHandler = () => {
        let {loginStore} = this.props;
        let detail = '确认退出司机端？';
        let params = {
            detail,
            actions: [
                { title: '取消', },
                {
                    title: '确定', 
                    onPress: () => {
                        loginStore.doDriverLogout()
                        RouterHelper.reset('', 'Tab')
                    },
                },
            ],
        };
        AlertManager.show(params);
        return true;
    }

    _renderSegmentedTabView = () => {
        let {dataSources} = this.state;
        let content = dataSources.map((item, index) => {
            return (
                <DriverOrderList
                    key={index}
                    title={item.name}
                    titleStyle={{color: '#fff'}}
                    style={styles.segmentedTabView}
                    item={item}
                    status={item}
                    {...this.props}
                />
            )
        })
        return content;
    }
    
    render() {
        let {dataSources, initialPage} = this.state;
        let {loginStore} = this.props;
        let {userInfo, getJSDataSources} = loginStore;
        console.log('getJSDataSources(userInfo)------------->', getJSDataSources(userInfo))
        return (
            <PageContainer style={styles.container}>
                <NavigationBar
                    title={'我的订单'}
                    renderLeftAction={null}
                    renderRightAction={[{
                        title: <IconFont.AntDesign
                            name={'poweroff'}
                            style={styles.rightAction}
                        />, 
                        onPress: () => this.onBackHandler()
                    }]}
                />
                <View style={{flex: 1}}>
                    {dataSources.length > 0 ? <SegmentedView
                        initialPage={initialPage}
                        indicatorType={'lengThen'}
                        indicatorWidthType={'item'}
                        style={styles.segmentedView}
                    >
                        {this._renderSegmentedTabView()}
                    </SegmentedView> : null}    
                </View>
            </PageContainer>
        );
    }
}

const styles = StyleSheet.create({
    rightAction: {
        color: '#fff',
        fontSize: 20,
    },
    segmentedTabView: {
        flex: 1,
    },
});