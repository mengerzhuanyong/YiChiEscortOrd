/**
 * 易驰护运 - Driver
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, TextInput, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard} from 'react-native'
import {
    IconFont,
    ListView,
    Checkbox,
    VerticalLine,
    HorizontalLine,
    BannerComponent,
    HotNewsComponent,
    HomeResourceItem,
    ListHeaderLoading,
} from '../../components'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view"
import {FormValidation} from '../../utils/validation/FormValidation'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class Driver extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            emp_no: '', // 手机号
            code: '', // 密码
            checked_one: false,
            time: 60,
            codeBtnText: '获取验证码',
            dataSource: [],
        }
    }

    componentDidMount () {
        this.requestData()
    }

    requestData = async() => {
        let url = ServicesApi.ARTICLE_ARTICLE_LIST;
        let data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                dataSource: result.data,
                loading: false,
            });
        }
    }

    // 获取验证码
    requestGetCodeSms = async () => {
        let {emp_no, time} = this.state
        if (emp_no == '') {
            ToastManager.message('请输入手机号');
            return
        }
        if (time < 60) return false
        let url = ServicesApi.SEND_SMS;
        let data = {
            mobile: emp_no,
            type: 2, // 1：注册时绑定手机号，2：验证码登录时获取验证码，3：忘记密码时验证手机号
            opmode: 2,
        };
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success('短信发送成功，请注意查收');
            this.countDown()
        } else {
            ToastManager.fail(result.msg);
        }
    };

    // 倒计时
    countDown = () => {
        let {time} = this.state
        this.timer = setInterval(()=> {
            if (time == 0){
                clearInterval(this.timer);
                this.setState({
                    time: 60,
                    codeBtnText: '获取验证码'
                })
            } else{
                this.setState({
                    time: time,
                    codeBtnText: time + '秒后重新获取'
                })
            }
            time--
        }, 1000) 
    }
    
    // 提交表单
    onSubmit = () => {
        Keyboard.dismiss();
        let {emp_no, code, checked_one} = this.state;
        if (emp_no == '') {
            ToastManager.message('请输入手机号')
            return
        }
        if (code == '') {
            ToastManager.message('请输入验证码')
            return
        }
        if (checked_one == false) {
            ToastManager.message('请同意《用户服务协议》')
            return
        }
        this.requestSubmit()
    }

    // 发送请求->登录
    requestSubmit = async () => {
        let {loginStore} = this.props;
        let {emp_no, code} = this.state;
        let url = ServicesApi.LOGIN;
        let data = {
            type: 3, // 类型。1：密码登录，2：货主、承运商验证码登录，3：司机验证码登录
            emp_no: emp_no,
            code: code,
        };
        let result = await loginStore.doDriverLogin(url, data);
        console.log('result------------->', result)
    };
    
    render() {
        let {loading, emp_no, code, checked_one, codeBtnText, dataSource} = this.state;
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar
                    title={'司机入口'}
                    renderLeftAction={null}
                    style={[styles.navigationBarStyle]}
                />
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={'handled'}
                    style={styles.contentBoxStyle}
                >
                    <ImageView
                        resizeMode={'cover'}
                        style={styles.backGroundImg}
                        source={Images.img_login_bg}
                    />
                    <View style={styles.containerBox}>

                        <View style={styles.box}>
                            <View style={styles.logoBox}>
                                <ImageView
                                    resizeMode={'contain'}
                                    style={styles.logo}
                                    source={Images.icon_yc_logo1}
                                />
                            </View>
                            <View style={styles.form}>
                                <View style={styles.inputLine}>
                                    <ImageView
                                        resizeMode={'contain'}
                                        style={styles.inputIcon}
                                        source={Images.icon_login_icon1}
                                    />
                                    <TextInput
                                        style={styles.input}
                                        placeholder={'请输入手机号'}
                                        placeholderTextColor={'#999'}
                                        onChangeText={(text) => this.setState({emp_no: text})}
                                        keyboardType={'phone-pad'}
                                        maxLength={11}
                                        defaultValue={emp_no}
                                    />
                                </View>
                                <View style={styles.inputLine}>
                                    <ImageView
                                        resizeMode={'contain'}
                                        style={styles.inputIcon}
                                        source={Images.icon_login_icon2}
                                    />
                                    <TextInput
                                        style={styles.input}
                                        placeholder={'请输入验证码'}
                                        placeholderTextColor={'#999'}
                                        onChangeText={(text) => this.setState({code: text})}
                                        secureTextEntry={true}
                                        defaultValue={code}
                                    />
                                    <TouchableOpacity style={styles.inputCodeBox} onPress={() => this.requestGetCodeSms()}>
                                        <Text style={styles.inputCode} numberOfLines={1}>{codeBtnText}</Text>
                                    </TouchableOpacity>
                                </View>    
                            </View>

                            <View style={styles.loginBtnBox}>
                                <Button
                                    style={styles.loginBtn}
                                    title={'登录'}
                                    titleStyle={styles.loginBtnTitle}
                                    onPress={this.onSubmit}
                                />
                            </View> 
                        </View>

                        <View style={styles.agreement}>
                            <Checkbox
                                style={styles.agreementImg}
                                checked={checked_one}
                                onPress={() => {
                                    if (checked_one === true) {
                                        this.setState({checked_one: false})
                                    } else {
                                        this.setState({checked_one: true})
                                    }
                                }}
                            />
                            <Text
                                style={styles.agreementText}
                                onPress={() => RouterHelper.navigate('', 'Agreement', {
                                    agreementTitle: dataSource[0].title,
                                    agreementUrl: dataSource[0].url
                                })}
                            >
                                已同意并同意<Text style={{color: '#0C81FF'}}>{dataSource[0] && dataSource[0].title}</Text>
                            </Text>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const SecondaryColor = '#E4E4E4';
const styles = StyleSheet.create({
    // 背景图
    backGroundImg: {
        zIndex: 0,
        position: 'absolute',
        width: SCREEN_WIDTH,
        height: SCREEN_WIDTH * (570 / 1125),
    },
    containerBox: {
        minHeight: SCREEN_HEIGHT - 260,
        justifyContent: 'space-between',
    },
    box: {
        flex: 1,
        alignItems: 'center',
    },

    // 图标
    logoBox: {
        width: 80,
        height: 80,
        borderRadius: 10,
        backgroundColor: '#fff',
        marginTop: SCREEN_WIDTH * (570 / 1125) - 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 20, // 安卓
        shadowOffset: {width: 5, height: 5},
        shadowColor: '#C8C8C8',
        shadowOpacity: 1,
        shadowRadius: 10,
        marginBottom: 20,
    },
    logo: {
        width: 70,
        height: 70,
    },

    // 密码登录/验证码登录
    header: {
        flexDirection: 'row',
        marginBottom: 20,
    },
    headerBox: {
        width: 100,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: SecondaryColor,
    },
    headerBoxAchieve: {
        borderBottomColor: DominantColor,
    },
    headerText: {
        color: '#BBB',
    },
    headerTextAchieve: {
        color: DominantColor,
    },

    // form
    form: {
        width: SCREEN_WIDTH,
        paddingHorizontal: 30,
        marginBottom: 50,
    },
    inputLine: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#C8C8C8',
    },
    inputIcon: {
        width: 14,
        height: 14,
        marginRight: 8,
    },
    input: {
        flex: 1,
        height: 50,
        paddingHorizontal: 0,
        paddingVertical: 0,
        fontSize: 14,
    },
    inputCodeBox: {
        paddingLeft: 5,
        borderLeftWidth: 1,
        borderLeftColor: DominantColor,
    },
    inputCode: {
        fontSize: 15,
        color: DominantColor,
    },

    // 按钮
    loginBtnBox: {
        elevation: 20, // 安卓
        shadowOffset: {width: 1, height: 1},
        shadowColor: DominantColor,
        shadowOpacity: 0.5,
        shadowRadius: 5,
    },
    loginBtn: {
        width: SCREEN_WIDTH - 60,
        height: 36,
        backgroundColor: DominantColor,
        borderRadius: 18,
        paddingHorizontal: 0,
        paddingVertical: 0,
    },

    // 底部用户服务协议
    agreement: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 15,
        paddingTop: 50,
    },
    agreementImg: {
        marginRight: 6,
    },
    agreementText: {
        fontSize: 14,
    },
});