/**
 * 易驰护运 - DriverMap
 * http://menger.me
 * 地图
 * @huanhaun
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    StatusBar,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { MapView, MapTypes, Geolocation, Overlay, MapApp } from 'react-native-baidu-map'
import Webview from 'react-native-webview'

export default class DriverMapWeb extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            from_address: '',
            from_warp: '',
            from_weft: '',
            to_address: '',
            to_warp: '',
            to_weft: '',
            uri: '',
        };
    }

    componentDidMount () {
        this.requestData();
    }

    requestData = async() => {
        var X1 = {$start_longitude};
        var Y1 = {$start_latitude};
        var X2 = {$end_longitude};
        var Y2 = {$end_latitude};
        let {item} = this.params;
        let url = ServicesApi.VEHICLE_NAVIGATION;
        let data = {
            id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                from_address: result.data.from_address,
                from_warp: Number(result.data.from_warp),
                from_weft: Number(result.data.from_weft),
                to_address: result.data.to_address,
                to_warp: Number(result.data.to_warp),
                to_weft: Number(result.data.to_weft),
                loading: false,
            }, () => {this.requestWebData()})
        }
    }

    requestWebData = async () => {
        let {from_warp, from_weft, to_warp, to_weft} = this.state;
        let url = ServicesApi.VEHICLE_DRIVER_MAP;
        let data = {
            start_longitude: from_warp,
            start_latitude: from_weft,
            end_longitude: to_warp,
            end_latitude: to_weft,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({uri: result.data.url})
        }
    }

    render() {
        let {loading} = this.state;
        let uri = 'http://127.0.0.1:5500/driver_map/drive_map.html';
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={'地图'}/>
                <Webview
                    source={{uri}}
                    onLoadEnd={this.onLoadEnd}
                    startInLoadingState={false}
                    style={[styles.webContainer]}
                />
                <View style={styles.btnBox}>
                    <Button
                        style={styles.btn}
                        title={'打开百度地图导航'}
                        titleStyle={styles.btnTitle}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => this.openMap()}
                    />
                </View>
            </PageContainer>
        )
    }

    openMap = () => {
        let params = {
            title: '即将离开APP',
            detail: '为了给您带来更好的体验，请确保您的手机中安装有百度地图APP。',
            actions: [
                { title: '取消', },
                { title: '确定', onPress: () => this.openBaiduMapApp() },
            ],
        };
        AlertManager.show(params);
        return true;
    }

    openBaiduMapApp = () => {
        let {from_address, from_warp, from_weft, to_address, to_warp, to_weft} = this.state;
        //唤起百度地图
        MapApp.openDrivingRoute(
            {latitude: from_warp, longitude: from_weft, name: from_address}, 
            {latitude: to_warp, longitude: to_weft, name: to_address}, 
        )
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
    },
    map: {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT - NAV_BAR_HEIGHT - 50,
    },
    list: {
        paddingHorizontal: 15,
    },
    listText: {
        height: 20,
    },
    btnBox: {
        // paddingHorizontal: 15,
        // paddingVertical: 10,
    },
    btn: {
        backgroundColor: DominantColor,
        height: 50,
        paddingVertical: 0,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
    },
})