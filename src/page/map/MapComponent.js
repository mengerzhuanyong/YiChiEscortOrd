/**
 * 易驰护运 - DriverMap
 * http://menger.me
 * 地图
 * @huanhaun
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    StatusBar,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import {
    IconFont,
} from '../../components'
import { MapView, MapTypes, Geolocation, Overlay, MapApp, } from 'react-native-baidu-map'

export default class MapComponent extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            center: {
                longitude: 0,
                latitude: 0,
            },
            markers: [
                {
                    longitude: 0, 
                    latitude: 0,
                    title: 'title',
                    uid: 0,
                    random: Math.random(),
                }
            ],
            clickMessage: '',
            poiMessage: '',
        };
    }

    componentDidMount () {
        this.getCurrentPosition()
    }

    // 请求位置
    getCurrentPosition = () => {
        Geolocation.getCurrentPosition()
        .then(data => {
            console.log('地理位置', data)
            this.setState({
                center: {
                    longitude: data.longitude,
                    latitude: data.latitude
                },
                markers: [{
                    longitude: data.longitude,
                    latitude: data.latitude,
                    title: data.district + data.street,
                    random: Math.random(),
                    uid: data.uid,
                }],
                loading: false,
            })
        })
        .catch(e => {console.warn(e, 'error')})
    }

    //地图空白区域点击事件,返回经纬度
    onMapClick = (e) => {
        let title = '';
        Geolocation.reverseGeoCode(e.latitude,e.longitude)
            .then(res => {
                console.log('点击事件', res)
                console.log('点击值', e)
                Platform.OS == 'ios' ? 
                    title = res.district + res.streetName
                :
                    title = res.district + res.street;
                this.setState({
                    center: {
                        longitude: e.longitude,
                        latitude: e.latitude,
                    },
                    markers: [{
                        longitude: e.longitude,
                        latitude: e.latitude,
                        title: title,
                        random: Math.random(),
                        uid: e.uid,
                    }],
                    clickMessage: JSON.stringify(res)
                })
            })
            .catch(err => {console.log(err)})
    }

    //地图已有点点击
    onMapPoiClick = (e) => {
        Geolocation.reverseGeoCode(e.latitude,e.longitude)
        .then(res => {
            res = JSON.stringify(res)
            console.log('e------------->', e)
            this.setState({
                center: {
                    longitude: e.longitude,
                    latitude: e.latitude,
                },
                markers: [{
                    longitude: e.longitude,
                    latitude: e.latitude,
                    title: e.name,
                    random: Math.random(),
                    uid: e.uid,
                }],
                poiMessage: res
            })
        })
        .catch(err => {console.log(err)})
    }

    render() {
        let {getAddress, markersType} = this.params;
        let {loading, markers} = this.state;
        let icon = { uri: 'https://mapopen-website-wiki.cdn.bcebos.com/homePage/images/logox1.png' };
        let location = {
            longitude: markers[0].longitude, 
            latitude: markers[0].latitude, 
            random: Math.random(),
            uid: markers[0].uid,
        };
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={'地图'}/>
                <MapView 
                    style={styles.map}
                    zoomControlsVisible={true} //默认true,是否显示缩放控件,仅支持android
                    trafficEnabled={false} //默认false,是否显示交通线
                    baiduHeatMapEnabled={false} //默认false,是否显示热力图
                    mapType={MapTypes.NORMAL} //地图模式,NORMAL普通 SATELLITE卫星图
                    zoom={15} //缩放等级,默认为10
                    center={location}
                    // center={this.state.center} // 地图中心位置
                    onMarkerClick={(e) => console.log('点击出问题了', e)} //标记点点击事件
                    onMapClick={(e) => this.onMapClick(e)} //地图空白区域点击事件,返回经纬度
                    onMapPoiClick={(e) => this.onMapPoiClick(e)} //地图已有点点击
                >
                    {/* <Overlay.Cluster>
                        <Overlay.Marker 
                            rotate={45} 
                            // icon={icon} 
                            location={location}
                        />
                    </Overlay.Cluster> */}
                </MapView>
                <View style={styles.list}>
                    <Text style={styles.listText}>所选位置信息：</Text>
                    <View style={styles.listLine}>
                        <IconFont.Fontisto name={'map-marker-alt'} size={20} color={DominantColor} style={styles.listIcon} />
                        <Text style={styles.listText}>精度: {markers[0].longitude}</Text>
                    </View>
                    <View style={styles.listLine}>
                        <IconFont.Fontisto name={'map-marker-alt'} size={20} color={DominantColor} style={styles.listIcon} />
                        <Text style={styles.listText}>维度: {markers[0].latitude}</Text>
                    </View>
                    <View style={styles.listLine}>
                        <IconFont.Entypo name={'map'} size={20} color={DominantColor} style={styles.listIcon} />
                        <Text style={styles.listText}>{markers[0].title.toString() == 'NaN' ? '所选地址暂无详细信息' : markers[0].title}</Text>
                    </View>
                </View>
                <View style={styles.btnBox}>
                    <Button
                        style={styles.btn}
                        title={'确定'}
                        titleStyle={styles.btnTitle}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => {
                            getAddress && getAddress(markersType == 'start' ? 'start' : 'end', { markers: markers[0] })
                            RouterHelper.goBack()
                        }}
                    />    
                </View>
            </PageContainer>
        )
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#f2f2f2',
    },
    map: {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT - (Predefine.statusBarHeight + Predefine.navBarHeight) - 110 - 55,
        marginBottom: 5,
    },
    list: {
        paddingHorizontal: 15,
        height: 100,
        justifyContent: 'space-around',
    },
    listLine: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    listIcon: {
        marginRight: 6,
    },
    listText: {
        fontSize: 15,
        height: 20,
    },
    btnBox: {
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    btn: {
        backgroundColor: DominantColor,
        height: 35,
        paddingVertical: 0,
    },
})