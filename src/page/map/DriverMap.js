/**
 * 易驰护运 - DriverMap
 * http://menger.me
 * 地图
 * @huanhaun
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    StatusBar,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { MapView, MapTypes, Geolocation, Overlay, MapApp } from 'react-native-baidu-map'

export default class DriverMap extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            center: {
                longitude: 0,
                latitude: 0,
            },
            markers: [
                { // 目的地
                    longitude: 0, 
                    latitude: 0,
                    title: 'title',
                    random: Math.random(),
                },
                { // 所在地
                    longitude: 0, 
                    latitude: 0,
                    title: 'title',
                    random: Math.random(),
                }
            ],
            clickMessage: '',
            poiMessage: '',
        };
    }

    componentDidMount () {
        this.requestData()
    }

    requestData = async() => {
        let {item} = this.params;
        let url = ServicesApi.VEHICLE_NAVIGATION;
        let data = {
            id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            Geolocation.getCurrentPosition()
                .then(data => {
                    console.log('地理位置', data)
                    this.setState({
                        center: {
                            longitude: data.longitude,
                            latitude: data.latitude
                        },
                        markers: [
                            {
                                longitude: Number(result.data.warp), 
                                latitude: Number(result.data.warp),
                                title: result.data.to_address,
                                random: Math.random(),
                            },
                            {
                                longitude: data.longitude,
                                latitude: data.latitude,
                                title: data.district + data.street,
                                random: Math.random(),
                            }
                        ],
                        loading: false,
                    })
                })
                .catch(e => {console.warn(e, 'error')})
        }
    }

    render() {
        let {loading, markers} = this.state;
        let location1 = {
            longitude: markers[0].longitude, 
            latitude: markers[0].latitude,
        };
        let location2 = {
            longitude: markers[1].longitude, 
            latitude: markers[1].latitude,
        };
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={'地图'}/>
                <MapView 
                    style={styles.map}
                    zoomControlsVisible={true} //默认true,是否显示缩放控件,仅支持android
                    trafficEnabled={false} //默认false,是否显示交通线
                    baiduHeatMapEnabled={false} //默认false,是否显示热力图
                    mapType={MapTypes.NORMAL} //地图模式,NORMAL普通 SATELLITE卫星图
                    zoom={1} //缩放等级,默认为10
                    center={this.state.center} // 地图中心位置
                >
                    <Overlay.Cluster>
                        <Overlay.Marker 
                            rotate={45} 
                            location={location1}
                        />
                        <Overlay.Marker 
                            rotate={45} 
                            location={location2}
                        />
                    </Overlay.Cluster>
                </MapView>
                <View style={styles.btnBox}>
                    <Button
                        style={styles.btn}
                        title={'打开百度地图导航'}
                        titleStyle={styles.btnTitle}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => this.openMap()}
                    />
                </View>
            </PageContainer>
        )
    }

    openMap = () => {
        let params = {
            title: '即将离开APP',
            detail: '为了给您带来更好的体验，请确保您的手机中安装有百度地图APP。',
            actions: [
                { title: '取消', },
                { title: '确定', onPress: () => this.openBaiduMapApp() },
            ],
        };
        AlertManager.show(params);
        return true;
    }

    openBaiduMapApp = () => {
        let {markers} = this.state;
        //唤起百度地图
        MapApp.openDrivingRoute(
            {latitude: markers[1].latitude, longitude: markers[1].longitude, name: markers[1].title}, 
            {latitude: markers[0].latitude, longitude: markers[0].longitude, name: markers[0].title},
        )
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
    },
    map: {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT - NAV_BAR_HEIGHT - 50,
    },
    list: {
        paddingHorizontal: 15,
    },
    listText: {
        height: 20,
    },
    btnBox: {
        // paddingHorizontal: 15,
        // paddingVertical: 10,
    },
    btn: {
        backgroundColor: DominantColor,
        height: 50,
        paddingVertical: 0,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
    },
})