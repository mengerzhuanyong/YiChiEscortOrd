/**
 * 易驰护运 - Supply
 * http://menger.me
 * 货源大厅
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard, DeviceEventEmitter} from 'react-native'
import {
    IconFont,
    ListView,
    VerticalLine,
    ChoiceTagItem,
    HorizontalLine,
    BannerComponent,
    HotNewsComponent,
    HomeResourceItem,
    ListHeaderLoading,
} from '../../components';
import SupplyList from '../../components/customer/Supply/SupplyList';
import {inject, observer} from 'mobx-react';

@inject('loginStore')
@observer
export default class Supply extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            // 筛选条件
            filter: {},
            // 地址
            startProvince: '',
            startCity: '',
            startDistrict: '',
            endProvince: '',
            endCity: '',
            endDistrict: '',
            startAddress: '',
            endAddress: '',
        }
    }

    componentDidMount = () => {
        DeviceEventEmitter.addListener('SearchHotCity', (e) => this.getSkipVal(e));// 个人中心切换身份跳过来的
        this.getSkipVal()
    }
    
    componentWillUnmount() {
        DeviceEventEmitter.removeAllListeners('SearchHotCity');
    }
    
    // 获取从热门城市搜索跳过来的值
    getSkipVal = (e) => {
        let area = '';
        if (e) {
            area = e 
        } else {
            area = ( this.params ? this.params.address : '' ) // params首次进入可能没有，是个Tab页面。
        }
        if (area && area.startProvince == '北京') {
            area && this.setState({
                startProvince: '北京',
                startCity: '北京市',
                startDistrict: '北京市',
                startAddress: '北京-北京市-北京市',
            }, () => {
                setTimeout(() => {
                    this.refList._onRefresh()
                }, 500);
            }) 
        } else {
            area && this.setState({
                startProvince: area.startProvince || '',
                startCity: area.startCity || '',
                startDistrict: area.startDistrict || '',
                startAddress: area.startAddress || '',
            }, () => {
                setTimeout(() => {
                    this.refList._onRefresh()
                }, 500);
            })    
        }
    }

    showActionAreaPicker = (type) => {
        let params = {
            // showArea: false,
            onCallBack: (selectedData, addressText) => {
                if (type == 'start') {
                    this.setState({
                        startProvince: selectedData.province.name,
                        startCity: selectedData.city.name,
                        startDistrict: selectedData.area.name,
                        startAddress: addressText,
                    },() => this.refList._onRefresh());     
                } else if (type == 'end') {
                    this.setState({
                        endProvince: selectedData.province.name,
                        endCity: selectedData.city.name,
                        endDistrict: selectedData.area.name,
                        endAddress: addressText,
                    },() => this.refList._onRefresh()); 
                }
            },
            option: {
                panGestureEnabled: false,
            }
        }
        ActionManager.showCityTree(params);
    };

    callback = (item) => { // 筛选传过来的值
        this.setState({
            filter: item,
        }, () => this.refList._onRefresh())
    };
    
    render() {
        let {filter, startProvince, startCity, startDistrict, endProvince, endCity, endDistrict, startAddress, endAddress} = this.state;
        let address = {
            startAddress:{startProvince,startCity,startDistrict},
            endAddress:{endProvince,endCity,endDistrict}
        }
        return (
            <PageContainer style={styles.container}>
                <NavigationBar
                    title={'货源大厅'}
                    style={[styles.navigationBarStyle]}
                    renderLeftAction={null}
                    renderRightAction={[{
                        title: <Text style={styles.navigationBarBtn}><IconFont.AntDesign name={'filter'} size={14} color={'#fff'}/>更多筛选</Text>, 
                        onPress: () => RouterHelper.navigate('', 'SupplyFilter', {
                            filter, // 在非第一次进入页面吧已经筛选的条件传进去
                            callback: (item) => this.callback(item)
                        })
                    }]}
                />
                <View style={{flex: 1}}>
                    <View style={styles.choice}>
                        <TouchableOpacity style={styles.choiceBox} onPress={() => this.showActionAreaPicker('start')}>
                            <Text style={styles.choiceText} numberOfLines={1}>{startProvince == '' ? '发货地' : startAddress}</Text>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.choiceArrow}
                                source={Images.icon_arrow_logo1}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.choiceBox} onPress={() => this.showActionAreaPicker('end')}>
                            <Text style={styles.choiceText} numberOfLines={1}>{endProvince == '' ? '卸货地' : endAddress}</Text>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.choiceArrow}
                                source={Images.icon_arrow_logo1}
                            />
                        </TouchableOpacity>
                    </View>
                    <SupplyList 
                        style={styles.supplyList}
                        address={address} // 地址的选择
                        filter={filter} // 筛选页面的筛选条件
                        ref={(ref) => {this.refList = ref}}  
                        {...this.props}
                    />    
                </View>
            </PageContainer>
        );
    }
}

const styles = StyleSheet.create({
    navigationBarBtn: {
        color: '#fff',
        fontSize: 14,
    },
    choice: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 30,
        height: 50,
        backgroundColor: '#FAFAFA',
    },
    choiceBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    choiceText: {
        fontSize: 14,
        color: '#333',
    },
    choiceArrow: {
        width: 12,
        height: 6,
        marginLeft: 5,
    },
});