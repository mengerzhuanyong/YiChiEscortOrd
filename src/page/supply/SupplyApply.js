/**
 * 易驰护运 - SupplyApply
 * http://menger.me
 * 申请订单
 * @桓桓
 */

'use strict';

import React from 'react'
import {StyleSheet, Text, ScrollView, TouchableOpacity, View, Keyboard, TextInput} from 'react-native'
import * as WeChat from 'react-native-wechat-lib'
import {
    IconFont,
    ApplySupplyCard,
    ApplyMessageCard,
    ListHeaderLoading,
} from '../../components'
import {inject, observer} from 'mobx-react';

@inject('loginStore')
@observer
export default class SupplyApply extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            isRefreshing: false,
            payTypeArray: [],
            dataSource: [],
            carsSource: [],
            carsList: [],
            carsDate: [],
            carsTime: [],
            payType: 0, // 支付方式
            weight: 0,
            carsNum: 0,
            price: 0,
        }
    }

    componentDidMount() {
        // 监听微信的支付
        this._addWeChatListener();
        // 请求申请接单的信息
        this.requestData();
        // 请求支付方式
        this.requestPayStyle();
    }

    _onRefresh = async () => {
        this.setState({isRefreshing: true});
        this.requestData();
        this.requestPayStyle();
        this.setState({
            weight: 0,
            carsNum: 0,
            price: 0,
            carsDate: [],
            carsTime: [],
        })
        setTimeout(() => this.setState({isRefreshing: false}), 500);
    };

    // componentWillUnmount() {
    //     DeviceEventEmitter.removeAllListeners('PayReq.Resp');
    // }

    // 监听那个react-native-wechat-lib
    _addWeChatListener = async () => {
        let {onRefresh} = this.params;
        WeChat.addListener('PayReq.Resp', (res) => {
            if (res.errCode === 0) {
                ToastManager.message('支付成功!');
                this._onRefresh();
                onRefresh && onRefresh();
                RouterHelper.goBack();
            } else if (res.errCode === -1) {
                ToastManager.message('支付失败!');
                this._onRefresh();
                onRefresh && onRefresh();
            } else if (res.errCode === -2) {
                ToastManager.message('取消支付!');
            }
        });
    };

    requestData = async () => {
        let {item, payType} = this.params;
        let url = ServicesApi.ORDER_RECEIVE_GOODS;
        let data = {goods_id: item.id};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {

            let arr = [], carArr = [];
            result.data.vehicle_list.forEach(element => {
                arr.push('')
                carArr.push(false)
            });

            let _dataSource = result.data.data;
            let price = '';
            payType == '竞价发布'
                ? (_dataSource.receipt_type == 1 ? price = _dataSource.shipper_freight_price : price = price || 0)
                : (_dataSource.receipt_type == 1 ? price = _dataSource.shipper_freight_price : price = _dataSource.platform_freight_price)

            this.setState({
                dataSource: result.data.data,
                carsSource: result.data.vehicle_list,
                carsList: carArr,
                carsDate: arr,
                carsTime: arr,
                price: price,
                loading: false,
            });
        }
    }

    requestPayStyle = async () => {
        let url = ServicesApi.INDEX_PAY_TYPE;
        let data = {  };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                payTypeArray: result.data,
            })
        }
    }

    // ---------------------------------- 功能函数 ----------------------------------

    // 选择支付方式
    payTypeChoice = () => {
        let cate_list = [
            {id: 1, name: '支付宝支付'},
            {id: 2, name: '微信支付'},
            {id: 3, name: '余额支付'},
        ]
        let actions = cate_list.map((item) => {
            return {
                title: item.name,
                onPress: () => {
                    this.setState({ payType: item.id });
                    ActionManager.hide();
                }
            }
        });
        ActionManager.show({actions});
    }

    // 点击选择车辆
    _choiceCar = (item, index) => {
        let {carsList, weight, carsNum} = this.state;
        let arr = carsList.slice()
        if (arr[index] === true) {
            arr.splice(index, 1, false);
            this.setState({
                weight: weight*1 - item.vehicle_weight*1,
                carsNum: carsNum - 1,
            })
        } else {
            arr.splice(index, 1, true);
            this.setState({
                weight: weight*1 + item.vehicle_weight*1,
                carsNum: carsNum + 1,
            })
        }
        this.setState({carsList: arr})
    }

    // 点击选择日期
    _choiceDate = (carsIndex) => {
        let {carsDate} = this.state;
        let arr = carsDate.slice()
        let params = {
            onPress: (value) => {
                let date = value.year+'-'+value.month+'-'+value.day;
                if (arr.length == 0) {
                    carsDate.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.push(date)
                        } else {
                            arr.push('')
                        }
                    })
                } else {
                    carsDate.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.splice(index, 1, date)
                        }
                    })
                }
                this.setState({carsDate: arr})
            },
            option: {
                panGestureEnabled: false,
            }
        }
        ActionManager.showWheelDate(params);
    }

    // 点击选择时间
    _choiceTime = (carsIndex) => {
        let {carsTime} = this.state;
        let arr = carsTime.slice()
        let params = {
            onPress: (value) => { 
                if (arr.length == 0) {
                    carsTime.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.push(value.hour+':'+value.minute)
                        } else {
                            arr.push('')
                        }
                    })
                } else {
                    carsTime.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.splice(index, 1, value.hour+':'+value.minute)
                        }
                    })
                }
                this.setState({carsTime: arr})
            },
            option: {
                panGestureEnabled: false,
            }
        }
        ActionManager.showTimeContent(params);
    }

    // 弹出的支付方式选择
    payTypeBtnView = () => {
        let {payType} = this.state;
        let payBtn = '';
        switch (payType) {
            case 0:
                payBtn = <Text style={styles.cardHeaderText}>请选择支付方式</Text>
                break;
            case 1:
                payBtn = (
                    <View style={styles.cardHeaderTextBox}>
                        <IconFont.AntDesign name={'alipay-circle'} size={16} color={'#108ee9'} style={styles.cardHeaderTextIcon}/>
                        <Text style={[styles.cardHeaderText, {color: '#108ee9'}]}>支付宝支付</Text>
                    </View>
                )
                break;
            case 2:
                payBtn = (
                    <View style={styles.cardHeaderTextBox}>
                        <IconFont.AntDesign name={'wechat'} size={16} color={'#01BD0C'} style={styles.cardHeaderTextIcon}/>
                        <Text style={[styles.cardHeaderText, {color: '#01BD0C'}]}>微信支付</Text>
                    </View>
                )
                break;
            case 3:
                payBtn = (
                    <View style={styles.cardHeaderTextBox}>
                        <IconFont.Entypo name={'wallet'} size={16} color={'#FF651D'} style={styles.cardHeaderTextIcon}/>
                        <Text style={[styles.cardHeaderText, {color: '#FF651D'}]}>余额支付</Text>
                    </View>
                )
                break;
            default:
                break;
        }
        return payBtn
    }

    // 更多车辆的选择返回数据
    getCars = (item) => {
        this.setState({
            carsList: item.carsList,
            carsDate: item.carsDate,
            carsTime: item.carsTime,
            weight: item.weight,
            carsNum: item.carsNum,
        })
    }

    // ---------------------------------- 提交部分 ----------------------------------

    // 点击支付服务费
    onSubmitWindow = () => {
        let {payTypeArray} = this.state;
        let actions = payTypeArray.map((item) => {
            return {
                title: item.name,
                onPress: () => {
                    this.setState({
                        payType: item.key,
                    }, () => this.onSubmit(1))
                    ActionManager.hide()
                }
            }
        });
        let params = {
            actions,
            option: {panGestureEnabled: false}
        }
        ActionManager.show(params);
    }

    // 点击接单
    onSubmit = async (type) => {
        Keyboard.dismiss();
        let {item, onRefresh} = this.params;
        let {dataSource, carsSource, payType, carsList, carsDate, carsTime, weight, carsNum, price} = this.state;
        if (carsNum == 0) {
            ToastManager.success('请添加接单车辆');
            return
        }
        if (payType == 0 && type == 1) {
            ToastManager.success('请选择支付方式');
            return
        }
        // 要使用的车辆并添加到数组里------------------------------------
        let cardsIdArr = [], carsTimeArray = [], _carsDateArray = [], _carsTimeArray = [];
        carsList.map((item, index) => {
            if (item == true) {
                cardsIdArr.push(carsSource[index].id)
                carsTimeArray.push(carsDate[index] + ' ' + carsTime[index])
                _carsDateArray.push(carsDate[index])
                _carsTimeArray.push(carsTime[index])
            }
        })
        // 效验要使用的车辆时间------------------------------------
        if (_carsDateArray.includes("") || _carsTimeArray.includes("")) {
            ToastManager.message('请填写完整所有要申请接单车辆的预计到达时间');
            return
        }

        let url = ServicesApi.PAY_CONTROLLER_SERVICE_PAY;
        let data = {
            goods_id: item.id, // 货源id
            service_charge: (price * weight) * (dataSource.service_percentage / 100), // 服务费金额
            carrier_receipt_weight: weight, // 承运吨数
            carrier_transportation_unit_price: price, // 运费单价
            carrier_transportation_total_price: price * weight, // 运费总计
            carrier_vehicle_num: carsNum, // 承运车数量
            carrier_vehicle_id: cardsIdArr.join(','), // 承运车id
            account_type: payType, // 1、支付宝，2、微信、3：余额
            carrier_vehicle_arrival_time: carsTimeArray.join(','),
        }
        if (payType == 3) {
            let password = '';
            let payData = {
                url,
                data,
            };
            let detail = (
                <TextInput
                    secureTextEntry={true} 
                    style={styles.passwordInput}
                    placeholder={'请输入'}
                    placeholderTextColor={'#999'}
                    onChangeText={(text) => password = text}
                />
            );
            let params = {
                title: '请输入支付密码',
                detail,
                actions: [
                    {title: '取消'},
                    {
                        title: '确认',
                        titleStyle: {color: Predefine.themeColor},
                        onPress: () => this.checkPassword(password, payData)
                    },
                ]
            };
            AlertManager.show(params);
            
        } else {
            let result = await Services.post(url, data);
            if (result.code == StatusCode.SUCCESS_CODE) {
                if (dataSource.platform_operate_mode == 1) {
                    ToastManager.message(result.msg);
                    setTimeout(() => {
                        this._onRefresh();
                        onRefresh && onRefresh()
                        RouterHelper.navigate('', 'SupplyOrderDetail', {item: {id: result.order_id}})
                    }, 1000);
                } else {
                    this.onArousePayment(result.data, result.order_id)
                }
            } else {
                ToastManager.warn(result.msg);
            }
        }
    }

    // 检查余额支付的密码
    checkPassword = async (password, payData) => {
        let url = ServicesApi.PERSONAL_CENTER_CHECK_SECONDARY_PASSWORD;
        let data = {
            secondary_password: password,
        }
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.yuePay(payData)
        } else {
            ToastManager.fail(result.msg);
        }
    }

    // 余额支付
    yuePay = async (payData) => {
        let {onRefresh} = this.params;
        let result = await Services.post(payData.url, payData.data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.message(result.msg);
            setTimeout(() => {
                this._onRefresh();
                onRefresh && onRefresh();
                RouterHelper.navigate('', 'SupplyOrderDetail', {item: {id: result.order_id}})
            }, 1000);
        } else {
            ToastManager.warn(result.msg);
        }
    }

    // 唤起alipay或者wechat的支付
    onArousePayment = async (data, id) => {
        let {onRefresh} = this.params;
        let {payType} = this.state;
        let type = '';
        if (payType === 1) {
            type = Constants.PAY_ALIPAY
        } else if (payType === 2) {
            type = Constants.PAY_WECHAT
        }
        let result = await PayManager.pay(type, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.message(result.msg);
            setTimeout(() => {
                this._onRefresh();
                onRefresh && onRefresh()
                RouterHelper.navigate('', 'SupplyOrderDetail', {item: {id: id}})
            }, 1000);
        } else {
            ToastManager.message(result.msg);
        }
    }

    // ---------------------------------- 页面部分 ----------------------------------

    // 被选择的每一个车辆信息卡片
    cardListView = () => {
        let {carsSource, carsList, carsDate, carsTime, weight, carsNum} = this.state;
        if (carsSource.length == 0) {
            return (
                <View style={styles.noCar}>
                    <Text style={styles.noCarText} numberOfLines={1}>*暂无符合要求的车辆</Text>
                </View>
            )
        }
        let contain = Array.isArray(carsSource) && carsSource.map((item, index) => {
            if (index < 3) {
                return (
                    <TouchableOpacity
                        key={index}
                        style={[styles.carCard, carsList[index] == true && {backgroundColor: '#CAF2F7'}]}
                        onPress={() => this._choiceCar(item, index)}
                    >
                        <View style={styles.carCardHeader}>
                            <View style={styles.carCardHeaderBall}/>
                            <Text style={[styles.carCardLineText, styles.carCardHeaderText]}>
                                {item.status == 0 && '审核中'}
                                {item.status == 1 && '闲置中'}
                                {item.status == 2 && '运营中'}
                            </Text>
                            <Button
                                style={styles.carCardHeaderBtn}
                                title={'选择车辆'}
                                titleStyle={styles.carCardHeaderBtnTitle}
                                resizeMode={'cover'}
                                backgroundImage={Images.img_btn_bg1}
                                onPress={() => this._choiceCar(item, index)}
                            />
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText} numberOfLines={1}>车牌号：{item.vehicle_number}</Text>
                            <Text style={styles.carCardLineText} numberOfLines={1}>司机姓名：{item.driver_name}</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText} numberOfLines={1}>车辆类型：{item.vehicle_type_text}</Text>
                            <Text style={styles.carCardLineText} numberOfLines={1}>车辆载重：{item.vehicle_weight}吨</Text>
                        </View>
                        <View style={styles.carCardTimeLine}>
                            <Text style={styles.carCardLineTitle}>预计到达时间：</Text>
                            <View style={styles.carCardLine}>
                                <TouchableOpacity style={styles.carCardLineInputBox} onPress={() => this._choiceDate(index)}>
                                    <Text style={styles.carCardLineInput} numberOfLines={1}>{carsDate[index] && carsDate[index] || '请点击选择日期'}</Text>
                                    <IconFont.Fontisto name={'date'} size={14} color={DominantColor}/>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.carCardLineInputBox} onPress={() => this._choiceTime(index)}>
                                    <Text style={styles.carCardLineInput} numberOfLines={1}>{carsTime[index] && carsTime[index] || '请点击选择时间'}</Text>
                                    <IconFont.MaterialIcons name={'access-time'} size={14} color={DominantColor}/>
                                </TouchableOpacity>    
                            </View>
                        </View>
                        {
                            carsList[index] == true &&
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.carCardImg}
                                source={Images.icon_choice_car}
                            />
                        }
                    </TouchableOpacity>
                );
            }
        })
        return (
            <View style={styles.carView}>
                <View style={[Predefine.RCB, styles.carViewHeader]}>
                    <Text style={styles.carViewHeaderText}>接单车辆<Text style={{color: '#E71426'}}>*</Text>:</Text>
                </View>
                {contain}
                {
                    carsSource.length > 3 &&  
                    <Button
                        style={styles.btn}
                        title={'更多车辆'}
                        titleStyle={styles.btnTitle}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => RouterHelper.navigate('', 'MoreCars', {
                            data: carsSource, 
                            carsList: carsList,
                            carsDate: carsDate,
                            carsTime: carsTime,
                            weight: weight, 
                            carsNum: carsNum,
                            getCars: (item) => this.getCars(item)
                        })}
                    />
                }
            </View>
        )
    }

    // 选择车辆的card
    supplyCarsView = (dataSource) => {
        let {payType} = this.params;
        let {weight, carsNum, price} = this.state;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBox}>
                        <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                        <Text style={styles.cardHeaderTitle}>详细地址</Text>
                    </View>
                </View>
                <View style={styles.carForm}>
                    <Text style={styles.carFromTitle}>接单吨数<Text style={{color: '#E71426'}}>*</Text>：</Text>
                    <Text style={[styles.carFromText, {color: '#333'}]} numberOfLines={1}>{}</Text>
                    <Text style={styles.carFromText}>{weight}吨</Text>
                </View>
                <View style={styles.carForm}>
                    <Text style={styles.carFromText}>货源剩余未承运吨数：<Text style={{color: '#FF651D'}}>{dataSource.remanent_weight}</Text>吨</Text>
                </View>
                {
                    payType == '竞价发布' &&
                    <View style={styles.carForm}>
                        <Text style={styles.carFromTitle}>接单报价<Text style={{color: '#E71426'}}>*</Text>：</Text>
                        <TextInput
                            defaultValue={price+''}
                            style={styles.carFromInput}
                            placeholder={'请输入'}
                            placeholderTextColor={'#999'}
                            keyboardType={'phone-pad'}
                            onChangeText={(text) => {
                                let newText = text.replace(/[^\d]+/, ''); // 只要数字
                                this.setState({ price: newText })
                            }}
                        />
                        <Text style={styles.carFromText}>/吨</Text>
                    </View>
                }
                {this.cardListView(this.state)}
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>可承接总吨数：</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}><Text style={{color: '#FF651D'}}>{weight}</Text>吨</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>承接车辆数：</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{carsNum}辆</Text>
                </View>
            </View>
        )
    }

    // 底部的btn视图
    bottomBtnView = (dataSource) => {
        let btn1 = (
            <Button
                style={styles.btn}
                title={'支付服务费'}
                titleStyle={styles.btnTitle}
                resizeMode={'cover'}
                backgroundImage={Images.img_btn_bg1}
                onPress={() => this.onSubmitWindow()}
            />
        )
        let btn2 = (
            <Button
                style={styles.btn}
                title={'提交报价审核'}
                titleStyle={styles.btnTitle}
                resizeMode={'cover'}
                backgroundImage={Images.img_btn_bg1}
                onPress={() => this.onSubmit(2)}
            />
        )
        let btn = '';
        if (dataSource.receipt_type == 1) {
            btn = btn1
        } else {
            dataSource.platform_operate_mode == 2 ? btn = btn1 : btn = btn2
        }
        return btn
    }

    render() {
        let {loading, dataSource, weight, price} = this.state;
        let data = {
            weight: weight,
            price: price,
            distance: dataSource.distance,
            total: price * weight,
            percentage: (price * weight) * (dataSource.service_percentage / 100),
            showPercentage: (dataSource.receipt_type == 2 && dataSource.platform_operate_mode == 1) ? false : true,
        }
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar
                    title={'申请接单'}
                    style={styles.navigationBar}
                />
                <View style={styles.headerPadding}/>
                <ScrollView refreshControl={<ListHeaderLoading onRefresh={this._onRefresh} refreshing={this.state.isRefreshing} />}>
                    <Text style={styles.type}>{dataSource.platform_operate_mode == 1 ? '竞价' : '定价'}</Text>
                    {this.supplyCarsView(dataSource)}
                    <ApplySupplyCard
                        item={data}
                        {...this.props}
                    />
                    <View style={styles.btnBox}>
                        <Text style={styles.btnBoxHint}>温馨提示：支付服务费之后可查看货单详细信息</Text>
                        {this.bottomBtnView(dataSource)}
                    </View>
                </ScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f9f9f9',
    },
    navigationBar: {
        zIndex: 10,
        position: 'absolute',
    },
    headerPadding: {
        height: NAV_BAR_HEIGHT,
    },

    type: {
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 1,
        fontSize: 12,
        color: DominantColor,
        backgroundColor: '#F6FEFF',
        paddingVertical: 3,
        paddingHorizontal: 6,
        borderRadius: 2,
    },

    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    cardHeaderBox: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderTitle: {
        color: '#333',
        fontSize: 13,
        fontWeight: 'bold',
    },
    cardHeaderTextBox: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    cardHeaderText: {
        fontSize: 12,
        color: '#999',
    },
    cardHeaderTextIcon: {
        marginRight: 6,
    },
    cardHeaderImg: {
        width: 6,
        height: 12,
        marginLeft: 6,
    },
    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
    },

    carForm: {
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    carFromTitle: {
        color: '#333',
        fontSize: 14,
    },
    carFromInput: {
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        height: 26,
        width: Predefine.screenWidth - 200,
        paddingVertical: 0,
        paddingHorizontal: 10,
        fontSize: 14,
        marginRight: 6,
    },
    carFromText: {
        color: '#999',
        fontSize: 14,
    },

    btnBox: {
        padding: 15,
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    btnBoxHint:{
        color:'#EA2222',
        marginBottom: 10
    },
    btn: {
        backgroundColor: DominantColor,
    },

    carView: {
        marginTop: 10,
        borderTopWidth: Predefine.minPixel,
        borderTopColor: '#e1e1e1',
    },
    carViewHeader: {
        height: 40,
    },
    carViewHeaderText: {
        fontSize: 14,
    },

    carCard: {
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        marginBottom: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    carCardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 25,
    },
    carCardHeaderBall: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
        backgroundColor: DominantColor,
    },
    carCardHeaderText: {
        color: DominantColor,
        fontSize: 14,
    },
    carCardHeaderBtn: {
        backgroundColor: DominantColor,
        paddingHorizontal: 0,
        paddingVertical: 0,
        height: 26,
        width: 80,
    }, 
    carCardHeaderBtnTitle: {
        fontSize: 13,
    },
    carCardLine: {
        height: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineText: {
        flex: 1,
        fontSize: 14,
        color: '#333',
    },
    carCardLineTitle: {
        lineHeight: 30,
        fontSize: 14,
        color: '#333',
    },
    carCardLineInputBox: {
        height: 26,
        borderWidth: 1,
        borderColor: DominantColor,
        borderRadius: 5,
        marginHorizontal: 5,
        flex: 1,
        paddingHorizontal: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineInput: {},
    carCardImg: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: 28,
        height: 30,
    },
    passwordInput: {
        marginTop: 10,
        width: 200,
        height: 40,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        paddingHorizontal: 10,
        paddingVertical: 0,
        textAlign: 'center',
    },
});