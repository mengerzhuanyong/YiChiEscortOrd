/**
 * 易驰护运 - Supply
 * http://menger.me
 * 动画
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard, Animated, Easing } from 'react-native'
import {inject, observer} from 'mobx-react'


@inject('loginStore')
@observer
export default class Animateds extends React.Component {

    constructor(props) {
        super(props);
        this.spinValue = new Animated.Value(0)
    }

    componentDidMount () {
        this.spin()
    }

    spin () {
        this.spinValue.setValue(0)
        // Animated.spring(
        Animated.decay(
        // Animated.parallel(
        // Animated.sequence(
        // Animated.stagger(
        // Animated.timing(
            this.spinValue,
            {
                // toValue: 1,
                // duration: 4000,
                // easing: Easing.linear
                // toValue: 1,                         // 透明度最终变为1，即完全不透明
                // friction: 8,                        // 弹性系数
                // tension: 35  
                toValue: 1,                         // 透明度最终变为1，即完全不透明
                velocity: 0.1                       // 初始速度
            }
        ).start()
        // ).start(() => this.spin()) // 一轮动画完成后的回调，这里传spin可以形成无限动画
    }
    
    render() {
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
        return (
            <PageContainer style={styles.container}>
                <NavigationBar 
                    title={'动画'}
                />
                <Animated.View
                    style={{
                        width: 227,
                        height: 200,
                        backgroundColor: 'powderblue',
                        // transform: [{rotate: spin}]
                        opacity: this.spinValue, 
                    }}
                    // source={{uri: 'https://s3.amazonaws.com/media-p.slid.es/uploads/alexanderfarennikov/images/1198519/reactjs.png'}}
                />
            </PageContainer>
        );
    }
}

const styles = StyleSheet.create({
    a: {
        
    },
});