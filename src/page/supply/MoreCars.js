/**
 * 易驰护运 - MoreCars
 * http://menger.me
 * 更多车辆
 * @桓桓
 */

'use strict';

import React from 'react'
import {StyleSheet, Text, ScrollView, TouchableOpacity, View, Keyboard} from 'react-native'
import {
    IconFont,
    ApplyMessageCard,
    ApplySupplyCard,
} from '../../components'
import {inject, observer} from 'mobx-react'
import RouterHelper from '../../routers/RouterHelper';

@inject('loginStore')
@observer
export default class MoreCars extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            carsList: this.params.carsList || [],
            carsDate: this.params.carsDate || [],
            carsTime: this.params.carsTime || [],
            weight: this.params.weight || 0,
            carsNum: this.params.carsNum || 0,
        }
    }

    componentDidMount () {
        console.log('this.params.carsList------------->', this.params)
    }

    _choiceCar = (item, index) => {
        let {carsList, weight, carsNum} = this.state;
        let arr = carsList.slice()
        if (arr[index] === true) {
            arr.splice(index, 1, false);
            this.setState({
                weight: weight*1 - item.vehicle_weight*1,
                carsNum: carsNum - 1,
            })
        } else {
            arr.splice(index, 1, true);
            this.setState({
                weight: weight*1 + item.vehicle_weight*1,
                carsNum: carsNum + 1,
            })
        }
        this.setState({carsList: arr})
    }

    _choiceDate = (carsIndex) => {
        let {carsDate} = this.state;
        let arr = carsDate.slice()
        let params = {
            onPress: (value) => {
                let date = value.year+'-'+value.month+'-'+value.day;
                if (arr.length == 0) {
                    carsDate.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.push(date)
                        } else {
                            arr.push('')
                        }
                    })
                } else {
                    carsDate.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.splice(index, 1, date)
                        }
                    })
                }
                this.setState({carsDate: arr})
            },
            option: {
                panGestureEnabled: false,
            }
        }
        ActionManager.showWheelDate(params);
    }

    _choiceTime = (carsIndex) => {
        let {carsTime} = this.state;
        let arr = carsTime.slice()
        let params = {
            onPress: (value) => { 
                if (arr.length == 0) {
                    carsTime.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.push(value.hour+':'+value.minute)
                        } else {
                            arr.push('')
                        }
                    })
                } else {
                    carsTime.map((item, index) => {
                        if ( carsIndex == index ) {
                            arr.splice(index, 1, value.hour+':'+value.minute)
                        }
                    })
                }
                this.setState({carsTime: arr})
            },
            option: {
                panGestureEnabled: false,
            }
        }
        ActionManager.showTimeContent(params);
    }

    onSubmit = () => {
        let {getCars} = this.params;
        let {carsList, carsDate, carsTime, weight, carsNum} = this.state;
        let item = {
            carsList: carsList,
            carsDate: carsDate,
            carsTime: carsTime,
            weight: weight,
            carsNum: carsNum,
        };
        getCars && getCars(item);
        RouterHelper.goBack();
    }

    carsListView = () => {
        let {data} = this.params;
        let {carsList, carsDate, carsTime} = this.state;
        let content = data.map((item, index) => {
            return (
                <TouchableOpacity
                    key={index}
                    style={[styles.carCard, carsList[index] == true && {backgroundColor: '#CAF2F7'}]}
                    onPress={() => this._choiceCar(item, index)}
                >
                    <View style={styles.carCardHeader}>
                        <View style={styles.carCardHeaderBall}/>
                        <Text style={[styles.carCardLineText, styles.carCardHeaderText]}>
                            {item.status == 0 && '审核中'}
                            {item.status == 1 && '闲置中'}
                            {item.status == 2 && '运营中'}
                        </Text>
                        <Button
                            style={styles.carCardHeaderBtn}
                            title={'选择车辆'}
                            titleStyle={styles.carCardHeaderBtnTitle}
                            resizeMode={'cover'}
                            backgroundImage={Images.img_btn_bg1}
                            onPress={() => this._choiceCar(item, index)}
                        />
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText} numberOfLines={1}>车牌号：{item.vehicle_number}</Text>
                        <Text style={styles.carCardLineText} numberOfLines={1}>司机姓名：{item.driver_name}</Text>
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText} numberOfLines={1}>车辆类型：{item.vehicle_type_text}</Text>
                        <Text style={styles.carCardLineText} numberOfLines={1}>车辆载重：{item.vehicle_weight}吨</Text>
                    </View>
                    <View style={styles.carCardTimeLine}>
                        <Text style={styles.carCardLineTitle}>预计到达时间：</Text>
                        <View style={styles.carCardLine}>
                            <TouchableOpacity style={styles.carCardLineInputBox} onPress={() => this._choiceDate(index)}>
                                <Text style={styles.carCardLineInput} numberOfLines={1}>{carsDate[index] && carsDate[index] || '请点击选择日期'}</Text>
                                <IconFont.Fontisto name={'date'} size={14} color={DominantColor}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.carCardLineInputBox} onPress={() => this._choiceTime(index)}>
                                <Text style={styles.carCardLineInput} numberOfLines={1}>{carsTime[index] && carsTime[index] || '请点击选择时间'}</Text>
                                <IconFont.MaterialIcons name={'access-time'} size={14} color={DominantColor}/>
                            </TouchableOpacity>    
                        </View>
                    </View>
                    {
                        carsList[index] == true &&
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.carCardImg}
                            source={Images.icon_choice_car}
                        />
                    }
                </TouchableOpacity>
            );
        })
        return content
    }

    render() {
        let {loading, weight,carsNum} = this.state;
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar
                    title={'选择接单车辆'}
                    style={styles.navigationBar}
                />
                <ScrollView>
                    {this.carsListView()}
                </ScrollView>
                <View style={styles.window}>
                    <Text style={styles.windowText} numberOfLines={1}>吨数：{weight}</Text>
                    <Text style={styles.windowText} numberOfLines={1}>数量：{carsNum}</Text>
                </View>
                <View style={styles.btnBox}>
                    <Button
                        style={styles.btn}
                        title={'确认选择'}
                        titleStyle={styles.btnTitle}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => this.onSubmit()}
                    />
                </View>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    carCard: {
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        marginTop: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginHorizontal: 15,
    },
    carCardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 25,
    },
    carCardHeaderBall: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
        backgroundColor: DominantColor,
    },
    carCardHeaderText: {
        color: DominantColor,
        fontSize: 14,
    },
    carCardHeaderBtn: {
        backgroundColor: DominantColor,
        paddingHorizontal: 0,
        paddingVertical: 0,
        height: 26,
        width: 80,
    }, 
    carCardHeaderBtnTitle: {
        fontSize: 13,
    },
    carCardLine: {
        height: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineText: {
        flex: 1,
        fontSize: 14,
        color: '#333',
    },
    carCardLineTitle: {
        lineHeight: 30,
        fontSize: 14,
        color: '#333',
    },
    carCardLineInputBox: {
        height: 26,
        borderWidth: 1,
        borderColor: DominantColor,
        borderRadius: 5,
        marginHorizontal: 5,
        flex: 1,
        paddingHorizontal: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineInput: {},
    carCardImg: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: 28,
        height: 30,
    },

    window: {
        backgroundColor: DominantColor,
        position: 'absolute',
        right: 0,
        bottom: 100,
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
    },
    windowText: {
        color: '#fff',
        fontSize: 15,
    },

    btnBox: {
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    btn: {
        backgroundColor: DominantColor,
    },
});