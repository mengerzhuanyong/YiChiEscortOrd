/**
 * 易驰护运 - SupplyDetail
 * http://menger.me
 * 货源详情
 * @大梦
 */

'use strict';

import React from 'react'
import {ImageBackground, ScrollView, StyleSheet, Text, View,} from 'react-native'
import {
    AddressInfo,
    CargoMessageCard,
    CarMessageCard,
    HorizontalLine,
    IconFont,
    ListHeaderLoading,
    RemarkMessageCard,
    TransportMessageCard,
    VerticalLine,
} from '../../components'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class SupplyDetail extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            isRefreshing: false,
            dataSource: [],
        }
    }

    // 增加CallBack的跳转方法
    _onPressToNavigate = (pageTitle, component, params) => {
        let {loginStore} = this.props;
        RouterHelper.navigate(pageTitle, component, {
            ...params,
            onCallBack: () => loginStore.getLatestUserInfo(),
        });
    };

    componentDidMount() {
        this.requestData(); // 请求详情
    }

    // 请求详情
    requestData = async() => {
        let {item} = this.params;
        let url = ServicesApi.GOODS_GOODS_INFO;
        let data = {
            id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                dataSource: result.data,
                loading: false,
            });
        }
    }

    // 下拉刷新
    _onRefresh = async () => {
        this.setState({isRefreshing: true});
        this.requestData();
        setTimeout(() => this.setState({isRefreshing: false}), 500);
    };

    render() {
        let pageTitle = this.params.pageTitle || '货源详情';
        let {item, payType} = this.params;
        let {loginStore} = this.props;
        let {userInfo} = loginStore;
        let {loading, dataSource} = this.state;
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar 
                    title={pageTitle}
                    style={styles.navigationBar}
                    renderRightAction={[{
                        title: <IconFont.AntDesign name={'customerservice'} size={20} color={'#fff'} />, 
                        onPress: () => RouterHelper.navigate('', 'WebPage', {
                            pageTitle: '联系客服',
                            uri: ServicesApi.CUSTOMER_SERVICE,
                        })
                    }]}
                />
                <View style={styles.headerPadding}/>
                <ScrollView style={styles.scrollView} refreshControl={<ListHeaderLoading onRefresh={this._onRefresh} refreshing={this.state.isRefreshing}/>}>
                    <View style={[styles.contentItemView, styles.contentSmallItemView]}>
                        <View style={styles.contentItemBgView} />
                        <ImageBackground
                            resizeMode={'stretch'}
                            style={styles.detailAddressView}
                            source={Images.img_bg_block_content}
                        >
                            <AddressInfo item={dataSource} style={styles.addressInfoStyle}/>
                            <HorizontalLine style={styles.addressInfoSeparator} />
                            <View style={[Predefine.RCB, styles.cargoInfoCardContent]}>
                                <View style={[Predefine.CCC, styles.cargoInfoCardItemView]}>
                                    <Text style={styles.cargoInfoCardItemValue} numberOfLines={1}>{dataSource.goods_name}</Text>
                                    <View style={[Predefine.RCC, styles.cargoInfoCardItemTitleView]}>
                                        <ImageView source={Images.icon_text} style={styles.cargoInfoCardItemTitleIcon} />
                                        <Text style={styles.cargoInfoCardItemTitle}>货物名称</Text>
                                    </View>
                                </View>
                                <VerticalLine style={styles.cargoInfoCardSeparator} />
                                <View style={[Predefine.CCC, styles.cargoInfoCardItemView, styles.cargoInfoCardItemViewCenter]}>
                                    <Text style={styles.cargoInfoCardItemValue} numberOfLines={1}>{dataSource.goods_dangerous_level_text}</Text>
                                    <View style={[Predefine.RCC, styles.cargoInfoCardItemTitleView]}>
                                        <ImageView source={Images.icon_grid} style={styles.cargoInfoCardItemTitleIcon} />
                                        <Text style={styles.cargoInfoCardItemTitle}>货物类型</Text>
                                    </View>
                                </View>
                                <VerticalLine style={styles.cargoInfoCardSeparator} />
                                <View style={[Predefine.CCC, styles.cargoInfoCardItemView]}>
                                    <Text style={styles.cargoInfoCardItemValue} numberOfLines={1}>{dataSource.remanent_weight}</Text>
                                    <View style={[Predefine.RCC, styles.cargoInfoCardItemTitleView]}>
                                        <ImageView source={Images.icon_rank} style={styles.cargoInfoCardItemTitleIcon} />
                                        <Text style={styles.cargoInfoCardItemTitle}>剩余吨数</Text>
                                    </View>
                                </View>
                            </View>
                        </ImageBackground>
                        <Text style={styles.orderId} numberOfLines={1}>订单号：{dataSource.goods_no}</Text>
                    </View>
                    <CarMessageCard item={dataSource}/>
                    <TransportMessageCard item={dataSource}/>
                    <CargoMessageCard item={dataSource}/>
                    <RemarkMessageCard item={dataSource}/>
                    <View style={styles.btnBox}>
                        {
                            userInfo.id
                            ?
                            (
                                userInfo.is_pass == 1
                                ?
                                <Button
                                    style={styles.btn}
                                    title={'申请接单'}
                                    titleStyle={styles.btnTitle}
                                    resizeMode={'cover'}
                                    backgroundImage={Images.img_btn_bg1}
                                    onPress={() => RouterHelper.navigate('', 'SupplyApply', {item, payType})}
                                />
                                :
                                <Button
                                    style={styles.btn}
                                    title={'认证通过的用户才能申请接单'}
                                    titleStyle={styles.btnTitle}
                                    resizeMode={'cover'}
                                    backgroundImage={Images.img_btn_bg1}
                                    onPress={() => RouterHelper.navigate('', 'ApproveChoice')}
                                />
                            )
                            :
                            <Button
                                style={styles.btn}
                                title={'承运'}
                                titleStyle={styles.btnTitle}
                                resizeMode={'cover'}
                                backgroundImage={Images.img_btn_bg1}
                                onPress={() => RouterHelper.navigate('', 'Login')}
                            />
                        }
                    </View> 
                </ScrollView>
            </PageContainer>
        );
    }
}

const contentWidth = Predefine.screenWidth - 20;
const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navigationBar: {
        zIndex: 10,
        position: 'absolute',
    },
    headerPadding: {
        height: NAV_BAR_HEIGHT,
    },

    // 顶部的卡片
    contentItemView: {
        minHeight: 100,
        marginBottom: 10,
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
    },
    contentSmallItemView: {
        paddingHorizontal: 10,
    },
    contentItemBgView: {
        top: 0,
        left: 0,
        height: 100,
        position: 'absolute',
        width: Predefine.screenWidth,
        backgroundColor: Predefine.themeColor,
    },
    detailAddressView: {
        width: contentWidth,
        paddingVertical: 15,
        paddingHorizontal: 15,
        // height: contentWidth * 0.45,
    },
    addressInfoStyle: {
        marginVertical: 0,
    },
    addressInfoSeparator: {
        marginVertical: 5,
        backgroundColor: Predefine.themeColor,
    },

    // 顶部卡片的底部
    cargoInfoCardContent: {
        marginTop: 10,
    },
    cargoInfoCardItemView: {
        flex: 2,
    },
    cargoInfoCardItemViewCenter: {
        flex: 3,
    },
    cargoInfoCardItemValue: {
        fontSize: 15,
        fontWeight: '700',
        color: Predefine.themeColor,
    },
    cargoInfoCardItemTitleView: {
        marginTop: 5,
    },
    cargoInfoCardItemTitle: {
        fontSize: 14,
        color: '#666',
    },
    cargoInfoCardItemTitleIcon: {
        width: 12,
        height: 12,
        marginRight: 3,
    },
    cargoInfoCardSeparator: {
        width: 1,
        height: 25,
        backgroundColor: Predefine.themeColor,
    },
    orderId: {
        marginTop: 10,
        fontSize: 14,
        color: '#333',
        // textAlign: 'right',
    },

    btnBox: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
    },
    btn: {
        height: 35,
        backgroundColor: DominantColor,
        paddingHorizontal: 0,
        paddingVertical: 0,
    },
    btnTitle: {
        fontSize: 15,
    },
});