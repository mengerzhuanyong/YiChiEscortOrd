/**
 * 易驰护运 - SupplyFilter
 * http://menger.me
 * 货源大厅 - 筛选
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard} from 'react-native'
import {
    IconFont,
    ListView,
    VerticalLine,
    ChoiceTagItem,
    HorizontalLine,
    BannerComponent,
    HotNewsComponent,
    HomeResourceItem,
    ListHeaderLoading,
} from '../../components'
import {inject, observer} from 'mobx-react'
import RouterHelper from '../../routers/RouterHelper';

@inject('loginStore')
@observer
export default class SupplyFilter extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            kindDataSource: [],
            // 类型数组
            vehicleTypeArray: [], // 要求车辆类型
            vehicleLengthArray: [], // 车长
            vehicleDischargeTextArray: [], // 排放量
            vehicleTankTypeArray: [], // 要求罐型
            goodsDangerousLevelArray: [], // 货物类型
            appPlatformPaymentTypeTextArray: [], // 支付方式
            shipperAccountPeriodTextArray: [], // 结算周期
            remanentWeightArray: [], // 最小剩余吨数
        }
    }

    componentDidMount () {
        this.requestKindData()
        this.setFilterVal()
    }

    setFilterVal = () => { // 如果之前筛选过就把值附上去
        let {filter} = this.params;
        if (
            filter.vehicleTypeArrayText || 
            filter.vehicleLengthArrayText ||
            filter.vehicleDischargeTextArrayText ||
            filter.vehicleTankTypeArrayText ||
            filter.goodsDangerousLevelArrayText ||
            filter.appPlatformPaymentTypeTextArrayText ||
            filter.shipperAccountPeriodTextArrayText ||
            filter.remanentWeightArrayText
        ) {
            this.setState({
                vehicleTypeArray: filter.vehicleTypeArrayText.split(","),
                vehicleLengthArray: filter.vehicleLengthArrayText.split(","),
                vehicleDischargeTextArray: filter.vehicleDischargeTextArrayText.split(","),
                vehicleTankTypeArray: filter.vehicleTankTypeArrayText.split(","),
                goodsDangerousLevelArray: filter.goodsDangerousLevelArrayText.split(","),
                appPlatformPaymentTypeTextArray: filter.appPlatformPaymentTypeTextArrayText.split(","),
                shipperAccountPeriodTextArray: filter.shipperAccountPeriodTextArrayText.split(","),
                remanentWeightArray: filter.remanentWeightArrayText.split(","),
            })
        }
    }

    // 请求筛选条件
    requestKindData = async() => {
        let url = ServicesApi.SETUP_CONFIG_LIST;
        let data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                kindDataSource: result.data,
                loading: false,
            });
        }
    }

    // 多选
    onPressMultiSelect = (key, data) => {
        let dataTemp = data.slice();
        if (dataTemp.indexOf(key) > -1) {
            let _index = dataTemp.indexOf(key)
            dataTemp.splice(_index,1)
        } else {
            dataTemp.push(key)
        }
        this.upDataState(data, dataTemp)
    }
    
    upDataState = (data, dataTemp) => {
        let {
            vehicleTypeArray, vehicleLengthArray,
            vehicleDischargeTextArray,vehicleTankTypeArray,
            goodsDangerousLevelArray,appPlatformPaymentTypeTextArray,
            shipperAccountPeriodTextArray,remanentWeightArray,
        } = this.state;
        if (data == vehicleTypeArray) {
            this.setState({
                vehicleTypeArray: dataTemp,
            })
        } else if (data == vehicleLengthArray) {
            this.setState({
                vehicleLengthArray: dataTemp,
            })
        } else if (data == vehicleDischargeTextArray) {
            this.setState({
                vehicleDischargeTextArray: dataTemp,
            })
        } else if (data == vehicleTankTypeArray) {
            this.setState({
                vehicleTankTypeArray: dataTemp,
            })
        } else if (data == goodsDangerousLevelArray) {
            this.setState({
                goodsDangerousLevelArray: dataTemp,
            })
        } else if (data == appPlatformPaymentTypeTextArray) {
            this.setState({
                appPlatformPaymentTypeTextArray: dataTemp,
            })
        } else if (data == shipperAccountPeriodTextArray) {
            this.setState({
                shipperAccountPeriodTextArray: dataTemp,
            })
        } else if (data == remanentWeightArray) {
            this.setState({
                remanentWeightArray: dataTemp,
            })
        }
    }

    vehicleType = (kindDataSource) => {
        let {vehicleTypeArray} = this.state;
        let contain = kindDataSource.vehicle_type_text && kindDataSource.vehicle_type_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, vehicleTypeArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, vehicleTypeArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, vehicleTypeArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>要求车辆类型<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    vehicleLength = (kindDataSource) => {
        let {vehicleLengthArray} = this.state;
        let contain = kindDataSource.vehicle_length_text && kindDataSource.vehicle_length_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, vehicleLengthArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, vehicleLengthArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, vehicleLengthArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>车长<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    vehicleDischargeText = (kindDataSource) => {
        let {vehicleDischargeTextArray} = this.state;
        let contain = kindDataSource.vehicle_discharge_text && kindDataSource.vehicle_discharge_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, vehicleDischargeTextArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, vehicleDischargeTextArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, vehicleDischargeTextArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>排放量<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    vehicleTankType = (kindDataSource) => {
        let {vehicleTankTypeArray} = this.state;
        let contain = kindDataSource.vehicle_tank_type_text && kindDataSource.vehicle_tank_type_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, vehicleTankTypeArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, vehicleTankTypeArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, vehicleTankTypeArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>要求罐型<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    goodsDangerousLevel = (kindDataSource) => {
        let {goodsDangerousLevelArray} = this.state;
        let contain = kindDataSource.goods_dangerous_level_text && kindDataSource.goods_dangerous_level_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, goodsDangerousLevelArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, goodsDangerousLevelArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, goodsDangerousLevelArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>货物类型<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    appPlatformPaymentTypeText = (kindDataSource) => {
        let {appPlatformPaymentTypeTextArray} = this.state;
        let contain = kindDataSource.app_platform_payment_type_text && kindDataSource.app_platform_payment_type_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, appPlatformPaymentTypeTextArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, appPlatformPaymentTypeTextArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, appPlatformPaymentTypeTextArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>支付方式<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    shipperAccountPeriodText = (kindDataSource) => {
        let {shipperAccountPeriodTextArray} = this.state;
        let contain = kindDataSource.app_shipper_account_period_text && kindDataSource.app_shipper_account_period_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, shipperAccountPeriodTextArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, shipperAccountPeriodTextArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, shipperAccountPeriodTextArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>支付方式<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    remanentWeight = (kindDataSource) => {
        let {remanentWeightArray} = this.state;
        let contain = kindDataSource.remanent_weight_text && kindDataSource.remanent_weight_text.map((item, index) => {
            return (
                <TouchableOpacity key={index} onPress={() => this.onPressMultiSelect(item.key, remanentWeightArray)}>
                    <ChoiceTagItem 
                        title={item.val} 
                        style={[styles.choiceTagItem, remanentWeightArray.indexOf(item.key) > -1 && {backgroundColor: '#E4F9F6'}]} 
                        titleStyle={[styles.choiceTagItemTitle, remanentWeightArray.indexOf(item.key) > -1 && {color: '#00BFCC'}]}
                    />      
                </TouchableOpacity>
            )
        })
        return (
            <View style={styles.kindView}>
                <View style={styles.kindViewTitleBox}>
                    <Text style={styles.kindViewTitle}>最小剩余吨数<Text style={{color: '#999', fontSize: 14}}>（可多选）</Text></Text>
                </View>
                <View style={styles.kindViewBody}>{contain}</View>
            </View>
        )
    }

    // 重置
    onReset = () => {
        let {callback} = this.params;
        let filter = {} // 直接清空筛选条件
        callback && callback(filter)
        RouterHelper.goBack()
    }
    
    // 筛选
    onFilter = () => {
        let {callback} = this.params;
        let {vehicleTypeArray, vehicleLengthArray, vehicleDischargeTextArray, vehicleTankTypeArray, goodsDangerousLevelArray, appPlatformPaymentTypeTextArray, shipperAccountPeriodTextArray, remanentWeightArray} = this.state;
        let filter = {
            vehicleTypeArrayText: vehicleTypeArray.join(","),
            vehicleLengthArrayText: vehicleLengthArray.join(","),
            vehicleDischargeTextArrayText: vehicleDischargeTextArray.join(","),
            vehicleTankTypeArrayText: vehicleTankTypeArray.join(","),
            goodsDangerousLevelArrayText: goodsDangerousLevelArray.join(","),
            appPlatformPaymentTypeTextArrayText: appPlatformPaymentTypeTextArray.join(","),
            shipperAccountPeriodTextArrayText: shipperAccountPeriodTextArray.join(","),
            remanentWeightArrayText: remanentWeightArray.join(",")
        }
        callback && callback(filter)
        RouterHelper.goBack()
    }
    
    render() {
        let {loading, kindDataSource} = this.state;
        return (
            <PageContainer 
                loading={loading} 
                style={styles.container} 
                fitIPhoneX={true} 
                fitNotchedScreen={Predefine.isNotchedScreen}
            >
                <NavigationBar
                    title={'货源大厅'}
                    style={[styles.navigationBarStyle]}
                />
                <ScrollView>
                    {this.vehicleType(kindDataSource)}
                    {this.vehicleLength(kindDataSource)}
                    {this.vehicleDischargeText(kindDataSource)}
                    {this.vehicleTankType(kindDataSource)}
                    {this.goodsDangerousLevel(kindDataSource)}
                    {this.appPlatformPaymentTypeText(kindDataSource)}
                    {this.shipperAccountPeriodText(kindDataSource)}
                    {this.remanentWeight(kindDataSource)}
                </ScrollView>
                <View style={styles.bottom}>
                    <Button
                        style={[styles.btn, styles.btn1]}
                        title={'重置'}
                        titleStyle={styles.btnTitle1}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => this.onReset()}
                    />
                    <Button
                        style={styles.btn}
                        title={'筛选'}
                        titleStyle={styles.btnTitle2}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => this.onFilter()}
                    />
                </View>  
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    kindView: {

    },
    kindViewTitleBox: {
        height: 40,
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    kindViewTitle: {
        color: '#333',
        fontSize: 15,
    },
    kindViewBody: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        paddingLeft: 10,
    },
    choiceTagItem: {
        height: 36,
        width: SCREEN_WIDTH / 4 - 15,
        marginHorizontal: 5,
        marginBottom: 10,
    },

    bottom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTopWidth: Predefine.minPixel,
        borderTopColor: '#e1e1e1',
    },
    btn: {
        width: SCREEN_WIDTH / 2,
        borderRadius: 0,
        backgroundColor: DominantColor,
    },
    btn1: {
        backgroundColor: '#fff',
    },
    btnTitle1: {
        color: '#333',
    },
});