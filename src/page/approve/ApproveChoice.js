/**
 * 易驰护运 - Approve
 * http://menger.me
 * 承运商认证
 * @桓桓
 */

'use strict';
import React from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity, View, Keyboard, ListView} from 'react-native'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class ApproveChoice extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
        }
    }

    componentDidMount() {

    }

    onPressItem = (type) => {
        type === 'company'
            ? RouterHelper.navigate('', 'ApproveCompany', {type})
            : RouterHelper.navigate('', 'ApproveUser', {type})
    }

    render() {
        let {loading} = this.state;
        return (
            <PageContainer
                loading={loading}
                fitNotchedScreen={Predefine.isNotchedScreen}
                style={styles.container}
            >
                <NavigationBar 
                    title={'承运商认证'} 
                    style={styles.navigationBar}
                />
                <Text style={styles.title} numberOfLines={1}>请选择您要认证的身份类型</Text>
                <View style={styles.body}>
                    <TouchableOpacity style={styles.item} onPress={() => this.onPressItem('company')}>
                        <ImageView resizeMode={'contain'} style={styles.itemImg} source={Images.icon_company_logo}/>
                        <Text style={styles.itemText} numberOfLines={1}>公司</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={() => this.onPressItem('user')}>
                        <ImageView resizeMode={'contain'} style={styles.itemImg} source={Images.icon_user_logo}/>
                        <Text style={styles.itemText} numberOfLines={1}>个人</Text>
                    </TouchableOpacity>
                </View>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    title: {
        marginVertical: 60,
        fontSize: 15,
        color: '#333',
        textAlign: 'center',
    },
    body: {
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    item: {
        width: 150,
        height: 200,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: DominantColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemImg: {
        width: 60,
        height: 60,
        marginBottom: 10,
    },
    itemText: {
        fontSize: 15,
        color: DominantColor,
    },
});