/**
 * 易驰护运 - Approve
 * http://menger.me
 * 承运商认证
 * @桓桓
 */

'use strict';
import React from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity, View, Keyboard, ListView} from 'react-native'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view"
import loginStyle from '../../style/loginStyle'
import {FormValidation} from '../../utils/validation/FormValidation'
import { LargePicture } from '../../components';
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class Approve extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            dataSource: [
                {bond: 'id_card_name', name: '身份证姓名', data: '', placeholder: '请输入身份证姓名', type: 'input'},
                {bond: 'id_card_address', name: '身份证地址', data: '', placeholder: '请输入身份证地址', type: 'input'},
                {bond: 'id_card_no', name: '身份证号码', data: '', placeholder: '请输入身份证号码', type: 'input'},
                {bond: 'id_card_front', name: '身份证正面照：', data: '', type: 'img'},
                {bond: 'id_card_back', name: '身份证反面照：', data: '', type: 'img'},
            ],

            sgs_type: 0, // 0公司 1个人
        }
    }

    componentDidMount() {
        this.updateUserInfo()
    }

    // 把之前的数据放进页面
    updateUserInfo = () => {
        let {loginStore} = this.props;
        let {userInfo, getJSDataSources} = loginStore;
        console.log('getJSDataSources(userInfo)------------->', getJSDataSources(userInfo))
        let { dataSource } = this.state;
        let dataSourceTemp = dataSource.slice();
        dataSourceTemp.forEach((item) => {
            switch (item.type) {
                case 'input':
                case 'img':
                    this.updateState(item.bond, userInfo[item.bond]);
                    break;
                default:
                    break;
            }
        });
        this.setState({
            dataSource: dataSourceTemp,
            loading: false,
        });
    };

    // 时间选择
    showActionTimePicker = (type) => {
        Keyboard.dismiss();
        let params = {
            onPress: (value) => {
                if (type == 1) {
                    this.setState({
                        year1: value.year,
                        month1: value.month,
                        day1: value.day,
                    })    
                } else if (type == 2) {
                    this.setState({
                        year2: value.year,
                        month2: value.month,
                        day2: value.day,
                    }) 
                } else if (type == 3) {
                    this.setState({
                        year3: value.year,
                        month3: value.month,
                        day3: value.day,
                    }) 
                }
            },
            option: {
                panGestureEnabled: false,
            }
        }
        ActionManager.showWheelDate(params);
    };

    // 地址选择
    showActionAreaPicker = (type) => {
        Keyboard.dismiss();
        let params = {
            // showArea: false,
            onCallBack: (selectedData, addressText) => {
                this.setState({
                    province: selectedData.province.name,//省
                    city: selectedData.city.name,//市
                    district: selectedData.area.name,//区
                }); 
            },
            option: {
                panGestureEnabled: false,
            }
        }
        ActionManager.showCityTree(params);
    };

    // 上传图片
    onPressUpImg = async (bond) => {
        Keyboard.dismiss();
        let _result = await MediaManager.showActionPicker({
            includeBase64: true,
        });
        if (_result && _result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.loading('上传中...', {duration: 9999999});
            const upRes = await Services.uploadQiNiu(_result.data);
            if (upRes.code === StatusCode.SUCCESS_CODE) {
                ToastManager.hide();
                ToastManager.success('上传成功');
                this.updateState(bond, ServicesApi.QI_NIU_HOST + upRes.data[0].key, true);
            } else {
                ToastManager.fail('上传失败');
            }
        }
    };

    // 删除
    deleteImg = (bond) => {
        Keyboard.dismiss();
        this.updateState(bond, '')
    }

    // 修改数据
    updateState = (bond, data, refresh) => {
        let { dataSource } = this.state;
        let index = dataSource.findIndex((item) => item.bond === bond);
        if (index !== -1) {
            let newItem = dataSource[index];
            newItem['data'] = data;
            if (refresh) {
                let newDataSource = dataSource.slice();
                newDataSource[index] = newItem;
                this.setState({ dataSource: newDataSource });
            } else { 
                dataSource[index] = newItem;
            }
        }
    };

    // 渲染的具体页面
    ListView = (dataSource) => {
        let {province, city, district} = this.state;
        let contain = dataSource.map((item, index) => {
            if (item.type == 'input') {
                return (
                    <View key={index} style={[styles.inputLine, item.space == true && {borderBottomWidth: 0}]}>
                        <Text style={styles.inputLineLeft} numberOfLines={1}>{item.name}</Text>
                        <TextInput
                            defaultValue={item.data}
                            style={styles.inputLineRight}
                            placeholder={item.placeholder}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.updateState(item.bond, text)} 
                        />
                    </View>
                )
            } else if (item.type == 'img') {
                return (
                    <View key={index} style={[styles.inputLine, {height: 90}]}>
                        <Text style={styles.inputLineLeft} numberOfLines={1}>{item.name}</Text>
                        <TouchableOpacity onPress={() => this.onPressUpImg(item.bond)}>
                            {
                                item.data
                                ?
                                <View>
                                    <LargePicture 
                                        resizeMode={'cover'} 
                                        style={styles.inputUpImg} 
                                        imageStyle={styles.inputUpImg} 
                                        source={{uri: item.data}}
                                    /> 
                                    <TouchableOpacity style={styles.deleteImgBox} onPress={() => this.deleteImg(item.bond)}>
                                        <ImageView resizeMode={'contain'} style={styles.deleteImg} source={Images.icon_result_fail}/>
                                    </TouchableOpacity>
                                </View>
                                :
                                <ImageView
                                    resizeMode={'cover'}
                                    style={styles.inputUpImg}
                                    source={Images.img_uploadimg8}
                                />
                            }
                        </TouchableOpacity>
                    </View>
                )
            } else if (item.type == 'area') {
                return (
                    <View key={index} style={styles.inputLine}>
                        <Text style={styles.inputLineLeft} numberOfLines={1}>{item.name}</Text>
                        <TouchableOpacity style={styles.inputDay} onPress={() => this.showActionAreaPicker()}>
                            <Text style={[styles.inputDayText, province !== '' && {color: '#333'}]} numberOfLines={1}>
                                {province == '' ? '请选择省/市/区' : province+'-'+city+'-'+district}
                            </Text>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.inputDayImg}
                                source={Images.icon_arrow_right}
                            />
                        </TouchableOpacity>
                    </View>
                )
            } else if (item.type == 'space') {
                return (
                    <View key={index} style={styles.spaceLine}></View>
                )
            }
        })
        return contain
    }

    // 提交
    onSubmit = async () => {
        Keyboard.dismiss();
        let {dataSource} = this.state;
        let allDown = true, data = {};
        dataSource.forEach(element => {
            if (element.data === '') allDown = false
            data[element.bond] = element.data
        });
        if (!allDown) {
            ToastManager.message('请检查是否有未完善信息')
            return 
        }
        let url = ServicesApi.LOGIN_BIO_AUTHENTICATION;
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success('提交认证成功');
            setTimeout(()=> {
                RouterHelper.reset('', 'Tab');
            }, 1000);
        } else if (result.code === StatusCode.FAIL_CODE) {
            ToastManager.warn(result.msg)
        }
    }

    render() {
        let {loginStore} = this.props;
        let {userInfo} = loginStore;
        let {loading, dataSource} = this.state;
        return (
            <PageContainer
                loading={loading}
                fitNotchedScreen={Predefine.isNotchedScreen}
                style={styles.container}
            >
                <NavigationBar 
                    title={'承运商认证'} 
                    style={styles.navigationBar}
                />
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={'handled'}
                    style={styles.contentBoxStyle}
                >
                    {
                        userInfo.is_pass == 0 &&
                        <View style={styles.reasonsBox}>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.reasonsImg}
                                source={Images.icon_warring_logo1}
                            />
                            <Text style={styles.reasons}>拒绝原因：{userInfo.reasons}</Text>
                        </View>
                    }
                    {this.ListView(dataSource)}
                    <View style={styles.btnBox}>
                        <Button
                            style={styles.btn}
                            title={'提交认证'}
                            titleStyle={styles.btnTitle}
                            resizeMode={'cover'}
                            backgroundImage={Images.img_btn_bg1}
                            onPress={() => this.onSubmit()}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    rightAction: {
        color: '#fff',
        fontSize: 15,
    },

    reasonsBox: {
        backgroundColor: '#e1e1e1',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    reasonsImg: {
        width: 16,
        height: 16,
        marginRight: 6,
    },
    reasons: {
        fontSize: 14,
        color: '#333',
    },
    inputLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 50,
        marginHorizontal: 15,
        borderBottomWidth: Predefine.minPixel,
        borderBottomColor: '#e1e1e1',
    },
    inputLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    inputLineRight: {
        color: '#666',
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
    },
    inputDay: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    inputDayText: {
        color: '#999',
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
        marginHorizontal: 6,
    },
    inputDayImg: {
        width: 12,
        height: 12,
    },
    inputUpImg: {
        width: 120,
        height: 72,
    },
    spaceLine: {
        height: 10,
        backgroundColor: '#F2F1F6',
    },

    deleteImgBox: {
        position: 'absolute',
        top: -8,
        right: -8,
    },
    deleteImg: {
        width: 16,
        height: 16,
        borderRadius: 8,
    },

    btnBox: {
        padding: 30,
    },
    btn: {
        backgroundColor: DominantColor,
    },
});