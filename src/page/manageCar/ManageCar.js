/**
 * 易驰护运 - ManageCar
 * http://menger.me
 * 车辆管理
 * @huanhaun
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    StatusBar,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import {
    IconFont,
    ListView,
} from '../../components'
import CarListItem from '../../components/customer/Item/CarListItem'

export default class ManageCar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            dataSource: [],
        };
        this.page = 1;
        this.limit = 10;
    }

    componentDidMount() {
        this._onRefresh();
    };

    requestData = async () => { // 数据列表
        let {dataSource} = this.state;
        let url = ServicesApi.PERSONAL_CENTER_VEHICLE_LIST;
        let data = {
            page: this.page,
            limit: this.limit,
        };
        try {
            const result = await Services.post(url, data);
            let dataSourceTemp = dataSource.slice();
            let dataList = [];
            if (result.code === StatusCode.SUCCESS_CODE) {
                dataList = result.data.dataList;
                if (parseInt(data.page) === 1) {
                    dataSourceTemp = dataList
                } else {
                    if (dataList.length !== 0) {
                        dataSourceTemp = dataSourceTemp.concat(dataList);
                    }
                }
            }
            this.setState({
                dataSource: dataSourceTemp,
                loading: false,
            });
            this._onStopLoading(dataList.length < this.limit);
        } catch (error) {
            this._onStopLoading(true);
        }
    };

    _onStopLoading = (status) => {
        this.setState({ready: true});
        this._listRef && this._listRef.stopRefresh();
        this._listRef && this._listRef.stopEndReached({allLoad: status});
    };

    renderItem = ({item, index}) => {
        return (
            <CarListItem
                item={item}
                onRefresh={() => this._onRefresh()}
                {...this.props}
            />
        )
    };

    _captureRef = (v) => {
        this._listRef = v
    };

    _keyExtractor = (item, index) => {
        return `item_${index}`
    };

    _onRefresh = () => {
        this.page = 1;
        this.requestData()
    };

    _onEndReached = () => {
        this.page++;
        this.requestData()
    };

    renderItemSeparator = (info) => {
        return null;
    };

    donNotHaveACar = () => {
        return (
            <View style={styles.donNotHaveACar}>
                <ImageView
                    resizeMode={'contain'}
                    style={styles.donNotHaveACarImg}
                    source={Images.icon_don_not_have_car}
                />
                <Text style={styles.donNotHaveACarText}>暂未添加车辆，不可进行承运</Text>
                <Button
                    style={styles.donNotHaveACarBtn}
                    title={'添加车辆'}
                    titleStyle={styles.donNotHaveACarBtnTitle}
                    resizeMode={'cover'}
                    backgroundImage={Images.img_btn_bg1}
                    onPress={() => RouterHelper.navigate('', 'EditCar', {
                        onRefresh: () => this._onRefresh()
                    })}
                />
            </View>
        )
    }

    render() {
        let {loading, dataSource} = this.state;
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
                fitIPhoneX={true} 
                // fitNotchedScreen={Predefine.isNotchedScreen}
            >
                <NavigationBar 
                    title={'车辆管理'} 
                    renderRightAction={[
                        {
                            title: <Text style={styles.rightAction}><IconFont.Ionicons name={'md-add-circle-outline'} size={14} color={'#fff'}/> 添加车辆</Text>, 
                            onPress: () => RouterHelper.navigate('', 'EditCar', {onRefresh: () => this._onRefresh()})
                        }
                    ]}
                />
                { 
                    dataSource.length == 0 
                    ?
                    this.donNotHaveACar()
                    :
                    <View style={{flex: 1}}>
                        <ListView
                            style={styles.listView}
                            initialRefresh={false}
                            data={dataSource}
                            ref={this._captureRef}
                            onRefresh={this._onRefresh}
                            renderItem={this.renderItem}
                            keyExtractor={this._keyExtractor}
                            onEndReached={this._onEndReached}
                            ItemSeparatorComponent={this.renderItemSeparator}
                        />
                    </View>
                }
            </PageContainer>
        )
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#f2f2f2',
    },
    rightAction: {
        color: '#fff',
        fontSize: 14,
    },
    btnBox: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: '#fff',
    },
    btn: {
        backgroundColor: DominantColor,
    },

    donNotHaveACar: {
        alignItems: 'center',
        paddingVertical: 50,
    },
    donNotHaveACarImg: {
        width: 150,
        height: 150,
    },
    donNotHaveACarText: {
        fontSize: 15,
        color: '#333',
        fontWeight: 'bold',
        marginBottom: 20,
    },
    donNotHaveACarBtn: {
        backgroundColor: DominantColor,
        paddingHorizontal: 30,
        width: SCREEN_WIDTH - 60,
    },
})