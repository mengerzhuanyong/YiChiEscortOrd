/**
 * 易驰护运 - Withdraw
 * http://menger.me
 * 提现
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, TextInput, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard, ImageStore} from 'react-native';
import {
    IconFont,
    ListView,
    VerticalLine,
    HorizontalLine,
    BannerComponent,
    HotNewsComponent,
    HomeResourceItem,
    ListHeaderLoading,
} from '../../components';
import {inject, observer} from 'mobx-react';

@inject('loginStore')
@observer
export default class Withdraw extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            price: '',
            pageTitle: '',
            dataSource: [],
        };
    }

    componentDidMount () {
        this.requestData()
        this.requestWebData()
    }

    requestData = async () => {
        let url = ServicesApi.PERSONAL_CENTER_ATM;
        let data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({dataSource: result.data});
        }
    }

    requestWebData = async () => {
        let url = ServicesApi.ARTICLE_ARTICLE_LIST;
        let data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            console.log('result.data------------->', result.data)
            this.setState({
                pageTitle: result.data[7].title,
                uri: result.data[7].url
            });
        }
    }

    onSubmit = async () => {
        let {onRefresh} = this.params;
        let {price, dataSource} = this.state;
        if (price == '') {
            ToastManager.message('请输入提现金额');
            return
        }
        if (Number(price) >  Number(dataSource.balance)) {
            ToastManager.message('余额不足');
            return
        } 
        let url = ServicesApi.PERSONAL_CENTER_APPLY_ATM;
        let data = {
            money: price,
        };
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success(result.msg);
            onRefresh && onRefresh();
            setTimeout(() => RouterHelper.goBack(), 1000);
        } else if (result.code === StatusCode.FAIL_CODE) {
            ToastManager.warn(result.msg);
        }
    }

    render() {
        let {price, dataSource, uri, pageTitle} = this.state;
        return (
            <PageContainer style={styles.container}>
                <NavigationBar
                    title={'提现'}
                    style={[styles.navigationBarStyle]}
                />
                <ScrollView keyboardShouldPersistTaps={'handled'}>
                    <ImageBackground source={Images.img_withdraw_bg} style={styles.header}>
                        <View style={styles.headerBox}>
                            <Text style={styles.headerBoxTitle} numberOfLines={1}>提现金额</Text>
                            <TextInput
                                defaultValue={price}
                                style={styles.headerBoxInput}
                                placeholder={'请输入'}
                                placeholderTextColor={'#9AE6F1'}
                                keyboardType={'phone-pad'}
                                onChangeText={(text) => this.setState({price: text})}
                            />
                        </View>
                        <View style={styles.headerBox}>
                            <Text style={styles.headerBoxTitle} numberOfLines={1}>可提现金额</Text>
                            <Text style={styles.headerBoxText} numberOfLines={1}>{dataSource.balance}</Text>
                        </View>
                    </ImageBackground>
                    <View style={styles.card}>
                        <View style={styles.cardHeader}>
                            <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                            <Text style={styles.cardHeaderText} numberOfLines={1}>提现至</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.lineLeft}>企业全称<Text style={{color: '#E71425'}}>*</Text>：</Text>
                            <Text style={styles.lineInput} numberOfLines={1}>{dataSource.company_name}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.lineLeft}>银行账户<Text style={{color: '#E71425'}}>*</Text>：</Text>
                            <Text style={styles.lineInput} numberOfLines={1}>{dataSource.bank_account}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.lineLeft}>开户行<Text style={{color: '#E71425'}}>*</Text>：</Text>
                            <Text style={styles.lineInput} numberOfLines={1}>{dataSource.bank_open}</Text>
                        </View>
                        <View style={styles.line}>
                            <Text style={styles.lineLeft}>公司税号<Text style={{color: '#E71425'}}>*</Text>：</Text>
                            <Text style={styles.lineInput} numberOfLines={1}>{dataSource.company_tax_number}</Text>
                        </View>
                        <View style={styles.tips}>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.tipsImg}
                                source={Images.icon_warring_logo1}
                            />
                            <Text style={styles.tipsText}>
                                温馨提示：订单完成之后，运费当即结算至您的余额中，按运输订单的结算周期来计算提现的用，具体请看
                                <Text style={{color: '#436EEE'}} onPress={() => RouterHelper.navigate('', 'WebPage', {uri, pageTitle})}>《提现服务费细则》</Text>
                                。提现钱款将会在T+n到账，清注 意查收。
                            </Text>
                        </View>
                        <Button
                            style={styles.btn}
                            title={'提现'}
                            titleStyle={styles.btnTitle}
                            resizeMode={'cover'}
                            backgroundImage={Images.img_btn_bg1}
                            onPress={() => this.onSubmit()}
                        />
                    </View>
                </ScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#F0F0F0'
    },

    header: {
        width: SCREEN_WIDTH - 30,
        height: (SCREEN_WIDTH - 30)*(365/1035),
        margin: 15,
        borderRadius: 5,
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    headerBox: {
        flex: 1,
        paddingLeft: 20,
    },
    headerBoxTitle: {
        fontSize: 12,
        color: '#fff',
        marginBottom: 10,
    },
    headerBoxInput: {
        fontSize: 20,
        color: '#fff',
    },
    headerBoxText: {
        fontSize: 25,
        color: '#fff',
    },

    card: {
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 13,
        fontWeight: 'bold',
    },

    line: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
        borderBottomWidth: Predefine.minPixel,
        borderBottomColor: '#e1e1e1',
    },
    lineLeft: {
        color: '#333',
        fontSize: 12,
    },
    lineInput: {
        color: '#333',
        fontSize: 12,
        flex: 1,
        textAlign: 'right',
    },

    tips: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    tipsImg: {
        width: 12,
        height: 12,
        marginRight: 6,
    },
    tipsText: {
        flex: 1,
        fontSize: 12,
        lineHeight: 18,
        color: '#999',
    },

    btn: {
        backgroundColor: DominantColor,
        marginTop: 20,
    },
});