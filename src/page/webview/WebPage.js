/**
 * 中危护运 - WebPage
 * http://menger.me
 * @大梦
 */

'use strict';

import React, {PureComponent} from 'react'
import {StyleSheet, View, PermissionsAndroid, CameraRoll, DeviceEventEmitter} from 'react-native'
import Webview from 'react-native-webview'

export default class WebPage extends PureComponent {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            imagePath: '',
            uri: this.params.uri || '',
            share_params: this.params.share_params || {},
            showRightAction: this.params.showRightAction || false,
        };
    }

    componentDidMount() {
        // this.requestData()
        // __ANDROID__ && this.downloadShareImage();
    }

    requestData = async() => {
        let url = ServicesApi.PERSONAL_CENTER_ABOUT_US;
        let data = {
            // uri: this.params.uri,
            type: 2
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            console.log('result------------->', result)
            this.setState({
                uri: result.data
            });
        }
    }

    componentWillUnmount() {
        // DeviceEventEmitter.emit('closeWebViewWindow','')
    }

    onLoadEnd = () => {
        this.setState({loading: false});
    };

    render() {
        console.log('this.params------------->', this.params)
        let {loading, imagePath, uri, share_params, showRightAction} = this.state;
        let style = this.params.style || '';
        let pageTitle = this.params.pageTitle || '详情页';
        console.log('页面地址---->', uri);
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar
                    title={pageTitle}
                    renderRightAction={showRightAction ? [{
                        icon: Images.icon_share,
                        iconStyle: Predefine.headerIconStyle,
                        onPress: () => {
                            ActionsManager.showShare(share_params, (res) => {
                                // console.log('res---->', res);
                            });
                        }
                    }] : null}
                />
                <Webview
                    source={{uri}}
                    onLoadEnd={this.onLoadEnd}
                    startInLoadingState={false}
                    style={[styles.webContainer, style]}
                />
            </PageContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f2f3',
    },
    webContainer: {
        flex: 1,
        backgroundColor: '#fff',
    },
});