/**
 * 易驰护运 - ChangeCar
 * http://menger.me
 * 订单详情更换车辆
 * @桓桓
 */

'use strict';

import React from 'react'
import {ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native'
import {ListHeaderLoading,} from '../../components'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class ChangeCar extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            isRefreshing: false,
            order_vehicle_list: [],
            change_vehicle_list: [],
            order_vehicle: '',
            change_vehicle: '',
            shipper_remarks: '',
        }
    }

    componentDidMount() {
        this.requestData();
    }

    _onRefresh = async () => {
        this.setState({isRefreshing: true});
        this.requestData();
        setTimeout(() => this.setState({isRefreshing: false}), 500);
    };
    
    requestData = async() => {
        let {item} = this.params;
        let url = ServicesApi.ORDER_CHANGE_VEHICLE;
        let data = {
            order_id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                order_vehicle_list: result.data.order_vehicle_list.dataList, 
                change_vehicle_list: result.data.change_vehicle_list.dataList,
                loading: false,
            });
        }
    }

    // 选中或取消选中车辆
    changeLeisure = (item, type) => {
        if (type == 'order_vehicle') {
            this.setState({order_vehicle: item.id})
        } else if (type == 'change_vehicle') {
            this.setState({change_vehicle: item.id})
        }
    }

    onSubmit = async () => {
        let {item, onRefresh} = this.params;
        let {shipper_remarks, order_vehicle, change_vehicle} = this.state;
        if (shipper_remarks == '') {
            ToastManager.message('请填写换车原因');
            return
        }
        if (order_vehicle == '') {
            ToastManager.message('请选择选择被替换的车辆');
            return
        }
        if (change_vehicle == '') {
            ToastManager.message('请选择选择新的车辆');
            return
        }
        let url = ServicesApi.ORDER_CHANGE_VEHICLE_APPLY;
        let data = {
            order_id: item.id,
            change_reasons: shipper_remarks,
            original_vehicle_id: order_vehicle,
            current_vehicle_id: change_vehicle,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.message(result.msg);
            setTimeout(() => {
                onRefresh && onRefresh()
                RouterHelper.goBack()
            }, 1000);
        } else {
            ToastManager.message(result.msg);
        }
    }

    oldCarList = (data) => {
        let {order_vehicle} = this.state;
        if (data.length == 0) {
            return <Text style={styles.doNotHaveCar} numberOfLines={1}>暂无符合要求车辆</Text>
        }
        let contain = data.map((item, index) => {
            return (
                <TouchableOpacity
                    key={index}
                    style={[styles.carCard, item.id == order_vehicle && {backgroundColor: '#CAF2F7'}]}
                    onPress={() => this.changeLeisure(item, 'order_vehicle')}
                >
                    <View style={styles.carCardHeader}>
                        <View style={styles.carCardHeaderBall}/>
                        <Text style={styles.carCardHeaderText}>{item.status_text}</Text>
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText}>车牌号：{item.vehicle_number}</Text>
                        <Text style={styles.carCardLineText}>司机姓名：{item.driver_name}</Text>
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText}>车辆类型：{item.vehicle_type_text}</Text>
                        <Text style={styles.carCardLineText}>车辆载重：{item.vehicle_weight}吨</Text>
                    </View>
                    {
                        item.id == order_vehicle &&
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.carCardImg}
                            source={Images.icon_choice_car}
                        />
                    }
                </TouchableOpacity>
            )
        })
        return contain
    }

    newCarList = (data) => {
        let {change_vehicle} = this.state;
        if (data.length == 0) {
            return <Text style={styles.doNotHaveCar} numberOfLines={1}>暂无符合要求车辆</Text>
        }
        let contain = data.map((item, index) => {
            return (
                <TouchableOpacity 
                    key={index} 
                    style={[styles.carCard, item.id == change_vehicle && {backgroundColor: '#CAF2F7'}]}
                    onPress={() => this.changeLeisure(item, 'change_vehicle')}
                >
                    <View style={styles.carCardHeader}>
                        <View style={styles.carCardHeaderBall}/>
                        <Text style={styles.carCardHeaderText}>{item.status_text}</Text>
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText}>车牌号：{item.vehicle_number}</Text>
                        <Text style={styles.carCardLineText}>司机姓名：{item.driver_name}</Text>
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText}>车辆类型：{item.vehicle_type_text}</Text>
                        <Text style={styles.carCardLineText}>车辆载重：{item.vehicle_weight}吨</Text>
                    </View>
                    {
                        item.id == change_vehicle &&
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.carCardImg}
                            source={Images.icon_choice_car}
                        />
                    }
                </TouchableOpacity>
            )
        })
        return contain
    }
    
    render() {
        let {loading, shipper_remarks, order_vehicle_list, change_vehicle_list} = this.state;
        return (
            <PageContainer 
                loading={loading}
                style={styles.container}
                fitIPhoneX={true} 
                fitNotchedScreen={Predefine.isNotchedScreen}
            >
                <NavigationBar title={'申请换车'} style={styles.navigationBar}/>
                <View style={styles.headerPadding}/>
                <ScrollView
                    style={styles.content}
                    refreshControl={<ListHeaderLoading onRefresh={this._onRefresh} refreshing={this.state.isRefreshing}/>}
                >
                    <View style={styles.card}>
                        <View style={styles.cardHeader}>
                            <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                            <Text style={styles.cardHeaderText} numberOfLines={1}>换车原因</Text>
                        </View>
                        <TextInput
                            multiline={true}
                            textAlignVertical={'top'}
                            style={styles.textArea}
                            placeholderTextColor={'#999'}
                            defaultValue={shipper_remarks}
                            placeholder={'请输入您的备注信息...'}
                            onChangeText={(text) => this.setState({shipper_remarks: text})}
                        />
                    </View>
                    <View style={styles.card}>
                        <View style={styles.cardHeader}>
                            <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                            <Text style={styles.cardHeaderText} numberOfLines={1}>选择被替换的车辆（单选）</Text>
                        </View>
                        {this.oldCarList(order_vehicle_list)}
                    </View> 
                    <View style={styles.card}>
                        <View style={styles.cardHeader}>
                            <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                            <Text style={styles.cardHeaderText} numberOfLines={1}>选择新的车辆（单选）</Text>
                        </View>
                        {this.newCarList(change_vehicle_list)}
                    </View>
                </ScrollView>
                <View style={styles.bottomBtn}>
                    <Button
                        style={styles.btn}
                        title={'提交换车审核'}
                        titleStyle={styles.btnTitle}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => this.onSubmit()}
                    />
                </View>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const contentWidth = Predefine.screenWidth - 20;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navigationBar: {
        zIndex: 10,
        position: 'absolute',
    },
    headerPadding: {
        height: NAV_BAR_HEIGHT,
    },

    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 13,
        fontWeight: 'bold',
    },
    textArea: {
        borderWidth: 1,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        marginBottom: 15,
        fontSize: 14,
        padding: 10,
        height: 80,
    },

    doNotHaveCar: {
        paddingBottom: 10,
        fontSize: 14,
        color: '#666',
    },
    carCard: {
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        marginBottom: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    carCardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 25,
    },
    carCardHeaderBall: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
        backgroundColor: DominantColor,
    },
    carCardHeaderText: {
        color: DominantColor,
        fontSize: 12,
    },
    carCardLine: {
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineText: {
        flex: 1,
        fontSize: 12,
        color: '#333',
    },
    carCardImg: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: 28,
        height: 30,
    },
    
    bottomBtn: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: '#fff',
    },
    btn: {
        backgroundColor: DominantColor,
    },
});