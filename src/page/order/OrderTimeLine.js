/**
 * 易驰护运 - OrderTimeLine
 * http://menger.me
 * 订单时间轴
 * @桓桓
 */

'use strict';

import React from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class OrderTimeLine extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            timeDataSource: ['', '', '', '', ''],
        }
    }

    componentDidMount() {
        this.requestTimeCardData();
    }

    // 请求时间轴数据
    requestTimeCardData = async () => {
        let {item} = this.params;
        let url = ServicesApi.ORDER_ORDER_LOG;
        let data = {
            order_id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                timeDataSource: result.data,
                loading: false,
            });
        }
    }

    timeLineView = (data) => {
        let contain = data.map((item, index) => {
            return (
                <View key={index} style={[styles.cardTimeLine, index == 0 && {marginTop: 0}]}>
                    {
                        item.is_complete == 0
                        ?
                        <Text style={styles.cardTimeLineDay} numberOfLines={1}>暂未到达</Text>
                        :
                        <Text style={styles.cardTimeLineDayCur} numberOfLines={2}>{item.update_time}</Text>
                    }
                    {
                        item.is_complete == 0
                        ?
                        <View style={styles.cardTimeLineImgBox}>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.cardTimeLineImg}
                                source={Images.icon_time_card_logo3}
                            />
                        </View>
                        :
                        <View style={styles.cardTimeLineImgBoxCur}>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.cardTimeLineImgCur}
                                source={Images.icon_time_card_logo1}
                            />
                        </View>
                    }
                    {
                        item.is_complete == 0 
                        ?
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.cardTimeLineRightArrow}
                        />
                        :
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.cardTimeLineRightArrow}
                            source={Images.icon_time_card_logo2}
                        />
                    }
                    {
                        item.is_complete == 0
                        ?
                        <View style={styles.cardTimeLineRight}>
                            <Text style={styles.cardTimeLineRightText} numberOfLines={1}>{item.order_log_status_text}</Text>
                        </View>
                        :
                        <View style={[styles.cardTimeLineRightCur]}>
                            <Text style={styles.cardTimeLineRightTextCur} numberOfLines={1}>{item.order_log_status_text}</Text>
                            {
                                item.weigh != null &&
                                <View>
                                    <View style={styles.statusBox}>
                                        <ImageView
                                            resizeMode={'cover'}
                                            style={styles.statusImg}
                                            source={{uri: item.weigh_img}}
                                        />
                                        <View style={styles.statusTextBox}>
                                            <Text style={styles.statusWeigh} numberOfLines={1}>装货吨数:<Text style={{color: '#FC6911'}}>{item.weigh}</Text>吨</Text>
                                            <View style={styles.smallStatusTextBox}>
                                                <Text style={styles.statusText} numberOfLines={1}>{item.is_pass_text}</Text>    
                                            </View>
                                        </View>
                                    </View>
                                    {item.reasons && <Text style={styles.reasons} numberOfLines={1}>{item.reasons}</Text>}   
                                </View>
                            }
                        </View>
                    }
                </View>
            )
        })
        return (
            <View style={styles.cardTime}>
                <View style={styles.line}/>
                {contain}
            </View>    
        )
    }

    render() {
        let {loading, timeDataSource} = this.state;
        return (
            <PageContainer 
                loading={loading} 
                style={styles.container}
                fitIPhoneX={true} 
                fitNotchedScreen={Predefine.isNotchedScreen}
            >
                <NavigationBar title={'订单时间轴'}/>
                <View style={styles.card}>
                    <View style={styles.cardHeader}>
                        <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                        <Text style={styles.cardHeaderText} numberOfLines={1}>时间轴</Text>
                    </View>
                    <ScrollView>
                        {this.timeLineView(timeDataSource)}
                    </ScrollView>    
                </View>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const contentWidth = Predefine.screenWidth - 20;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        // height: Predefine.screenHeight - Predefine.tabBarHeight,
    },

    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        overflow: 'hidden',
        paddingBottom: 10,
        height: '100%',
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 13,
        fontWeight: 'bold',
    },

    cardTime: {
        overflow: 'hidden',
    },
    line: {
        width: 1,
        backgroundColor: '#DCDCDC',
        height: '100%',
        position: 'absolute',
        left: 105,
        top: 0,
        zIndex: -1,
    },
    cardTimeLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
    },
    cardTimeLineDay: {
        width: 80,
        textAlign: 'center',
        fontSize: 12,
        color: '#999',
    },
    cardTimeLineDayCur: {
        width: 80,
        textAlign: 'center',
        fontSize: 12,
        color: '#666',
    },
    cardTimeLineImgBoxCur: {
        backgroundColor: '#fff',
        height: 30,
        width: 30,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardTimeLineImgCur: {
        width: 24,
        height: 24,
    },
    cardTimeLineImgBox: {
        backgroundColor: '#fff',
        height: 20,
        width: 30,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardTimeLineImg: {
        width: 10,
        height: 10,
    },
    cardTimeLineRightArrow: {
        width: 10,
        marginRight: -3,
    },
    cardTimeLineRight: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        padding: 10,
        borderRadius: 5,
        marginLeft: 7,
    },
    cardTimeLineRightText: {
        color: '#999',
        fontSize: 12,
    },
    cardTimeLineRightCur: {
        flex: 1,
        backgroundColor: DominantColor,
        padding: 10,
        borderRadius: 5,
    },
    cardTimeLineRightTextCur: {
        color: '#fff',
        fontSize: 12,
    },

    statusBox: {
        marginTop: 10,
        flexDirection: 'row',
    },
    statusImg: {
        width: 60,
        height: 60,
    },
    statusTextBox: {
        flex: 1,
        height: 60,
        paddingLeft: 6,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    statusWeigh: {
        fontSize: 13,
        color: '#333',
    },
    smallStatusTextBox: {
        padding: 4,
        borderRadius: 3,
        backgroundColor: '#82DFE4',
    },
    statusText: {
        color: '#fff',
        fontSize: 12,
    },
});