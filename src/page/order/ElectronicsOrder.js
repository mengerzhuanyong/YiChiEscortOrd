/**
 * 易驰护运 - ElectronicsOrder
 * http://menger.me
 * 电子运单
 * @桓桓
 */

'use strict';

import React from 'react'
import {StyleSheet, Text, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard, PermissionsAndroid} from 'react-native'
import ViewShot, { captureScreen } from "react-native-view-shot"
import CameraRoll from '@react-native-community/cameraroll'
import {ListHeaderLoading} from '../../components'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class ElectronicsOrder extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            isRefreshing: false,
            dataSource: [],
        }
    }

    componentDidMount() {
        this.requestData()
    }

    _onRefresh = async () => {
        this.setState({isRefreshing: true});
        this.requestData();
        setTimeout(() => this.setState({isRefreshing: false}), 500);
    };

    requestData = async () => {
        let {item} = this.params;
        let url = ServicesApi.ORDER_ELECTRONIC_WAYBILL;
        let data = {
            id: item.id,
            type: 1, // 1承运商 2司机
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                dataSource: result.data,
                loading: false,
            });
        }
    }

    _saveToCameraRoll = async (uri) => {
        if (__ANDROID__) {
            let perRes = await PermissionsAndroid.request('android.permission.WRITE_EXTERNAL_STORAGE', null);
        }
        console.log("Image saved to", uri)
        const saveRes = await CameraRoll.saveToCameraRoll(uri, 'photo');
        if (saveRes) {
            ToastManager.message('保存成功');
        }
    };

    render() {
        let pageTitle = this.params.pageTitle || '电子运单';
        let {loading, dataSource} = this.state;
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar
                    title={pageTitle}
                    style={styles.navigationBar}
                />
                <View style={styles.headerPadding}/>
                <ScrollView
                    style={styles.content}
                    refreshControl={<ListHeaderLoading onRefresh={this._onRefresh} refreshing={this.state.isRefreshing}/>}
                >
                    <ViewShot
                        ref={ref => this.viewShotRef = ref}
                        options={{ format: "jpg", quality: 0.9}}
                        style={{flex: 1, backgroundColor: '#f2f2f2'}}
                    >
                        <View style={styles.card}>
                            <View style={styles.cardHeader}>
                                <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                                <Text style={styles.cardHeaderText} numberOfLines={1}>订单信息</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>订单号：</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.order_no}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>订单生成时间：</Text>
                                <Text style={styles.cardLineRight}
                                    numberOfLines={1}>{dataSource.vehicle_order_start_time}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>订单完成时间：</Text>
                                <Text style={styles.cardLineRight}
                                    numberOfLines={1}>{dataSource.vehicle_order_end_time}</Text>
                            </View>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.cardHeader}>
                                <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                                <Text style={styles.cardHeaderText} numberOfLines={1}>人员信息</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>货主公司：</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.shipper_company_name}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>货主联系人：</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.shipper_name}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>货主联系方式：</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.shipper_mobile}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>承运商公司：</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.carrier_company_name}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>承运商联系人：</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.carrier_name}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>承运商联系方式：</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.carrier_mobile}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>装货单位</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.from_company}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>装货联系人</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.from_contact}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>装货联系人联系方式</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.from_mobile}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>卸货单位</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.to_company}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>卸货联系人</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.to_contact}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>卸货联系人联系方式</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.to_mobile}</Text>
                            </View>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.cardHeader}>
                                <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                                <Text style={styles.cardHeaderText} numberOfLines={1}>货物信息</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>货物名称</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.goods_name}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>货物密度</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.goods_density}吨/m³</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>危险品分类</Text>
                                <Text style={styles.cardLineRight}
                                    numberOfLines={1}>{dataSource.goods_dangerous_level_text}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>接单吨数</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.vehicle_weight}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>货物价值</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.goods_price}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>最大合理损耗</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.goods_reasonable_loss_text}</Text>
                            </View>
                        </View>
                        {/*//当前token 是承运商的还是司机的  如果是司机就隐藏 / 承运商显示*/}

                        {dataSource.type_m !== 'driver' &&
                        <View style={styles.card}>
                            <View style={styles.cardHeader}>
                                <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                                <Text style={styles.cardHeaderText} numberOfLines={1}>其他信息</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>付款方式</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.payment_type}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>实际装货重量</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.real_loading_weight}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>实际卸货重量</Text>
                                <Text style={styles.cardLineRight}
                                    numberOfLines={1}>{dataSource.real_discharge_weight}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>是否需要赔偿</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.is_compensate}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>结算周期</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.account_period}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>总运费</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.total_freight}</Text>
                            </View>
                            {/* <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>已收</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.total_freight}</Text>
                            </View> */}
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>总消耗</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.loss}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>赔偿金额</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.compensate_money}</Text>
                            </View>
                            <View style={styles.cardLine}>
                                <Text style={styles.cardLineLeft} numberOfLines={1}>最终运费</Text>
                                <Text style={styles.cardLineRight} numberOfLines={1}>{dataSource.finish_total_freight}</Text>
                            </View>
                        </View>
                        }
                    </ViewShot>
                </ScrollView>
                <View style={styles.btnBox}>
                    <Button
                        style={styles.btn}
                        title={'保存电子运单'}
                        titleStyle={styles.btnTitle}
                        resizeMode={'cover'}
                        backgroundImage={Images.img_btn_bg1}
                        onPress={() => this.viewShotRef.capture().then(uri => {
                            this._saveToCameraRoll(uri);
                        })}
                    />
                </View>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const contentWidth = Predefine.screenWidth - 20;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    navigationBar: {
        zIndex: 10,
        position: 'absolute',
    },
    headerPadding: {
        height: NAV_BAR_HEIGHT,
    },

    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 13,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 12,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 12,
    },

    btnBox: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: '#fff',
    },
    btn: {
        backgroundColor: DominantColor,
    },
});