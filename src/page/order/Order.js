/**
 * 易驰护运 - Order
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {StyleSheet, View} from 'react-native'
import {SegmentedView,} from '../../components'
import {inject, observer} from 'mobx-react'
import OrderList from '../../components/customer/Order/OrderList'

@inject('loginStore')
@observer
export default class Order extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            dataSources: [
                {id: 0, name: '全部',},
                {id: 1, name: '待审核',},
                {id: 2, name: '待开始',},
                {id: 3, name: '运输状态',},
                {id: 4, name: '待收款',},
                {id: 5, name: '已完成',},
            ],
            initialPage: this.params.initialPage || 0,
        }
    }

    _renderSegmentedTabView = () => {
        let {dataSources} = this.state;
        let content = dataSources.map((item, index) => {
            return (
                <View
                    key={index}
                    title={item.name}
                    titleStyle={{color: '#fff'}}
                    style={styles.segmentedTabView}
                >
                    <OrderList id={item.id} item={item} {...this.props}/>
                </View>
            )
        })
        return content;
    }

    render() {
        let {dataSources, initialPage} = this.state;
        return (
            <PageContainer style={styles.container}>
                <NavigationBar title={'我的订单'}/>
                <View style={{flex: 1}}>
                    {dataSources.length > 0 ? <SegmentedView
                        initialPage={initialPage}
                        indicatorType={'lengThen'}
                        indicatorWidthType={'item'}
                        style={styles.segmentedView}
                    >
                        {this._renderSegmentedTabView()}
                    </SegmentedView> : null}
                </View>

            </PageContainer>
        );
    }
}

const styles = StyleSheet.create({
    rightAction: {
        color: '#fff',
        fontSize: 20,
    },

    segmentedTabView: {
        flex: 1,
    },
});