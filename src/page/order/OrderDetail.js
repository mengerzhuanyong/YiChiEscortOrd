/**
 * 易驰护运 - OrderDetail
 * http://menger.me
 * 承运商个人中心的订单详情
 * @桓桓
 */

'use strict';

import React from 'react'
import {ImageBackground, Linking, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native'
import {
    AddressInfo,
    ApplySupplyCard,
    CarsCard,
    CompanyCard,
    EvaluateCard,
    EvaluateViewCard,
    HorizontalLine,
    IconFont,
    ListHeaderLoading,
    PayListCard,
    VerticalLine,
} from '../../components'
import * as WeChat from 'react-native-wechat-lib'
import {Geolocation, MapApp} from 'react-native-baidu-map'
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class OrderDetail extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            item: this.params.item || {},
            dataSource: [],
            isRefreshing: false,
            accountType: 0, // 支付方式
            cancel_order_reasons: '', // 取消订单的原因
            price: '', // 收款金额（供货商最终确认收款额）
            warn: false,

            address: '',
            warp: '',
            weft: '',
            from_address: '',
            from_warp: '',
            from_weft: '',
            to_address: '',
            to_warp: '',
            to_weft: '',
        }
    }

    componentDidMount() {
        this.requestData();
        this.readOrder();
        this.getCurrentPosition();
        this._addWeChatListener();
    }

    // 监听那个react-native-wechat-lib
    _addWeChatListener = async () => {
        WeChat.addListener('PayReq.Resp', (res) => {
            if (res.errCode === 0) {
                ToastManager.message('支付成功!');
                RouterHelper.goBack()
            } else if (res.errCode === -1) {
                ToastManager.message('支付失败!');
            } else if (res.errCode === -2) {
                ToastManager.message('取消支付!');
            }
        });
    };

    _onRefresh = async () => {
        let {onRefresh} = this.params;
        this.setState({isRefreshing: true});
        this.requestData();
        this.getCurrentPosition();
        setTimeout(() => this.setState({isRefreshing: false}), 500);
        onRefresh && onRefresh()
    };

    getCurrentPosition = () => {
        Geolocation.getCurrentPosition()
            .then(data => {
                console.log(data)
                this.setState({
                    warp: data.longitude,
                    weft: data.latitude,
                })
            })
            .catch(e =>{
                console.warn(e, 'error');
            })
    }

    // 已读
    readOrder = async () => {
        let {item, onRefresh} = this.params;
        let url = ServicesApi.PERSONAL_CENTER_ORDER_STATUS_READ;
        let data = {
            id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            onRefresh && onRefresh();
        }
    }
    
    // 订单的数据
    requestData = async() => {
        let {item} = this.params;
        let url = ServicesApi.PERSONAL_CENTER_ORDER_INFO;
        let data = {
            id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                dataSource: result.data, 
                from_address: result.data.from_address,
                from_warp: result.data.from_warp,
                from_weft: result.data.from_weft,
                to_address: result.data.to_address,
                to_warp: result.data.to_warp,
                to_weft: result.data.to_weft,
                loading: false,
            });
            if (result.data.carrier_order_status == 20 || result.data.carrier_order_status == 21) {          
                this.setState({
                    price: result.data.order_pay_list[0].actual_money,
                })
            } else if (result.data.carrier_order_status == 70 || result.data.carrier_order_status == 71) {
                this.setState({
                    price: result.data.order_pay_list[1].actual_money,
                })
            } else if (result.data.carrier_order_status == 72 || result.data.carrier_order_status == 73) {
                this.setState({
                    price: result.data.order_pay_list[0].actual_money,
                })
            }
        }
    }

    // 选择支付方式
    payTypeChoice = () => {
        let cate_list = [
            {id: 1, name: '支付宝支付'},
            {id: 2, name: '微信支付'},
            {id: 3, name: '余额支付'},
        ]
        let actions = cate_list.map((item) => {
            return {
                title: item.name,
                onPress: () => {
                    this.setState({
                        cate_name: item.name,
                        accountType: item.id,
                    });
                    ActionManager.hide();
                }
            }
        });
        ActionManager.show({actions});
    }

    // 确认收款弹窗
    onSubmitCollectionWindow = async () => {
        let {dataSource, price} = this.state;
        if (price < 0) { // 反向赔付前端直接截断
            let params = {
                title: '温馨提示',
                detail: '您当前货损赔偿大于此订单预期收益，请您联系客服。',
                actions: [
                    { title: '取消', },
                    { title: '联系客服', onPress: () => RouterHelper.navigate('联系客服', 'WebPage', {uri: ServicesApi.CUSTOMER_SERVICE}) },
                ],
            };
            AlertManager.show(params);
            return
        }
        let detail =
            <View>
                {
                    dataSource.platform_payment_type == 4 && 
                    (dataSource.carrier_order_status == 20 || dataSource.carrier_order_status == 21) &&
                    <View>
                        <Text style={styles.s} numberOfLines={1}/>
                        <Text style={styles.s} numberOfLines={1}>付款比例：</Text>
                        { dataSource.order_pay_list[0].platform_oil !== '' && <Text style={styles.s} numberOfLines={1}>油卡：{dataSource.order_pay_list[0].platform_oil || 0}</Text> }
                        { dataSource.order_pay_list[0].platform_remit !== '' && <Text style={styles.s} numberOfLines={1}>汇款：{dataSource.order_pay_list[0].platform_remit || 0}</Text> }
                    </View>
                }
                {
                    dataSource.platform_payment_type == 4 && 
                    ( dataSource.carrier_order_status == 70 || dataSource.carrier_order_status == 71 ) &&
                    <View>
                        <Text style={styles.s} numberOfLines={1}/>
                        <Text style={styles.s} numberOfLines={1}>付款比例：</Text>
                        { dataSource.order_pay_list[1].platform_oil !== '' && <Text style={styles.s} numberOfLines={1}>油卡：{dataSource.order_pay_list[1].platform_oil || 0}</Text> }
                        { dataSource.order_pay_list[1].platform_remit !== '' && <Text style={styles.s} numberOfLines={1}>汇款：{dataSource.order_pay_list[1].platform_remit || 0}</Text> }
                    </View>
                }
                {
                    dataSource.platform_payment_type == 4 && 
                    ( dataSource.carrier_order_status == 72 || dataSource.carrier_order_status == 73 ) &&
                    <View>
                        <Text style={styles.s} numberOfLines={1}/>
                        <Text style={styles.s} numberOfLines={1}>付款比例：</Text>
                        { dataSource.order_pay_list[0].platform_oil !== '' && <Text style={styles.s} numberOfLines={1}>油卡：{dataSource.order_pay_list[0].platform_oil || 0}</Text> }
                        { dataSource.order_pay_list[0].platform_remit !== '' && <Text style={styles.s} numberOfLines={1}>汇款：{dataSource.order_pay_list[0].platform_remit || 0}</Text> }
                    </View>
                }
                <TextInput
                    defaultValue={price + ''}
                    style={styles.confirmInput}
                    placeholder={'请输入'}
                    placeholderTextColor={'#999'}
                    onChangeText={(text) => this.setState({price: text})}
                    keyboardType={'phone-pad'}
                />
            </View>;
        let params = {
            title: '填写收款金额',
            detail,
            actions: [
                { title: '取消', },
                { title: '确定', onPress: () => this.onSubmitCollection() },
            ],
        };
        AlertManager.show(params);
        return true;
    }
    
    // 确认收款
    onSubmitCollection = async () => {
        let {item, onRefresh} = this.params;
        let {price} = this.state;
        if (price == '') {
            ToastManager.message('请填写支付金额');
            return
        }
        let url = ServicesApi.PERSONAL_CENTER_CARRIER_CONFIRM_INCOME;
        let data = { 
            id: item.id, 
            money: price,
        };
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success(result.msg);
            setTimeout(() => {
                onRefresh && onRefresh()
                RouterHelper.goBack()
            }, 1000);
        } else if (result.code === StatusCode.FAIL_CODE) {
            ToastManager.warn(result.msg);
        }
    }

    onSubmitPayWindow = () => {
        let {dataSource} = this.state;
        let params = {
            title: '温馨提醒',
            detail: '您需要支付服务费'+ dataSource.service_charge,
            actions: [
                {title: '再看看'},
                {
                    title: '支付服务费',
                    titleStyle: {color: Predefine.themeColor},
                    onPress: () => {
                        AlertManager.hide();
                        setTimeout(() => {
                            this.checkPayType()
                        }, 1000);
                    }
                },
            ]
        };
        AlertManager.show(params);
    }

    checkPayType = () => {
        let {accountType} = this.state;
        if (accountType == 0) {
            ToastManager.message('请选择支付方式');
            return
        } else if (accountType == 3) {
            let password = '';
            let detail = (
                <TextInput
                    secureTextEntry={true}
                    style={styles.passwordInput}
                    placeholder={'请输入'}
                    placeholderTextColor={'#999'}
                    onChangeText={(text) => password = text}
                />
            );
            let params = {
                title: '请输入支付密码',
                detail,
                actions: [
                    {title: '取消'},
                    {
                        title: '确认',
                        titleStyle: {color: Predefine.themeColor},
                        onPress: () => this.checkPassword(password)
                    },
                ]
            };
            AlertManager.show(params);
        } else {
            this.onSubmitPay()
        }
    }

    // 检查余额支付的密码
    checkPassword = async (password) => {
        let url = ServicesApi.PERSONAL_CENTER_CHECK_SECONDARY_PASSWORD;
        let data = {
            secondary_password: password,
        }
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.onSubmitPay()
        } else {
            ToastManager.fail(result.msg);
        }
    }

    // 支付服务费
    onSubmitPay = async () => {
        let {item, onRefresh} = this.params;
        let {accountType} = this.state;
        let url = ServicesApi.PAY_CONTROLLER_CARRIER_CONFIRM_INCOME;
        let data = {
            id: item.id, // 货源id
            account_type: accountType, // 1、支付宝，2、微信、3：余额
        }
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            if (accountType == 3) { // 余额
                ToastManager.success(result.msg);
                setTimeout(() => {
                    onRefresh && onRefresh()
                    RouterHelper.goBack()
                }, 1000);
            } else {
                this.onArousePayment(result.data);
            }
        } else if (result.code === StatusCode.FAIL_CODE) {
            ToastManager.warn(result.msg);
        }
    }

    // 支付
    onArousePayment = async (data) => {
        let {onRefresh} = this.params;
        let { accountType } = this.state;
        let type = '';
        if (accountType === 1) {
            type = Constants.PAY_ALIPAY
        } else if (accountType === 2) {
            type = Constants.PAY_WECHAT
        }
        let result = await PayManager.pay(type, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.message(result.msg);
            setTimeout(() => {
                onRefresh && onRefresh()
                RouterHelper.goBack()
            }, 1000);
        } else {
            ToastManager.message(result.msg);
        }
    }

    // 取消订单弹窗
    cancelOrderWindow = async () => {
        let {cancel_order_reasons} = this.state;
        let detail = 
            <TextInput
                defaultValue={cancel_order_reasons}
                style={styles.confirmTextArea}
                multiline={true}
                textAlignVertical={'top'}
                placeholder={'请输入'}
                placeholderTextColor={'#999'}
                onChangeText={(text) => this.setState({cancel_order_reasons: text})}
            />;
        let params = {
            title: '请填写取消原因',
            detail,
            actions: [
                { title: '取消', },
                { title: '确定', onPress: () => this.cancelOrder() },
            ],
        };
        AlertManager.show(params);
        return true;
    }

    // 取消订单
    cancelOrder = async () => {
        let {item, onRefresh} = this.params;
        let {cancel_order_reasons} = this.state;
        if (cancel_order_reasons === '') {
            AlertManager.hide()
            ToastManager.message('请填写取消原因，后台工作人员将根据实际情况进行审核。');
            return
        }
        let url = ServicesApi.PERSONAL_CENTER_CANCEL_ORDER;
        let data = { 
            id: item.id, 
            cancel_order_reasons: cancel_order_reasons,
        };
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success(result.msg);
            setTimeout(() => {
                onRefresh && onRefresh()
                RouterHelper.goBack()
            }, 1000);
        } else if (result.code === StatusCode.FAIL_CODE) {
            ToastManager.warn(result.msg);
        }
    }

    // 评价弹窗
    evaluateWindow = async () => {
        let params = {
            title: '温馨提示',
            detail: '确定您的评价',
            actions: [
                {title: '取消'},
                {
                    title: '确认',
                    titleStyle: {color: Predefine.themeColor},
                    onPress: () => this.evaluate()
                },
            ]
        };
        AlertManager.show(params);
    }

    // 评价
    evaluate = async () => {
        let {item, onRefresh} = this.params;
        let {star1, star2, star3, star4} = this.state;
        let star = star1 + ',' + star2 + ',' + star3 + ',' + star4;
        let url = ServicesApi.ORDER_EVALUATE;
        let data = {
            id: item.id, // 货源id
            evaluate: star,
            type: 1, // 0货主 1承运端
        }
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success(result.msg);
            setTimeout(() => {
                onRefresh && onRefresh();
                RouterHelper.goBack();
            }, 1000);
        } else if (result.code === StatusCode.FAIL_CODE) {
            ToastManager.warn(result.msg);
        }
    }

    // 获取
    getEvaluate = (val1, val2, val3, val4) => {
        this.setState({
            star1: val1,
            star2: val2,
            star3: val3,
            star4: val4,
        })
    }

    openMap = (type) => {
        let params = {
            title: '即将离开APP',
            detail: '为了给您带来更好的体验，请确保您的手机中安装有百度地图APP。',
            actions: [
                { title: '取消', },
                { title: '确定', onPress: () => this.openBaiduMapApp(type) },
            ],
        };
        AlertManager.show(params);
        return true;
    }

    openBaiduMapApp = (type) => {
        let {address, warp, weft, from_address, from_warp, from_weft, to_address, to_warp, to_weft} = this.state;
        if (type == 'start') {
            MapApp.openDrivingRoute(
                {latitude: Number(weft), longitude: Number(warp), name: address}, 
                {latitude: Number(from_weft), longitude: Number(from_warp), name: from_address},
            )
        } else if (type == 'end') {
            MapApp.openDrivingRoute(
                {latitude: Number(weft), longitude: Number(warp), name: address}, 
                {latitude: Number(to_weft), longitude: Number(to_warp), name: to_address}, 
            )
        }
    }

    onMakeCall = (data) => {
        Linking.openURL('tel:' + data).catch(e => console.log(e));
    }

    // 支付方式选择
    selectPayTypeView = (accountType) => {
        return (
            <TouchableOpacity style={styles.card} onPress={() => this.payTypeChoice()}>
                <View style={styles.payCardHeader}>
                    <View style={styles.payCardHeaderBox}>
                        <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                        <Text style={styles.cardHeaderTitle}>支付方式</Text>
                    </View>
                    <View style={styles.payCardHeaderBox}>
                        {
                            accountType == 0 && 
                            <Text style={styles.payCardHeaderText}>请选择支付方式</Text>
                        }
                        {
                            accountType == 1 && 
                            <View style={styles.payCardHeaderTextBox}>
                                <IconFont.AntDesign name={'alipay-circle'} size={16} color={'#108ee9'} style={styles.payCardHeaderTextIcon}/>
                                <Text style={[styles.payCardHeaderText, {color: '#108ee9'}]}>支付宝支付</Text>
                            </View>
                        }
                        {
                            accountType == 2 && 
                            <View style={styles.payCardHeaderTextBox}>
                                <IconFont.AntDesign name={'wechat'} size={16} color={'#01BD0C'} style={styles.payCardHeaderTextIcon}/>
                                <Text style={[styles.payCardHeaderText, {color: '#01BD0C'}]}>微信支付</Text>
                            </View>
                        }
                        {
                            accountType == 3 && 
                            <View style={styles.payCardHeaderTextBox}>
                                <IconFont.Entypo name={'wallet'} size={16} color={'#FF651D'} style={styles.payCardHeaderTextIcon}/>
                                <Text style={[styles.payCardHeaderText, {color: '#FF651D'}]}>余额支付</Text>
                            </View>
                        }
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.payCardHeaderImg}
                            source={Images.icon_arrow_right}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    // 底部按钮视图
    bottomBtnView = (dataSource) => {
        let {item} = this.params;
        return (
            <View style={styles.bottomBtnBox}>
                {
                    (
                        ( dataSource.receipt_type == 1 && dataSource.service_pay_status == 0 ) || 
                        ( dataSource.receipt_type == 2 && dataSource.platform_operate_mode == 1 && dataSource.is_pass == 2 ) ||
                        ( dataSource.receipt_type == 2 && dataSource.platform_operate_mode == 2 && dataSource.service_pay_status == 0 )
                    ) &&
                    <Button
                        title={'支付服务费'}
                        style={styles.bottomBtn}
                        titleStyle={styles.bottomBtnTitle}
                        onPress={() => this.onSubmitPayWindow()}
                    />
                }
                {
                    ( dataSource.carrier_order_status == 20 || dataSource.carrier_order_status == 21 ) &&
                    <Button
                        title={'确认收到预付款'}
                        style={styles.bottomBtn}
                        titleStyle={styles.bottomBtnTitle}
                        onPress={() => this.onSubmitCollectionWindow()}
                    />
                }
                {
                    (
                        dataSource.carrier_order_status == 70 ||
                        dataSource.carrier_order_status == 71 ||
                        dataSource.carrier_order_status == 72 ||
                        dataSource.carrier_order_status == 73
                    ) &&
                    <Button
                        title={'确认收到运费'}
                        style={[styles.bottomBtn, styles.bottomBtn1]}
                        titleStyle={styles.bottomBtnTitle1}
                        onPress={() => this.onSubmitCollectionWindow()}
                    />
                }
                {
                    ( dataSource.carrier_order_status >= 80 && dataSource.carrier_order_status <= 83 ) &&
                    <Button
                        title={'提交评价'}
                        style={[styles.bottomBtn, styles.bottomBtn1]}
                        titleStyle={[styles.bottomBtnTitle, styles.bottomBtnTitle1]}
                        onPress={() => this.evaluateWindow()}
                    />
                }
                {
                    dataSource.carrier_order_status > 50 && // 运输完成
                    <Button
                        title={'电子运单'}
                        style={[
                            styles.bottomBtn, 
                            (
                                dataSource.carrier_order_status == 70 ||
                                dataSource.carrier_order_status == 71 ||
                                dataSource.carrier_order_status == 72 ||
                                dataSource.carrier_order_status == 73 ||
                                ( dataSource.carrier_order_status >= 80 && dataSource.carrier_order_status <= 83 )
                            ) && styles.bottomBtn2
                        ]}
                        titleStyle={styles.bottomBtnTitle}
                        onPress={() => RouterHelper.navigate('', 'ElectronicsOrder', {item})}
                    />
                }
            </View>
        )
    }
    
    render() {
        let pageTitle = '订单详情';
        let {loading, dataSource, accountType} = this.state;
        let {item} = this.params;
        let data = { // 订单详情的接单总计数据 ApplySupplyCard
            weight: dataSource.carrier_receipt_weight,
            price: dataSource.carrier_transportation_unit_price,
            distance: dataSource.distance,
            total: dataSource.carrier_transportation_unit_price * dataSource.carrier_receipt_weight,
            percentage: (dataSource.carrier_transportation_unit_price * dataSource.carrier_receipt_weight) * (dataSource.service_percentage / 100),
            showPercentage: (dataSource.receipt_type == 2 && dataSource.platform_operate_mode == 1) ? false : true,
        }
        return (
            <PageContainer 
                loading={loading} 
                style={styles.container}
                fitIPhoneX={true} 
                fitNotchedScreen={Predefine.isNotchedScreen}
            >
                <NavigationBar 
                    title={pageTitle}
                    style={styles.navigationBar}
                    renderRightAction={[
                        ( 
                            (dataSource.is_pass == 1 || dataSource.is_pass == 2 ) && 
                            dataSource.cancel_button === 0 &&
                            dataSource.service_pay_status <= 50
                        ) && 
                        {
                            title: <IconFont.Feather name={'trash-2'} size={20} color={'#fff'}/>, 
                            onPress: () => this.cancelOrderWindow()
                        },
                        {
                            title: <IconFont.AntDesign name={'sharealt'} size={20} color={'#fff'}/>, 
                            onPress: () => RouterHelper.navigate('', 'ShareOrder', {item})
                        },
                        {
                            title: <IconFont.Ionicons name={'md-time'} size={20} color={'#fff'}/>, 
                            onPress: () => RouterHelper.navigate('', 'OrderTimeLine', {item})
                        }
                    ]}
                />
                <View style={styles.headerPadding}/>
                <ScrollView
                    style={styles.content}
                    refreshControl={<ListHeaderLoading onRefresh={this._onRefresh} refreshing={this.state.isRefreshing} />}
                >
                    {
                        dataSource.is_pass == 0 &&
                        <View style={styles.noPass}>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.noPassImg}
                                source={Images.icon_warring_logo1}
                            />
                            <Text style={styles.noPassText}>拒绝原因：{dataSource.reasons}</Text>
                        </View>
                    }
                    <View style={[styles.contentItemView, styles.contentSmallItemView]}>
                        <View style={styles.contentItemBgView} />
                        <ImageBackground
                            resizeMode={'stretch'}
                            style={styles.detailAddressView}
                            source={Images.img_bg_block_content}
                        >
                            <AddressInfo item={dataSource} style={styles.addressInfoStyle} />
                            <HorizontalLine style={styles.addressInfoSeparator} />
                            <View style={[Predefine.RCB, styles.cargoInfoCardContent]}>
                                <View style={[Predefine.CCC, styles.cargoInfoCardItemView]}>
                                    <Text style={styles.cargoInfoCardItemValue} numberOfLines={1}>{dataSource.goods_name}</Text>
                                    <View style={[Predefine.RCC, styles.cargoInfoCardItemTitleView]}>
                                        <ImageView source={Images.icon_text} style={styles.cargoInfoCardItemTitleIcon} />
                                        <Text style={styles.cargoInfoCardItemTitle}>货物名称</Text>
                                    </View>
                                </View>
                                <VerticalLine style={styles.cargoInfoCardSeparator} />
                                <View style={[Predefine.CCC, styles.cargoInfoCardItemView, styles.cargoInfoCardItemViewCenter]}>
                                    <Text style={styles.cargoInfoCardItemValue} numberOfLines={1}>{dataSource.goods_dangerous_level_text}</Text>
                                    <View style={[Predefine.RCC, styles.cargoInfoCardItemTitleView]}>
                                        <ImageView source={Images.icon_grid} style={styles.cargoInfoCardItemTitleIcon} />
                                        <Text style={styles.cargoInfoCardItemTitle}>货物类型</Text>
                                    </View>
                                </View>
                                <VerticalLine style={styles.cargoInfoCardSeparator} />
                                <View style={[Predefine.CCC, styles.cargoInfoCardItemView]}>
                                    <Text style={styles.cargoInfoCardItemValue} numberOfLines={1}>{dataSource.remanent_weight}</Text>
                                    <View style={[Predefine.RCC, styles.cargoInfoCardItemTitleView]}>
                                        <ImageView source={Images.icon_rank} style={styles.cargoInfoCardItemTitleIcon} />
                                        <Text style={styles.cargoInfoCardItemTitle}>剩余吨数</Text>
                                    </View>
                                </View>
                            </View>
                        </ImageBackground>
                        <Text style={styles.orderId} numberOfLines={1}>订单号：{dataSource.order_no}</Text>
                    </View>
                    { ( dataSource.is_pass == 1 || dataSource.is_pass == 10 || dataSource.is_pass == 30 ) && <CompanyCard item={dataSource} {...this.props}/> }
                    <CarsCard item={dataSource} onRefresh={() => this._onRefresh()} {...this.props}/>
                    <ApplySupplyCard item={data} {...this.props}/>
                    { ( dataSource.is_pass == 1 || dataSource.is_pass == 2 ) && <PayListCard item={dataSource} {...this.props}/> }
                    {
                        (
                            dataSource.carrier_order_status >= 80 && dataSource.carrier_order_status <= 83
                        ) && <EvaluateCard getEvaluate={this.getEvaluate} {...this.props}/>
                    }
                    {dataSource.carrier_order_status == 90 && <EvaluateViewCard item={dataSource} {...this.props}/>}
                    { dataSource.service_pay_status == 0 && this.selectPayTypeView(accountType) }
                    {this.bottomBtnView(dataSource)}
                </ScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const contentWidth = Predefine.screenWidth - 20;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navigationBar: {
        zIndex: 10,
        position: 'absolute',
    },
    headerPadding: {
        height: NAV_BAR_HEIGHT,
    },
    rightAction: {
        width: 20,
        height: 20,
    },
    content: {
        
    },
    noPass: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: "#ffe599",
    },
    noPassImg: {
        width: 16,
        height: 16,
        marginRight: 10,
    },
    noPassText: {
        lineHeight: 16,
        fontSize: 15,
        flex: 1,
    },

    contentItemView: {
        minHeight: 100,
        marginBottom: 10,
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
    },
    contentSmallItemView: {
        paddingHorizontal: 10,
    },
    contentItemBgView: {
        top: 0,
        left: 0,
        height: 100,
        position: 'absolute',
        width: Predefine.screenWidth,
        backgroundColor: Predefine.themeColor,
    },
    detailAddressView: {
        width: contentWidth,
        paddingVertical: 15,
        paddingHorizontal: 15,
        // height: contentWidth * 0.45,
    },
    addressInfoStyle: {
        marginVertical: 0,
    },
    addressInfoSeparator: {
        marginVertical: 5,
        backgroundColor: Predefine.themeColor,
    },
    // 顶部发片底部的信息（货物名称，货物类型，货物重量）
    cargoInfoCardContent: {
        marginTop: 10,
    },
    cargoInfoCardItemView: {
        flex: 2,
    },
    cargoInfoCardItemViewCenter: {
        flex: 3,
    },
    cargoInfoCardItemValue: {
        fontSize: 15,
        fontWeight: '700',
        color: Predefine.themeColor,
    },
    cargoInfoCardItemTitleView: {
        marginTop: 5,
    },
    cargoInfoCardItemTitle: {
        fontSize: 14,
        color: '#666',
    },
    cargoInfoCardItemTitleIcon: {
        width: 12,
        height: 12,
        marginRight: 3,
    },
    cargoInfoCardSeparator: {
        width: 1,
        height: 25,
        backgroundColor: Predefine.themeColor,
    },
    orderId: {
        marginTop: 10,
        fontSize: 14,
        color: '#333',
        // textAlign: 'right',
    },

    // 确认弹窗input
    confirmInput: {
        paddingVertical: 0,
        fontSize: 15,
        color: '#333',
        marginVertical: 15,
        textAlign: 'center',
        borderWidth: 1,
        borderRadius: 5,
        width: 200,
        borderColor: '#f2f2f2',
    },
    confirmTextArea: {
        height: 100,
        width: 200,
        lineHeight: 20,
        borderWidth: 1,
        borderColor: '#f2f2f2',
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 10,
        fontSize: 15,
        color: '#333',
        marginVertical: 15,
    },
    
    // 底部的按钮
    bottomBtnBox: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    btnBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    bottomBtn: {
        width: SCREEN_WIDTH - 30,
        backgroundColor: DominantColor,
        borderRadius: 5,
    },
    bottomBtnTitle: {

    },
    bottomBtn1: {
        width: SCREEN_WIDTH / 2 - 20,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: DominantColor,
    },
    bottomBtnTitle1: {
        color: DominantColor,
    },

    bottomBtn2: {
        width: SCREEN_WIDTH / 2 - 20,
        backgroundColor: DominantColor,
    },

    // card
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBox: {
        borderTopWidth: Predefine.minPixel,
        borderTopColor: '#F0F0F0',
    },
    cardHeaderIcon: {
        marginLeft: 15,
        marginTop: 10,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },
    

    cardLineBox: {
        flex: 1,
    },
    cardHeaderBoxRight: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    cardLine: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
    }, 


    payCardHeader: {
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    payCardHeaderBox: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    payCardHeaderImg: {
        width: 15,
        height: 15,
    },
    payCardHeaderTextBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    payCardHeaderTextIcon: {
        marginRight: 8,
    },
    payCardHeaderText: {
        fontSize: 14,
        color: '#999',
    },

    passwordInput: {
        marginTop: 10,
        width: 200,
        height: 40,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        paddingHorizontal: 10,
        paddingVertical: 0,
        textAlign: 'center',
    },
});