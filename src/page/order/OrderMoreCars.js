/**
 * 易驰护运 - OrderMoreCars
 * http://menger.me
 * 更多车辆
 * @桓桓
 */

'use strict';

import React from 'react'
import {StyleSheet, Text, ScrollView, TouchableOpacity, View, Keyboard, Linking} from 'react-native'
import {
    IconFont,
    ApplyMessageCard,
    ApplySupplyCard,
} from '../../components'
import {inject, observer} from 'mobx-react'
import RouterHelper from '../../routers/RouterHelper';

@inject('loginStore')
@observer
export default class OrderMoreCars extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
        }
    }

    componentDidMount () {
        
    }

    onMakeCall = (item) => {
        Linking.openURL('tel:' + item).catch(e => console.log(e));
    }

    carList = () => {
        let {data} = this.params;
        let contain = data.map((item, index) => {
            return (
                // <TouchableOpacity
                //     key={index}
                //     style={styles.carLineItem}
                //     onPress={() => {
                //         if (item.vehicle_order_id !== 0) {
                //             RouterHelper.navigate('', 'OrderDriverDetail', {
                //                 type: 'orderkistitem',
                //                 item: {id: item.vehicle_order_id}
                //             })
                //         }
                //     }}
                // >
                //     <View style={styles.cardLine}>
                //         <Text style={styles.carLineTitle} numberOfLines={1}>{item.vehicle_number}</Text>
                //         <Text style={[styles.carLineText, {color: DominantColor}]} numberOfLines={1}>{item.status_text}</Text>
                //     </View>
                //     <View style={styles.cardLine}>
                //         <Text style={styles.carLineText} numberOfLines={1}>{item.driver_name}</Text>
                //         <Text style={[styles.carLineText]} numberOfLines={1} onPress={() => this.onMakeCall(item.driver_mobile)}>{item.driver_mobile} <IconFont.Feather name={'phone-call'} style={styles.carLineIcon}/></Text>
                //     </View>
                //     <View style={styles.cardLine}>
                //         <Text style={styles.carLineText} numberOfLines={1}>{item.supercargo_name}</Text>
                //         <Text style={[styles.carLineText]} numberOfLines={1} onPress={() => this.onMakeCall(item.supercargo_mobile)}>{item.supercargo_mobile} <IconFont.Feather name={'phone-call'} style={styles.carLineIcon}/></Text>
                //     </View>
                // </TouchableOpacity>
                <TouchableOpacity 
                    key={index} 
                    style={[styles.carCard, item.leisure == true && {backgroundColor: '#CAF2F7'}]}
                    onPress={() => {
                        if (item.vehicle_order_id !== 0) {
                            RouterHelper.navigate('', 'OrderDriverDetail', {
                                item: {id: item.vehicle_order_id}
                            })
                        }
                    }}
                >
                    <View style={styles.carCardLine}>
                        <View style={styles.carCardHeader}>
                            <View style={styles.carCardHeaderBall}/>
                            <Text style={styles.carCardHeaderText}>{item.status_text}</Text>
                        </View>
                        <Text style={styles.s} numberOfLines={1}>{item.carrier_vehicle_arrival_time}</Text>
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText} numberOfLines={1}>车牌号：{item.vehicle_number}</Text>
                        <Text style={styles.carCardLineText} numberOfLines={1}>司机姓名：{item.driver_name}</Text>
                    </View>
                    <View style={styles.carCardLine}>
                        <Text style={styles.carCardLineText} numberOfLines={1}>车辆类型：{item.vehicle_type_text}</Text>
                        <Text style={styles.carCardLineText} numberOfLines={1}>车辆载重：{item.vehicle_weight}吨</Text>
                    </View>
                    {
                        item.leisure == true &&
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.carCardImg}
                            source={Images.icon_choice_car}
                        />
                    }
                </TouchableOpacity>
            )
        })
        return contain
    }

    render() {
        let {loading} = this.state;
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar
                    title={'查看司机'}
                    style={styles.navigationBar}
                />
                <ScrollView>
                    {this.carList()}
                </ScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({

    // carLineItem: {
    //     borderWidth: 1,
    //     borderColor: DominantColor,
    //     borderRadius: 5,
    //     marginHorizontal: 15,
    //     marginTop: 10,
    //     paddingHorizontal: 10,
    // },
    // cardLine: {
    //     flex: 1,
    //     flexDirection: 'row',
    //     justifyContent: 'space-between',
    //     alignItems: 'center',
    // },
    // carLineTitle: {
    //     lineHeight: 30,
    //     fontSize: 15,
    //     color: '#333',
    // },
    // carLineText: {
    //     lineHeight: 26,
    //     fontSize: 13,
    //     color: '#333',
    // },
    // carLineIcon: {
    //     color: '#1E70FF',
    //     fontSize: 16,
    // },
    // more: {
    //     height: 30,
    //     lineHeight: 30,
    //     textAlign: 'right',
    //     marginHorizontal: 15,
    //     marginBottom: 10,
    //     color: DominantColor,
    // },






    carCard: {
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        marginTop: 10,
        marginHorizontal: 15,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    carCardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 25,
    },
    carCardHeaderBall: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
        backgroundColor: DominantColor,
    },
    carCardHeaderText: {
        color: DominantColor,
        fontSize: 14,
    },
    carCardLine: {
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineText: {
        flex: 1,
        fontSize: 14,
        color: '#333',
    },
    carCardImg: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: 28,
        height: 30,
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
    }, 
});