/**
 * 易驰护运 - PayPassword
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    StatusBar,
    TextInput,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import RouterHelper from '../../routers/RouterHelper';

export default class PayPassword extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            password: [],
        }
    }

    onPressKeyboard = (key) => {
        let { password } = this.state;
        let arr = password.slice()
        if (key === '取消') {
            RouterHelper.goBack();
            return
        }
        if (key === '删除') {
            arr.pop(key)
            this.setState({
                password: arr,
            })
            return
        }
        arr.push(key)
        this.setState({
            password: arr,
        }, () => {
            if (arr.length === 6) {
                this.submit()
            }    
        })
    }

    goBack = () => {
        setTimeout(() => {
            RouterHelper.goBack();
        }, 1000);
    }

    submit = async () => {
        let { password } = this.state;
        let arr = [];
        let url = ServicesApi.PERSONAL_CENTER_CHECK_SECONDARY_PASSWORD;
        let data = {
            secondary_password: password.join(''),
        }
        const result = await Services.post(url, data);
        this.setState({
            password: arr,
        })
        if (result.code === StatusCode.SUCCESS_CODE) {
            RouterHelper.navigate('', 'ChangePayPassword', { goBack: () => this.goBack() })
        } else {
            ToastManager.fail(result.msg);
        }
    }

    passwordView = () => {
        let { password } = this.state;
        let arr = new Array(6).fill('○');
        let content = arr.map((item, index) => {
            return (
                <Text key={index} style={styles.passwordItem} numberOfLines={1}>{password.length < index + 1 ? item : password[index]}</Text>
            )
        })
        return <View style={styles.passwordView}>{content}</View>
    }

    keyboardView = () => {
        let arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '取消', '0', '删除'];
        let content = arr.map((item, index) => {
            return (
                <TouchableOpacity 
                    key={index} 
                    style={styles.keyboardItem}
                    onPress={() => this.onPressKeyboard(item)}
                >
                    <Text style={styles.keyboardItemText} numberOfLines={1}>{item}</Text>
                </TouchableOpacity>
            )
        })
        return <View style={styles.keyboard}>{content}</View>
    }

    render() {
        let pageTitle = this.params.pageTitle || '支付密码';
        let {loading, password} = this.state;
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={pageTitle}/>
                <Text style={styles.title} numberOfLines={1}>请先输入支付密码</Text>
                {this.passwordView()}
                {this.keyboardView()}
            </PageContainer>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        lineHeight: 30,
        paddingVertical: 30,
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 16,
        color: '#333',
    },
    // passwordView
    passwordView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 60,
        height: SCREEN_HEIGHT - 90 - 240 - NAV_BAR_HEIGHT,
    },
    passwordItem: {
        flex: 1,
        textAlign: 'center',
        fontSize: 18,
        color: '#333',
    },
    // keyword
    keyboard: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    keyboardItem: {
        height: 60,
        width: '33.3%',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    keyboardItemText: {
        fontSize: 15,
        color: '#333',
    },
})