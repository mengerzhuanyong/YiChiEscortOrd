/**
 * 易驰护运 - ChangePassword
 * http://menger.me
 * 修改密码
 * @huanhuan
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    TextInput,
    StatusBar,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

export default class ChangePassword extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            oldPassword: '',
            newPassword1: '',
            newPassword2: '',
        }
    }

    onSubmit = async () => {
        let {oldPassword, newPassword1, newPassword2} = this.state;
        if (oldPassword == '') {
            ToastManager.message('请输入当前密码')
        }
        if (newPassword1 == '') {
            ToastManager.message('请输入新密码')
        }
        if (newPassword2 !== newPassword1) {
            ToastManager.message('新密码两次输入不一致')
        }
        let url = ServicesApi.PERSONAL_CENTER_SAVE_PASSWORD;
        let data = {
            old_password: oldPassword,
            password: newPassword1,
            password_confirm: newPassword2,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.success(result.msg);
            RouterHelper.goBack();
        } else {
            ToastManager.fail(result.msg);
        }
    }

    render() {
        let {loading} = this.state;
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={'修改密码'}/>
                <ScrollView style={styles.scroll}>
                    <View style={styles.line}>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.lineImg}
                            source={Images.icon_password_logo1}
                        />
                        <Text style={styles.lineTitle} numberOfLines={1}>当前密码</Text>
                        <TextInput
                            style={styles.lineInput}
                            placeholder={'请输入当前密码'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({oldPassword: text})}
                        />
                    </View>
                    <View style={styles.line}>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.lineImg}
                            source={Images.icon_password_logo1}
                        />
                        <Text style={styles.lineTitle} numberOfLines={1}>新密码</Text>
                        <TextInput
                            style={styles.lineInput}
                            placeholder={'请输入新密码'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({newPassword1: text})}
                        />
                    </View>
                    <View style={styles.line}>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.lineImg}
                            source={Images.icon_password_logo1}
                        />
                        <Text style={styles.lineTitle} numberOfLines={1}>确认新密码</Text>
                        <TextInput
                            style={styles.lineInput}
                            placeholder={'请确认密码'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({newPassword2: text})}
                        />
                    </View>
                    <View style={styles.btnBox}>
                        <Button
                            style={styles.btn}
                            title={'保存'}
                            titleStyle={styles.btnTitle}
                            resizeMode={'cover'}
                            backgroundImage={Images.img_btn_bg1}
                            onPress={() => this.onSubmit()}
                        />
                    </View>
                </ScrollView>
            </PageContainer>
        )
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    line: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginTop: 10,
        height: 40,
        paddingHorizontal: 15,
    },
    lineImg: {
        width: 12,
        height: 12,
        marginRight: 6,
    },
    lineTitle: {
        fontSize: 12,
        color: '#333',
    },
    lineInput: {
        flex: 1,
        textAlign: 'right',
        fontSize: 12,
        color: '#333',
    },
    
    btnBox: {
        padding: 15,
    },
    btn: {
        backgroundColor: DominantColor,
    },
})