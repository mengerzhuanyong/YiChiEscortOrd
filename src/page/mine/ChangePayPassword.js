/**
 * 易驰护运 - ChangePayPassword
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    StatusBar,
    TextInput,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import RouterHelper from '../../routers/RouterHelper';

export default class ChangePayPassword extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            password1: '',
            password2: '',
        }
    }

    onSubmit = async () => {
        let {goBack} = this.params;
        let {password1, password2} = this.state;
        if (password1 === '') {
            ToastManager.message('请输入新的密码');
            return
        }
        if (password2 === '') {
            ToastManager.message('请确认新的密码');
            return
        }
        if (password1.length !== 6 || password2.length !== 6) {
            ToastManager.message('支付密码长度应为6位');
            return
        }
        let url = ServicesApi.PERSONAL_CENTER_SAVE_SECONDARY_PASSWORD;
        let data = {
            secondary_password: password1,
            secondary_password_confirm: password2,
        }
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.message(result.msg);
            setTimeout(() => {
                goBack && goBack();
                RouterHelper.goBack();
            }, 1000);
        } else {
            ToastManager.warn(result.msg);
        }
    }

    render() {
        let pageTitle = this.params.pageTitle || '修改密码';
        let {loading, password1, password2} = this.state;
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={pageTitle}/>
                <ScrollView style={styles.scrollView}>
                    <View style={styles.line}>
                        <Text style={styles.title} numberOfLines={1}>请输入新密码</Text>
                        <TextInput
                            defaultValue={password1}
                            style={styles.input}
                            placeholder={'请输入'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({password1: text})}
                        />
                    </View>
                    <View style={styles.line}>
                        <Text style={styles.title} numberOfLines={1}>请确认新密码</Text>
                        <TextInput
                            defaultValue={password2}
                            style={styles.input}
                            placeholder={'请输入'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({password2: text})}
                        />
                    </View>
                </ScrollView>
                <Button
                    style={styles.btn}
                    title={'保存'}
                    titleStyle={styles.btnTitle}
                    resizeMode={'cover'}
                    backgroundImage={Images.img_btn_bg1}
                    onPress={() => this.onSubmit()}
                />
            </PageContainer>
        )
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    line: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 15,
        height: 50,
        borderBottomWidth: 1,
        borderColor: '#e1e1e1',
    },
    title: {
        fontSize: 14,
        color:'#666',
    },
    input: {
        fontSize: 14,
        color:'#333',
    },
    btn: {
        backgroundColor: DominantColor,
        marginHorizontal: 15,
        marginVertical: 10,
    },
})