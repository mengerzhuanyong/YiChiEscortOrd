/**
 * 易驰护运 - AboutUs
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    StatusBar,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

export default class AboutUs extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            navArray: [
                {title: '修改密码', icon: Images.icon_user_customer, component: 'Login'},
                {title: '清除缓存', icon: Images.icon_user_profile, component: 'Login'},
                {title: '检查更新', icon: Images.icon_user_profile, component: 'Login'},
            ],
            xuans_show: [],
            xuans_synopsis: '',
            user_services_link: '',
            privacy_policy_link: '',
            user_services_title: '《用户服务协议》',
            privacy_policy_title: '《隐私政策协议》',
            powerby: "河南趣猫网络科技有限公司\nCopyright © 2020-2030 Qumao.\nAll Rights Reserved.",
        }
    }

    _onPressToNavigate = (pageTitle, component, params) => {
        RouterHelper.navigate(pageTitle, component, params);
    };

    componentDidMount() {
        // this._requestDataSources();
    }

    componentWillUnmount() {
    }

    _requestDataSources = async () => {
        let url = ServicesApi.RULE_ABOUTUS;
        let data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                loading: false,
                powerby: result.data.powerby,
                xuans_show: result.data.xuans_show,
                xuans_synopsis: result.data.xuans_synopsis,
                user_services_link: result.data.user_services_link,
                privacy_policy_link: result.data.privacy_policy_link,
                user_services_title: result.data.user_services_title,
                privacy_policy_title: result.data.privacy_policy_title,
            })
        }
    }

    _renderCompanyPhotoInfoView = (xuans_show) => {
        let contain = xuans_show && xuans_show.length !== 0 && xuans_show.map((item, index) => {
            return (
                <ImageView
                    key={index}
                    source={{uri: item}}
                    resizeMode={'cover'}
                    style={styles.contentImgStyle}
                />
            )
        })
        return contain
    }

    render() {
        let {xuans_show, xuans_synopsis, powerby, user_services_title, user_services_link, privacy_policy_title, privacy_policy_link, loading} = this.state;
        let pageTitle = this.params.pageTitle || '关于我们';
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar
                    title={pageTitle}
                />
                <ScrollView style={styles.scroll}>
                    <View style={styles.topBgStyle}></View>
                    <View style={styles.content}>
                        <View style={[Predefine.RCS,styles.logoBoxStyle]}>
                            <ImageView
                                resizeMode={'contain'}
                                source={Images.img_logo}
                                style={styles.logoImgStyle}
                            />
                            <Text style={styles.logoTextStyle}>易驰护运 <Text style={styles.versionStyle}>Version {Constants.VERSION_NAME}</Text></Text>
                        </View>
                        <View style={styles.contentBoxItemStyle}>
                            <Text style={styles.contentTitleStyle}>公司展示</Text>
                            <View style={[Predefine.WRAP, Predefine.RSS,styles.contentImgListStyle]}>
                                {this._renderCompanyPhotoInfoView(xuans_show)}
                            </View>
                        </View>
                        <View style={styles.contentBoxItemStyle}>
                            <Text style={styles.contentTitleStyle}>公司简介</Text>
                            <Text style={styles.contentStyle}>{xuans_synopsis}</Text>
                        </View>
                        <View style={[Predefine.CCC, styles.powerbyContent]}>
                            <View style={[Predefine.RCC, styles.protocolView]}>
                                <Text onPress={() => this._onPressToNavigate(user_services_title, 'WebPage', {uri: user_services_link})} style={[styles.powerbyItem, styles.powerbyItemCur]}>{user_services_title}</Text>
                                <Text style={[styles.powerbyItem, styles.powerbyItemCur]}>及</Text>
                                <Text onPress={() => this._onPressToNavigate(privacy_policy_title, 'WebPage', {uri: privacy_policy_link})} style={[styles.powerbyItem, styles.powerbyItemCur]}>{privacy_policy_title}</Text>
                            </View>
                            <Text style={styles.powerbyItem}>{powerby}</Text>
                        </View>
                    </View>
                </ScrollView>
            </PageContainer>
        )
    }
}

const styles = StyleSheet.create({
    navigationBarStyle: {
    },
    navtitleStyle: {
    },
    container: {
    },
    topBgStyle: {
        height: 60,
        backgroundColor: Predefine.themeColor,
    },
    content: {
        marginHorizontal: 15,
        marginBottom: 20,
        marginTop: -40,
    },
    // logo
    logoBoxStyle: {
        padding: 15,
        borderRadius: 6,
        backgroundColor: '#fff',
    },
    logoImgStyle: {
        width: 50,
        height: 50,
        marginRight: 15,
        backgroundColor: '#f60'
    },
    logoTextStyle: {
        fontSize: 18,
        color: Predefine.C333,
    },
    versionStyle: {
        fontSize: 14,
        color: Predefine.C333,
    },
    // content
    contentBoxItemStyle: {
        padding: 15,
        backgroundColor: '#fff',
        borderRadius: 6,
        marginTop: 20,
    },
    contentTitleStyle: {
        fontSize: 14,
        color: Predefine.C666,
        marginBottom: 20,
    },
    contentImgStyle: {
        width: (SCREEN_WIDTH - 80) / 2,
        height: 150,
        marginBottom: 20,
        marginHorizontal: 3,
    },
    contentStyle: {
        fontSize: 14,
        lineHeight: 22,
        color: Predefine.C999,
    },

    powerbyContent: {
        marginTop: 100,
    },
    powerbyItem: {
        fontSize: 12,
        color: '#999',
        lineHeight: 18,
        textAlign: 'center',
    },
    powerbyItemCur: {
        fontSize: 14,
        color: '#539ff3',
    },
})