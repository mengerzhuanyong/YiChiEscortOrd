/**
 * 易驰护运 - ChangeUserInfo
 * http://menger.me
 * 修改个人信息
 * @huanhuan
 */

'use strict';

import React from 'react';
import {
    Text,
    View,
    TextInput,
    StatusBar,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import {inject, observer} from 'mobx-react'

@inject('loginStore')
@observer
export default class ChangeUserInfo extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            name: '',
            avatar: '',
        }
    }

    componentDidMount () {
        this.requestData()
    }

    requestData = async() => {
        let {loginStore} = this.props;
        let {userInfo} = loginStore;
        this.setState({
            name: userInfo.nickname,
            avatar: userInfo.avatar,
            loading: false,
        })
    }

    // 提交图片（头像）
    _onSelectedPhotos = async () => {
        let _result = await MediaManager.showActionPicker({
            includeBase64: true,
        });
        if (_result && _result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.loading('上传中...', {duration: 9999999});
            const upRes = await Services.uploadQiNiu(_result.data);
            if (upRes.code === StatusCode.SUCCESS_CODE) {
                ToastManager.hide();
                ToastManager.success('上传成功');
                this.setState({avatar: ServicesApi.QI_NIU_HOST + upRes.data[0].key});
            } else {
                ToastManager.fail('上传失败');
            }
        }
    };

    // 提交
    onSubmit = async () => {
        let {onRefresh} = this.params;
        let {name, avatar} = this.state;
        let url = ServicesApi.PERSONAL_CENTER_SAVE;
        let data = {
            name,
            avatar,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            ToastManager.success(result.msg);
            onRefresh && onRefresh()
            RouterHelper.goBack()
        } else {
            ToastManager.fail(result.msg);
        }
    }

    render() {
        let {loading, name, avatar} = this.state;
        return (
            <PageContainer
                loading={loading}
                style={styles.container}
            >
                <NavigationBar title={'编辑个人信息'}/>
                <ScrollView style={styles.scroll}>
                    <View style={styles.card}>
                        <View style={styles.line}>
                            <Text style={styles.lineTitle}>用户名<Text style={{color: '#E71425'}}>*</Text>:</Text>
                            <TextInput
                                defaultValue={name}
                                style={styles.lineInput}
                                placeholder={'请输入用户名'}
                                placeholderTextColor={'#999'}
                                onChangeText={(text) => this.setState({name: text})}
                            />
                        </View>
                        <TouchableOpacity style={styles.line} onPress={() => this._onSelectedPhotos()}>
                            <Text style={styles.lineTitle}>头像<Text style={{color: '#E71425'}}>*</Text>:</Text>
                            <ImageView
                                resizeMode={'cover'}
                                style={styles.lineImg}
                                source={{uri: avatar}}
                            />
                        </TouchableOpacity>
                        <Button
                            style={styles.btn}
                            title={'保存'}
                            titleStyle={styles.btnTitle}
                            resizeMode={'cover'}
                            backgroundImage={Images.img_btn_bg1}
                            onPress={() => this.onSubmit()}
                        />    
                    </View>
                </ScrollView>
            </PageContainer>
        )
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F1EFF2',
    },

    card: {
        backgroundColor: '#fff',
    },
    line: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 15,
        borderBottomWidth: Predefine.minPixel,
        borderBottomColor: '#e1e1e1',
    },
    lineTitle: {
        fontSize: 14,
        color: '#333',
        paddingVertical: 15,
    },
    lineInput: {
        flex: 1,
        fontSize: 14,
        textAlign: 'right',
    },
    lineImg: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: '#e1e1e1',
        marginVertical: 10,
    },
    btn: {
        margin: 15,
        backgroundColor: DominantColor,
    },
})