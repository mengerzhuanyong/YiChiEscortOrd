/**
 * 易驰护运 - Withdraw
 * http://menger.me
 * 提现
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, TextInput, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard, ImageStore, PermissionsAndroid} from 'react-native';
import {
    IconFont,
    ListView,
    AddressInfo,
    VerticalLine,
    HorizontalLine,
    BannerComponent,
    HotNewsComponent,
    HomeResourceItem,
    ListHeaderLoading,
} from '../../components';
import CameraRoll from '@react-native-community/cameraroll';
import {inject, observer} from 'mobx-react';

@inject('loginStore')
@observer
export default class ShareOrder extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: false,
            dataSource: [],
        };
    }

    componentDidMount () {
        this.requestData()
    }

    requestData = async() => {
        let {item} = this.params;
        let url = ServicesApi.INDEX_SHARE;
        let data = {
            order_id: item.id,
        };
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                dataSource: result.data,
                loading: false,
            });
        }
    }

    // 点击分享
    onPressShare = async () => {
        let {dataSource} = this.state;
        let share_params = {
            url: dataSource.url,
            title: dataSource.title,
            text: dataSource.text,
            qr_code: dataSource.qr_code,
            imageUrl: dataSource.imageUrl,
            type: dataSource.type,
            // url:'http://huyun.yichi.3todo.com/api/Personal_Center/order_info?id=3',
            // title:'黄岛区光谷二路 至 邯郸市',
            // text:'黄岛区光谷二路 至 邯郸市',
            // qr_code:'http://qiniuyichihuyun.3todo.com/202004/FodeGfG398ycXLMg642bQGTLEGhD.png',
            // imageUrl:'http://qiniuyichihuyun.3todo.com/202004/Fuw33p5uS1kHAgme5voQooM-Eqjf.png',
            // type: 'link',
        };
        console.log('share_params------------->', share_params)
        ActionManager.showShare({share_params});
    }

    // 保存图片
    onPressToSaveImage = async () => {
        let imgSource = this.state.dataSource.qr_code;
        if (__ANDROID__) {
          let perRes = await PermissionsAndroid.request('android.permission.WRITE_EXTERNAL_STORAGE', null);
        }
        let result = await Services.download(imgSource);
        if (result) {
          const rollUri = __IOS__ ? result : `file://${result}`
          const saveRes = await CameraRoll.saveToCameraRoll(rollUri, 'photo');
          if (saveRes) {
            ToastManager.message('图片已保存至相册');
          } else {
            ToastManager.message('保存失败');
          }
        } else {
          ToastManager.message('下载失败')
        }
      };

    render() {
        let {loading, dataSource} = this.state;
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar
                    title={'分享订单'}
                    style={styles.navigationBarStyle}
                />
                <ScrollView keyboardShouldPersistTaps={'handled'}>
                    <View style={styles.header}>
                        <View style={[Predefine.RCS, styles.itemAddressItemView]}>
                            <Text style={styles.addressIconStyle} />
                            <Text style={styles.addressTitleStyle} numberOfLines={2}>{dataSource.from_province+''+dataSource.from_city+''+dataSource.from_district}</Text>
                        </View>
                        <IconFont.Fontisto name={'arrow-down-l'} style={[styles.addressArrowIconStyle]} />
                        <View style={[Predefine.RCS, styles.itemAddressItemView]}>
                            <Text style={[styles.addressIconStyle, styles.addressIconCurStyle]} />
                            <Text style={styles.addressTitleStyle} numberOfLines={2}>{dataSource.to_province+''+dataSource.to_city+''+dataSource.to_district}</Text>
                        </View>
                    </View>
                    <View style={styles.body}>
                        <View style={styles.leftArrow}/>
                        <View style={styles.bodyCenter}>
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.QRCode}
                                source={{uri: dataSource.qr_code}}
                            />
                            <Text style={styles.bodyCenterText} numberOfLines={1}>扫码可以直接查看订单实时详情</Text>
                            <View style={styles.centerBtnBox}>
                                <Button
                                    style={[styles.centerBtn, styles.centerBtn1]}
                                    title={'分享订单'}
                                    titleStyle={styles.centerBtnTitle1}
                                    resizeMode={'cover'}
                                    backgroundImage={Images.img_btn_bg1}
                                    onPress={() => this.onPressShare()}
                                />
                                <Button
                                    style={styles.centerBtn}
                                    title={'保存到相册'}
                                    titleStyle={styles.centerBtnTitle}
                                    resizeMode={'cover'}
                                    backgroundImage={Images.img_btn_bg1}
                                    onPress={() => this.onPressToSaveImage()}
                                />
                            </View>
                        </View>
                        <View style={styles.rightArrow}/>
                    </View>
                </ScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#42D9DF',
    },

    navigationBarStyle: {
        borderBottomWidth: 0,
        backgroundColor: '#42D9DF',
    },

    header: {
        width: SCREEN_WIDTH - 60,
        height: 120,
        backgroundColor: DominantColor,
        marginHorizontal: 30,
        marginTop: 15,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        paddingHorizontal: 15,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },

    addressIconStyle: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginRight: 6,
        overflow: 'hidden',
        backgroundColor: '#fff',
    },
    addressArrowIconStyle: {
        fontSize: 14,
        overflow: 'hidden',
        textAlign: 'center',
        color: '#fff',
        marginLeft: -3,
        marginVertical: 5,
    },
    addressIconCurStyle: {
        backgroundColor: Predefine.warnColor,
    },
    addressTitleStyle: {
        fontSize: 13,
        color: '#fff',
        fontWeight: '700',
        flex: 1,
    },

    body: {
        marginHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    leftArrow: {
        width:0,
        height:0,
        borderTopWidth:7,
        borderTopColor:'#008E98',
        borderLeftWidth:10,
        borderLeftColor:'transparent',
    },
    rightArrow: {
        width:0,
        height:0,
        borderTopWidth:7,
        borderTopColor:'#008E98',
        borderRightWidth:10,
        borderRightColor:'transparent',
    },
    bodyCenter: {
        backgroundColor: '#fff',
        width: SCREEN_WIDTH - 80,
        padding: 15,
        alignItems: 'center',
    },
    QRCode: {
        width: 200,
        height: 200,
        margin: 20,
        backgroundColor: '#e1e1e1',
    },
    bodyCenterText: {
        fontSize: 13,
        color: '#333',
        paddingVertical: 10,
        fontWeight: 'bold',
    },
    centerBtnBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: SCREEN_WIDTH - 110,
    },
    centerBtn: {
        marginTop: 20,
        width: SCREEN_WIDTH / 2 - 60,
        backgroundColor: '#FFA800',
    },
    centerBtn1: {
        borderWidth: 1,
        borderColor: '#FFA800',
        backgroundColor: '#fff',
    },
    centerBtnTitle1: {
        color: '#FFA800',
    },
});