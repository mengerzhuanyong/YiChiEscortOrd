/**
 * 易驰护运 - Level
 * http://menger.me
 * 等级规则
 * @桓桓
 */

'use strict';

import React from 'react'
import { StyleSheet, Text, ScrollView, TouchableOpacity, ImageBackground, View, Keyboard, ImageStore} from 'react-native';
import {
    IconFont,
    ListView,
    VerticalLine,
    HorizontalLine,
    BannerComponent,
    HotNewsComponent,
    HomeResourceItem,
    ListHeaderLoading,
} from '../../components';
import {inject, observer} from 'mobx-react';
import VipItem from '../../components/customer/Item/VipItem';

@inject('loginStore')
@observer
export default class Level extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            loading: true,
            logic: {}, // 逻辑
            dataSource: [], // 等级列表
        }
    }

    componentDidMount () {
        this.requestData()
    }

    requestData = async() => {
        let url = ServicesApi.PERSONAL_CENTER_LEVEL_LOGIC;
        let data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                logic: result.data.logic,
                dataSource: result.data.member_level_text,
                loading: false,
            });
        }
    }

    levelListView = (dataSource) => {
        let contain = dataSource.map((item, index) => {
            return (
                <View key={index} style={styles.vipLevel}>
                    {index % 5 == 0 && <ImageView resizeMode={'contain'} style={styles.vipLevelImg} source={Images.icon_level_logo1}/>}
                    {index % 5 == 1 && <ImageView resizeMode={'contain'} style={styles.vipLevelImg} source={Images.icon_level_logo2}/>}
                    {index % 5 == 2 && <ImageView resizeMode={'contain'} style={styles.vipLevelImg} source={Images.icon_level_logo3}/>}
                    {index % 5 == 3 && <ImageView resizeMode={'contain'} style={styles.vipLevelImg} source={Images.icon_level_logo4}/>}
                    {index % 5 == 4 && <ImageView resizeMode={'contain'} style={styles.vipLevelImg} source={Images.icon_level_logo5}/>}
                    <ImageBackground style={styles.vipLevelRight} source={Images.icon_level_text_bg} resizeMode={'contain'}>
                        <Text style={styles.vipLevelText} numberOfLines={1}>{item.member_level}:{item.min_point}-{item.max_point}</Text>
                    </ImageBackground>
                </View>
            )
        }) 
        return contain
    }

    render() {
        let {loading, logic, dataSource} = this.state;
        let logicText = logic.carrier_member_point_logic || ''
        let _logicText = logicText.replace(/\[换行]/g, '\n')
        return (
            <PageContainer loading={loading} style={styles.container}>
                <NavigationBar
                    title={'会员规则'}
                    style={[styles.navigationBarStyle]}
                />
                <ScrollView>
                    <View style={styles.card}>
                        <View style={styles.cardHeader}>
                            <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                            <Text style={styles.cardHeaderText} numberOfLines={1}>积分获取逻辑</Text>
                        </View>
                        <Text style={styles.textLine}>{_logicText}</Text>
                    </View>
                    <View style={styles.card}>
                        <View style={styles.cardHeader}>
                            <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                            <Text style={styles.cardHeaderText} numberOfLines={1}>等级计算规则</Text>
                        </View>
                        {this.levelListView(dataSource)}
                    </View>    
                </ScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F0F0F0'
    },

    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 13,
        fontWeight: 'bold',
    },

    textLine: {
        color: '#333',
        fontSize: 12,
        marginBottom: 10,
        lineHeight: 20,
    },

    vipLevel: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    vipLevelImg: {
        width: 40,
        height: 40,
        marginRight: 10,
    },
    vipLevelRight: {
        width: 200,
        height: 50,
        paddingLeft: 20,
        justifyContent: 'center',
    },
    vipLevelText: {
        color: '#968101',
        fontSize: 12,
    },
});