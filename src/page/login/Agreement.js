/**
 * 易驰护运 - agreement
 * http://menger.me
 * 协议
 * @桓桓
 */

'use strict';
import React from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity, View, Keyboard} from 'react-native'
import Webview from 'react-native-webview';

export default class Agreement extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {

        }
    }

    render() {
        let {agreementTitle, agreementUrl} = this.params;
        return (
            <PageContainer
                fitNotchedScreen={Predefine.isNotchedScreen}
                style={styles.container}
            >
                <NavigationBar title={agreementTitle || '协议'} style={styles.navigationBar}/>
                <Webview
                    startInLoadingState={true}
                    source={{uri: agreementUrl}}
                    style={styles.webContainer}
                />
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({

});