/**
 * 易驰护运 - Forget
 * http://menger.me
 * 忘记密码
 * @桓桓
 */

'use strict';
import React from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity, View, Keyboard} from 'react-native'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view"
import loginStyle from '../../style/loginStyle'
import {FormValidation} from '../../utils/validation/FormValidation'

export default class Forget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 60,
            codeBtnText: '获取验证码',

            emp_no: '',
            code: '',
            password: '',
            password_confirm: '',
        }
    }

    componentWillUnmount () {
        this.timer && clearInterval(this.timer);
    }

    // 获取验证码
    requestGetCodeSms = async () => {
        let {time, emp_no} = this.state
        if (time < 60) return false
        if (emp_no == '') {
            ToastManager.message('请输入手机号')
            return
        }
        let url = ServicesApi.SEND_SMS;
        let data = {
            mobile: emp_no,
            type: 3,
            app_type: 2,  // 1货主  2承运商
        };
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success('短信发送成功，请注意查收')
            this.countDown()
        } else {
            ToastManager.warn(result.msg);
        }
    };

    // 倒计时
    countDown = () => {
        let {time} = this.state
        this.timer = setInterval(()=> {
            if (time == 0){
                clearInterval(this.timer);
                this.setState({
                    time: 60,
                    codeBtnText: '获取验证码'
                })
            } else{
                this.setState({
                    time: time,
                    codeBtnText: time + '秒后重新获取'
                })
            }
            time--
        }, 1000)
    }

    // 提交表单
    onSubmit = async () => {
        Keyboard.dismiss();
        let {emp_no, code, password, password_confirm} = this.state;
        let url = ServicesApi.LOGIN_FIND_PASSWORD;
        let data = {
            emp_no,
            code,
            password,
            password_confirm,
            app_type: 2,  // 1货主  2承运商
        }
        let result = await Services.post(url, data);
        if (result.code == StatusCode.SUCCESS_CODE) {
            ToastManager.success(result.msg);
            setTimeout(()=> {
                RouterHelper.goBack()
            }, 1000);
        } else if (result.code === StatusCode.FAIL_CODE) {
            ToastManager.warn(result.msg)
        }
    };

    render() {
        let {codeBtnText, emp_no, code, password, password_confirm} = this.state
        return (
            <PageContainer
                fitNotchedScreen={Predefine.isNotchedScreen}
                style={styles.container}
            >
                <NavigationBar title={'找回密码'} style={styles.navigationBar}/>
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={'handled'}
                    style={styles.contentBoxStyle}
                >
                    <View style={styles.inputLine}>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.inputIcon}
                            source={Images.icon_login_icon1}
                        />
                        <TextInput
                            style={styles.input}
                            placeholder={'请输入手机号'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({emp_no: text})}
                            keyboardType={'phone-pad'}
                            maxLength={11}
                            defaultValue={emp_no}
                        />
                    </View>
                    <View style={styles.inputLine}>
                        <TextInput
                            style={styles.input}
                            placeholder={'请输入验证码'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({code: text})}
                            keyboardType={'phone-pad'}
                            maxLength={11}
                            defaultValue={code}
                        />
                        <Text style={styles.code} numberOfLines={1} onPress={() => this.requestGetCodeSms()}>{codeBtnText}</Text>
                    </View>
                    <View style={styles.inputLine}>
                        <TextInput
                            style={styles.input}
                            placeholder={'请输入新密码'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({password: text})}
                            keyboardType={'phone-pad'}
                            // maxLength={11}
                            defaultValue={password}
                        />
                    </View>
                    <View style={styles.inputLine}>
                        <TextInput
                            style={styles.input}
                            placeholder={'请确认新密码'}
                            placeholderTextColor={'#999'}
                            onChangeText={(text) => this.setState({password_confirm: text})}
                            keyboardType={'phone-pad'}
                            // maxLength={11}
                            defaultValue={password_confirm}
                        />
                    </View>
                    <View style={styles.btnBox}>
                        <Button
                            title={'保存'}
                            style={styles.btn}
                            titleStyle={styles.btnTitle}
                            onPress={this.onSubmit}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </PageContainer>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    btnBox: {
        padding: 30,
    },
    btn: {
        backgroundColor: DominantColor,
    },

    inputLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 30,
        height: 30,
        marginTop: 20,
        borderBottomWidth: Predefine.minPixel,
        borderBottomColor: '#e1e1e1',
    },
    inputIcon: {
        width: 15,
        height: 15,
        marginRight: 6,
    },
    input: {
        paddingVertical: 0,
        flex: 1,
        fontSize: 14,
        color: '#333',
    },
    code: {
        color: '#007AFF',
        fontSize: 14,
    },
});