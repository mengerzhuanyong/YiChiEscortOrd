/**
 * 易驰护运 - API接口
 * http://menger.me
 * @大梦
 */

'use strict';

const HOST = 'http://puhuo.yichi.3todo.com';
const API_HOST = HOST + '/api/';
const API_HOST_V1 = API_HOST + 'v1/';
const H5_HOST = HOST + '/h5/page/';


const QI_NIU_HOST = 'http://qiniuyichihuyun.3todo.com/';
const QI_NIU_UPLOAD_HOST = 'http://up-z0.qiniup.com';

export default {
    /**
     * @系统 [HOST]
     */
    HOST: HOST,
    API_HOST: API_HOST,
    API_HOST_V1: API_HOST_V1,

    /**
     * @资源 [APP图标]
     */
    ICON_APP: API_HOST_V1 + '/icon_app.png',
    // 版本信息
    VERSION_INFO: API_HOST + 'version/index',

    /**
     * @七牛云
     */
    // 七牛主域名
    QI_NIU_HOST,
    // 上传地址
    QI_NIU_UPLOAD_HOST,
    // 获取token
    GET_OSS_TOKEN: API_HOST + 'Qiniu/huanhuan',

    /**
     * @百度云
     */
    // AccessToken
    BAIDU_ACCESS_TOKEN: API_HOST_V1 + 'Consumer/baiduToken',
    // 公安验证
    PERSON_VERIFY: 'https://aip.baidubce.com/rest/2.0/face/v3/person/verify',

    /**
     * @业务接口
     */
    // 短信验证码
    SEND_SMS: API_HOST + 'login/send',
    // 公共-用户协议
    ARTICLE_ARTICLE_LIST: API_HOST + 'article/article_list',
    // 检查有没有快过期的证件
    INDEX_CHECK_VALIDITY: API_HOST + 'Index/check_validity',
    // 默认系数
    GOODS_UNIT: API_HOST + 'goods/unit',

    /**
     * @品类
     */
    // 所有品类
    SETUP_CONFIG_LIST: API_HOST + 'Setup/config_list',
    // 车辆排放标准
    SETUP_VEHICLE_DISCHARGE_LIST: API_HOST + 'Setup/vehicle_discharge_list',
    // 货物支付方式
    SETUP_PLATFORM_PAYMENT_TYPE_LIST: API_HOST + 'Setup/platform_payment_type_list',
    // 货物的主要种类
    GOODS_PRODUCT_LIST: API_HOST + 'Goods/product_list',

    /**
     * @H5
     **/
    // 专属顾问
    PERSONAL_ADVISER: H5_HOST + 'personalAdviser/type/2',
    // 联系客服
    CUSTOMER_SERVICE: H5_HOST + 'customerService/type/2',
    // 公司简介
    COMPANY_PROFILE: H5_HOST + 'companyProfile/type/2',
    // web
    PERSONAL_CENTER_ABOUT_US: API_HOST + 'Personal_Center/about_us',
    // 正常地图
    INDEX_BAIDU_MAP: API_HOST + 'Index/baidu_map',
    // 司机地图
    VEHICLE_DRIVER_MAP: API_HOST + 'Vehicle/drive_map',

    /**
     * @Login
     **/
    // 登录
    LOGIN: API_HOST + 'login/member_login',
    // 注册
    REGISTER: API_HOST + 'login/member_reg',
    // 忘记密码
    LOGIN_FIND_PASSWORD: API_HOST + 'login/find_password',
    // 司机注册（添加车辆）
    LOGIN_DRIVER_REG: API_HOST + 'login/driver_reg',
    // 承运商认证（公司）
    LOGIN_AUTHENTICATION: API_HOST + 'login/authentication',
    // 承运商认证（个人）
    LOGIN_BIO_AUTHENTICATION: API_HOST + 'login/bio_authentication',

    /**
     * @首页
     **/
    // 协议
    ARTICLE_DETAIL: API_HOST + 'article/detail',
    // 轮播
    INDEX_BANNER_LIST: API_HOST + 'Index/banner_list',
    // 最新公告
    INDEX_NOTICE: API_HOST + 'index/notice',
    // 未读最新公告
    INDEX_UNREAD: API_HOST + 'Index/unread',
    // 读未读的公告
    INDEX_READ: API_HOST + 'Index/read',
    // 首页的menu功能
    INDEX_INDEX_MENU: API_HOST + 'index/index_menu',
    // 首页数据
    INDEX_DATE: API_HOST + 'Index/date',
    // 首页货源列表
    GOODS_GOODS_LIST: API_HOST + 'Goods/goods_list',
    // 测算
    MEASURE_MATH: API_HOST + 'measure/distance',
    // 热门城市
    SETUP_CITY_LIST: API_HOST + 'Setup/city_list',
    // 我的消息
    INDEX_MESSAGE_LIST: API_HOST + 'Index/message_list',

    /**
     * @货源大厅
     **/
    // 货源列表
    GOODS_GOODS_LIST: API_HOST + 'Goods/goods_list',
    // 货源详情
    GOODS_GOODS_INFO: API_HOST + 'Goods/goods_info',
    // 申请接单（页面）
    ORDER_RECEIVE_GOODS: API_HOST + 'Order/receive_goods',
    // 申请接单（按钮）
    PAY_CONTROLLER_SERVICE_PAY: API_HOST + 'Pay_Controller/service_pay',
    // 支付方式
    INDEX_PAY_TYPE: API_HOST + 'index/pay_type',

    /**
     * @行情资讯
     **/
    // 轮播
    NEWS_BANNER_LIST: API_HOST + 'News/banner_list',
    // 资讯列表
    NEW_NEWS_LIST: API_HOST + 'news/news_list',

    /**
     * @个人中心
     **/
    // 设置二级密码
    PERSONAL_CENTER: API_HOST + 'Personal_Center/save_secondary_password',
    // 个人信息
    USER_INFO: API_HOST + 'Personal_Center/index',
    // 个人信息 - 头像昵称
    PERSONAL_CENTER_SAVE: API_HOST + 'Personal_Center/save',
    // 订单列表
    PERSONAL_CENTER_ORDER_LIST: API_HOST + 'Personal_Center/order_list',
    // 订单详情
    PERSONAL_CENTER_ORDER_INFO: API_HOST + 'Personal_Center/order_info',
    // 待支付的订单 - 支付
    PAY_CONTROLLER_CARRIER_CONFIRM_INCOME: API_HOST + 'Pay_Controller/carrier_confirm_income',
    // 确认收款
    PERSONAL_CENTER_CARRIER_CONFIRM_INCOME: API_HOST + 'Personal_Center/carrier_confirm_income',
    // 客服电话
    PERSONAL_CENTER_CUSTOMER_SERVICE_PHONE: API_HOST + 'Personal_Center/customer_service_phone',
    // 个人中心 - 会员中心
    PERSONAL_CENTER_MEMBER_CENTER: API_HOST + 'Personal_Center/member_center',
    // 个人中心 - 等级规则
    PERSONAL_CENTER_LEVEL_LOGIC: API_HOST + 'Personal_Center/level_logic',
    // 车辆列表
    PERSONAL_CENTER_VEHICLE_LIST: API_HOST + 'Personal_Center/vehicle_list',
    // 车辆详情
    PERSONAL_CENTER_VEHICLE_INFO: API_HOST + 'Personal_Center/vehicle_info',
    // 修改车辆信息
    PERSONAL_CENTER_DRIVER_RE_REG: API_HOST + 'Personal_Center/driver_re_reg',
    // 钱包
    PERSONAL_CENTER_MY_BALANCE: API_HOST + 'Personal_Center/my_balance',
    // 钱包 - 提现(页面)
    PERSONAL_CENTER_ATM: API_HOST + 'Personal_Center/atm',
    // 钱包 - 提现(按钮)
    PERSONAL_CENTER_APPLY_ATM: API_HOST + 'Personal_Center/apply_atm',
    // 钱包 - 充值
    PAY_CONTROLLER_RECHARGE: API_HOST + 'Pay_Controller/recharge',
    // 支付密码（输入）
    PERSONAL_CENTER_CHECK_SECONDARY_PASSWORD: API_HOST + 'Personal_Center/check_secondary_password',
    // 支付密码（修改）
    PERSONAL_CENTER_SAVE_SECONDARY_PASSWORD: API_HOST + 'Personal_Center/save_secondary_password',
    // 未读
    PERSONAL_CENTER_ORDER_STATUS_NUM: API_HOST + 'Personal_Center/order_status_num',
    // 读未读的单
    PERSONAL_CENTER_ORDER_STATUS_READ: API_HOST + 'Personal_Center/order_status_read',
    // 修改密码
    PERSONAL_CENTER_SAVE_PASSWORD: API_HOST + 'Personal_Center/save_password',
    // 评价
    ORDER_EVALUATE: API_HOST + 'Order/evaluate',
    // 订单的时间轴
    ORDER_ORDER_LOG: API_HOST + 'Order/order_log',
    // 换车申请（页面）
    ORDER_CHANGE_VEHICLE: API_HOST + 'Order/change_vehicle',
    // 删除车辆（页面）
    ORDER_CANCEL_VEHICLE_LIST: API_HOST + 'Order/cancel_vehicle_list',
    // 换车申请（按钮）
    ORDER_CHANGE_VEHICLE_APPLY: API_HOST + 'Order/change_vehicle_apply',
    // 删除车辆（按钮）
    ORDER_CANCEL_VEHICLE_APPLY: API_HOST + 'Order/cancel_vehicle_apply',
    // 电子运单
    ORDER_ELECTRONIC_WAYBILL: API_HOST + 'Order/electronic_waybill',
    // 取消订单
    PERSONAL_CENTER_CANCEL_ORDER: API_HOST + 'Personal_Center/cancel_order',
    // 分享订单
    INDEX_SHARE: API_HOST + 'index/share',


    // 司机报告位置
    VEHICLE_COORDINATE_LIST: API_HOST + 'Vehicle/coordinate_list',
    // 司机订单列表
    VEHICLE_ORDER_LIST: API_HOST + 'Vehicle/order_list',
    // 司机订单详情
    VEHICLE_ORDER_INFO: API_HOST + 'Vehicle/order_info',
    // 司机时间轴
    VEHICLE_ORDER_LOG: API_HOST + 'Vehicle/order_log',
    // 司机的操作（司机每一个操作会使订单往下一步）
    VEHICLE_ORDER_OPERATE: API_HOST + 'Vehicle/order_operate',
    // 司机导航坐标
    VEHICLE_NAVIGATION: API_HOST + 'Vehicle/navigation',
};