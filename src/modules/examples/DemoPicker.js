/**
 * Example - DemoPicker
 * http://menger.me
 * @大梦
 */

'use strict';

import React, { useRef, useCallback } from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'

export default class DemoPicker extends React.PureComponent {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            dataSource: [
                {icon: '', title: '支付宝会员',},
                {icon: '', title: '账单',},
                {icon: '', title: '总资产',},
                {icon: '', title: '余额',},
                {icon: '', title: '余额宝',},
                {icon: '', title: '花呗',},
                {icon: '', title: '余利宝',},
                {icon: '', title: '银行卡',},
                {icon: '', title: '芝麻信用',},
                {icon: '', title: '蚂蚁保险',},
                {icon: '', title: '相互宝',},
                {icon: '', title: '借呗',},
                {icon: '', title: '网商银行',},
            ],
        };
    }

    lineListView = () => {
        let {dataSource} = this.state;
        let content = dataSource.map((item, index) => {
            return (
                <View key={index} style={styles.line}>
                    <ImageView
                        resizeMode={'contain'}
                        style={styles.lineIcon}
                        source={Images.s}
                    />
                    <View style={styles.lineDetail}>
                        <Text style={styles.lineTitle} numberOfLines={1}>{item.title}</Text>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.lineArrow}
                            source={Images.icon_arrow_right}
                        />
                    </View>
                </View>
            )
        })
        return content
    }

    render() {
        return (
            <PageContainer style={styles.container}>
                <NavigationBar
                    title={''}
                    renderLeftAction={[
                        {title: '我的', titleStyle: styles.titleStyle,},
                    ]}
                    renderRightAction={[
                        {title: '设置', titleStyle: styles.titleStyle,},
                    ]}
                />
                <ScrollView>
                    {this.lineListView()}
                </ScrollView>
            </PageContainer>
        );
    }
}

const themeColor = '';
const styles = StyleSheet.create({
    container: {},
    titleStyle: {
        color: '#fff',
        fontSize: 18,
    },
    line: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
    },
    lineIcon: {
        width: 20,
        height: 20,
        marginHorizontal: 15,
        backgroundColor: '#f2f2f2',
    },
    lineDetail: {
        flex: 1,
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: Predefine.minPixel,
        borderBottomColor: '#e1e1e1',
    },
    lineTitle: {
        fontSize: 15,
        color: '#333',
    },
    lineArrow: {
        width: 10,
        height: 15,
        marginHorizontal: 15,
    },
});