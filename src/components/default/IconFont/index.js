import Entypo from 'react-native-vector-icons/dist/Entypo'
import Zocial from 'react-native-vector-icons/dist/Zocial'
import Feather from 'react-native-vector-icons/dist/Feather'
import Octicons from 'react-native-vector-icons/dist/Octicons'
import Fontisto from 'react-native-vector-icons/dist/Fontisto'
import Ionicons from 'react-native-vector-icons/dist/Ionicons'
import AntDesign from 'react-native-vector-icons/dist/AntDesign'
import EvilIcons from 'react-native-vector-icons/dist/EvilIcons'
import Foundation from 'react-native-vector-icons/dist/Foundation'
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/dist/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons'
import SimpleLineIcons from 'react-native-vector-icons/dist/SimpleLineIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons'

export const IconFont = {
    Entypo,
    Zocial,
    Feather,
    Octicons,
    Fontisto,
    Ionicons,
    AntDesign,
    EvilIcons,
    Foundation,
    FontAwesome,
    FontAwesome5,
    MaterialIcons,
    SimpleLineIcons,
    MaterialCommunityIcons,
};
