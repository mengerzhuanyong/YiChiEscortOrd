/**
 * 悬赏平台 - BannerComponent
 * http://menger.me
 * @大梦
 */

import React from 'react'
import {StyleSheet, Text, TouchableOpacity, View,} from 'react-native'
import Swiper from 'react-native-swiper'
import {checkNumber} from '../../../utils/Tool'
import {Predefine} from '../../../config/predefine'

export default class BannerComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            swiperShow: false,
            data: this.props.data,
        };
    }

    static defaultProps = {
        data: [],
        showControl: true,
    };

    componentDidMount() {
        this.timer = setTimeout(() => {
            this.setState({
                swiperShow: true
            });
        }, 0)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data
        });
    }

    componentWillUnmount() {
        this.timer && clearTimeout(this.timer);
    }

    _renderBannerContent = (row) => {
        let {style, ...others} = this.props;
        if (this.state.swiperShow) {
            let banners = row.map((item, index) => {
                let source = Images.img_banner;
                let component = item.component || 'WebPage';
                if (item.img) {
                    source = checkNumber(item.img) ? item.img : {uri: item.img};
                }
                return (
                    <TouchableOpacity
                        style={[styles.bannerViewWrap]}
                        key={"banner_" + index}
                        activeOpacity={1}
                        onPress={() => {
                            item.url && RouterHelper.navigate(item.title || '网页', component, {
                                uri: item.url,
                                ...item,
                            });
                        }}
                    >
                        <ImageView
                            style={[styles.slideImgStyle, style]}
                            resizeMode={'cover'}
                            source={source}
                        />
                        {item.title && <View style={styles.bannerTitleViewStyle}>
                            <Text style={styles.bannerTitleStyle}>{item.title}</Text>
                        </View>}
                    </TouchableOpacity>
                )
            });
            return (
                <Swiper
                    key={row && row.length}
                    style={[styles.wrapper, style]}
                    paginationStyle={[styles.paginationStyle]}
                    onScrollBeginDrag={this.setSlidePagination}
                    dot={<View style={styles.slidePaginationItemStyle}/>}
                    activeDot={<View style={styles.slidePaginationActiveStyle}/>}
                    {...others}
                >{banners}</Swiper>
            );
        }
    };

    render() {
        let {data} = this.state;
        let {style, showControl, ...other} = this.props;
        return (
            <View style={[styles.container, style]}>
                {this._renderBannerContent(data)}
            </View>
        );
    }
}

const itemWidth = Predefine.screenWidth;
const itemHeight = Predefine.screenWidth / 2.1;
const styles = StyleSheet.create({
    container: {
        height: itemHeight,
    },
    wrapper: {
        height: itemHeight,
    },
    slideImgStyle: {
        borderRadius: 0,
        width: itemWidth,
        height: itemHeight,
    },
    paginationStyle: {
        bottom: 5,
    },
    slidePaginationItemStyle: {
        width: 12,
        height: 3,
        margin: 3,
        backgroundColor: '#a3a5a4',
    },
    slidePaginationActiveStyle: {
        width: 12,
        height: 3,
        margin: 3,
        backgroundColor: '#fff',
    },

});