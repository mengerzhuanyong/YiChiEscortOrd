'use strict';
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { Wheel } from 'teaset';

export default class TimeContent extends React.PureComponent {


    constructor(props) {
        super(props);
        let hours = [], minute = []
        for (let index = 0; index < 24; index++) {
            const data = `${index}`.replace(/\b(\d)\b/g, "0$1");
            hours.push(data)
        }
        for (let index = 0; index < 60; index++) {
            const data = `${index}`.replace(/\b(\d)\b/g, "0$1");
            minute.push(data)
        }
        this.state = {
            hours,
            minute,
            hoursIndex: 0,
            minuteIndex: 0,
        };
    }

    componentDidMount() {

    }

    _onChangeHours = (index) => {
        this.setState({ hoursIndex: index })
    }

    _onChangeMinute = (index) => {
        this.setState({ minuteIndex: index })
    }

    _onPressOK = () => {
        const { onPress } = this.props;
        const { hours, minute, hoursIndex, minuteIndex } = this.state;
        const data = {
            'hour': hours[hoursIndex],
            'minute': minute[minuteIndex],
        }
        // Log.print('标记', data)
        onPress && onPress(data)
        ActionManager.hide();
    };

    _onPressCancel = () => {
        ActionManager.hide();
    };

    render() {
        const { hours, minute, hoursIndex, minuteIndex } = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.actionContainer}>
                    <TouchableOpacity onPress={this._onPressCancel}>
                        <Text style={styles.actionText}>取消</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._onPressOK}>
                        <Text style={styles.actionText}>确定</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.wheelContainer}>
                    <View style={styles.wheelItemContainer}>
                        <Text style={styles.wheelItemTitle}>时</Text>
                        <Wheel
                            style={styles.wheelItem}
                            itemStyle={styles.itemStyle}
                            index={hoursIndex}
                            items={hours}
                            // defaultIndex={data.findIndex((item) => item === startTime)}
                            onChange={this._onChangeHours}
                        />
                    </View>
                    <Text style={styles.maoHao}>:</Text>
                    <View style={styles.wheelItemContainer}>
                        <Text style={styles.wheelItemTitle}>分</Text>
                        <Wheel
                            style={styles.wheelItem}
                            itemStyle={styles.itemStyle}
                            index={minuteIndex}
                            items={minute}
                            // defaultIndex={data.findIndex((item) => item === endTime)}
                            onChange={this._onChangeMinute}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    wheelContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10,
    },
    wheelItemContainer: {
        flex: 1,
        height: 180,
        justifyContent: 'center',
    },
    wheelItem: {
        flex: 1,
        marginTop: 10,
    },
    itemStyle: {
        textAlign: "center",
        // fontSize: fontSize(14),
    },
    actionContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingVertical: 10,
        paddingHorizontal: 30,
    },
    actionText: {
        // color: Theme.overallColor,
        // fontSize: fontSize(14),
    },
    wheelItemTitle: {
        alignSelf: 'center',
        // fontSize: FontSize(12),
        color: '#333',
        marginBottom: 20,
    },
    maoHao: {
        // marginTop: scaleSize(80)
    }
});


