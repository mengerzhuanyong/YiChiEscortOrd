/**
 * 易驰护运 - CarNumberPicker
 * http://menger.me
 * 车牌号键盘
 * @huanhaun
 */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { IconFont } from '../../../components'

// 需要重构
export default class CarNumberPicker extends React.PureComponent {

    constructor(props) {
        super(props);
        this.province = ['京','津','冀','晋','蒙','辽','吉','黑','沪','苏','浙','皖','闽','赣','鲁','豫','鄂','湘','粤','桂','琼','渝','川','黔','滇','藏','陕','甘','青','宁','新','台','港','澳'];
        this.letter = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        this.number = ['0','1','2','3','4','5','6','7','8','9']
        this.state = { 
            num: [],
        };
    }

    componentDidMount () {
        let {defaultData} = this.props;
        this.setState({num: defaultData.split('')})
    }

    _onPressCancel = () => {
        if (this.props.onPressCancel) {
            this.props.onPressCancel()
        } else {
            ActionManager.hide()
        }
    }

    _onPressOk = () => {
        let {num} = this.state;
        const { onPress } = this.props;
        const data = {num}
        onPress && onPress(data)
        ActionManager.hide();
    }

    pushNum = (item) => {
        let {num} = this.state;
        let arr = num.slice();
        if (num.length === 0) {
            let regex = new RegExp("^([\u4E00-\uFA29])$"); 
            let res = regex.test(item);
            if (res === false ) {
                ToastManager.message('车牌号首位请填写省级简称汉字');
                return
            }
        }
        if (num.length === 1) {
            let regex = new RegExp("^([A-Z])$"); 
            let res = regex.test(item);
            if (res === false ) {
                ToastManager.message('车牌号第二位请填写英文字母');
                return
            }
        }
        if (num.length > 1) {
            let regex = new RegExp("^([\u4E00-\uFA29])$"); 
            let res = regex.test(item);
            if (res === true ) {
                ToastManager.message('请填写英文字母或数字');
                return
            }
        }
        num.length < 8 && arr.push(item)
        this.setState({num: arr})
    }

    deleteNum = () => {
        let {num} = this.state;
        let arr = num.slice();
        arr.pop()
        this.setState({num: arr})
    }

    provinceBtn = () => {
        let contain1 = this.province.map((item, index) => {
            return <Text key={index} style={styles.bodyBtn} onPress={() => this.pushNum(item)}>{item}</Text>
        })
        let contain2 = this.letter.map((item, index) => {
            return <Text key={index} style={styles.bodyBtn} onPress={() => this.pushNum(item)}>{item}</Text>
        })
        let contain3 = this.number.map((item, index) => {
            return <Text key={index} style={styles.bodyBtn} onPress={() => this.pushNum(item)}>{item}</Text>
        })
        return <View style={styles.bodyBtnView}>{contain1}{contain2}{contain3}</View>
    }

    render() {
        let {num} = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerButton} onPress={this._onPressCancel}>
                        <Text style={styles.headerText}>取消</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.headerButton} onPress={this._onPressOk}>
                        <Text style={styles.headerText}>确定</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.body}>
                    <View style={styles.bodyTop}>
                        <View style={styles.numBox}>
                            <Text style={styles.num}>{num[0]}</Text>
                            <Text style={styles.num}>{num[1]}</Text>
                            <Text style={styles.num}>{num[2]}</Text>
                            <Text style={styles.num}>{num[3]}</Text>
                            <Text style={styles.num}>{num[4]}</Text>
                            <Text style={styles.num}>{num[5]}</Text>
                            <Text style={styles.num}>{num[6]}</Text>
                            <Text style={styles.num}>{num[7]}</Text>
                        </View>   
                        <IconFont.Feather name={'delete'} size={30} color={'#999'} onPress={() => this.deleteNum()}/>
                    </View>
                    {this.provinceBtn()}
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    // header
    header: {
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginBottom: 10
    },
    headerButton: {
        padding: 10
    },
    headerText: {
        fontSize: 14,
    },
    // body
    bodyTop: {
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    numBox: {
        flexDirection: 'row',
        marginRight: 15,
        flex: 1,
    },
    num: {
        color: '#333',
        fontSize: 26,
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#999',
        textAlign: 'center',
        lineHeight: 30,
        margin: 5,
    },
    bodyBtnView: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: 10,
    },
    bodyBtn: {
        height: 40,
        width: '12.5%',
        backgroundColor: '#fff',
        fontSize: 14,
        color: '#333',
        lineHeight: 40,
        textAlign: 'center',
        borderWidth: 1,
        borderColor: '#999',
        borderRadius: 3,
    },
});


