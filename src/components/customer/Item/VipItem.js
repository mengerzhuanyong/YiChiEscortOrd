/**
 * 易驰护运 - VipItem
 * http://menger.me
 * 积分获取记录
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';

export default class VipItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let {item} = this.props;
        return (
            <TouchableOpacity style={styles.item}>
                <View style={styles.itemLeft}>
                    <Text style={styles.itemTitle} numberOfLines={1}>{item.type_text}</Text>
                    <Text style={styles.itemTime} numberOfLines={1}>{item.create_time}</Text>
                </View>
                <Text style={styles.itemPrice} numberOfLines={1}>+{item.point}积分</Text>
            </TouchableOpacity>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 60,
        paddingHorizontal: 15,
    },
    itemLeft: {
        flex: 1,
    },
    itemTitle: {
        fontSize: 13,
        color: '#333',
        marginBottom: 8,
    },
    itemTime: {
        fontSize: 12,
        color: '#999',
    },
    itemPrice: {
        fontSize: 12,
        color: DominantColor,
    },
});