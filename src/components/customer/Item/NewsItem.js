/**
 * 易驰护运 - NewsItem
 * http://menger.me
 * 行情资讯的单条
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';

export default class NewsItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let {item} = this.props;
        return (
            <TouchableOpacity style={styles.item} onPress={() => RouterHelper.navigate('', 'NewsDetail', {item})}>
                <View style={styles.itemLeft}>
                    <Text style={styles.itemTitle} numberOfLines={3}>{item.name}</Text>
                    <View style={styles.time}>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.timeImg}
                            source={Images.icon_time_logo1}
                        />
                        <Text style={styles.timeText} numberOfLines={1}>{item.create_time}</Text>
                    </View>
                </View>
                <ImageView
                    resizeMode={'cover'}
                    style={styles.itemImg}
                    source={{uri: item.cover}}
                />
            </TouchableOpacity>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    item: {
        backgroundColor: '#fff',
        borderRadius: 5,
        height: 110,
        marginHorizontal: 10,
        marginTop: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    itemLeft: {
        flex: 1,
        height: 90,
        justifyContent: 'space-between',
    },
    itemTitle: {
        fontSize: 14,
        lineHeight: 22,
        color: '#333',
    },
    time: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    timeText: {
        fontSize: 12,
        color: '#999',
    },
    timeImg: {
        width: 13,
        height: 13,
        marginRight: 6,
    },
    itemImg: {
        backgroundColor: '#bfbfbf',
        width: 120,
        height: 90,
        marginLeft: 10,
    },
});