/**
 * 易驰护运 - HomeResourceItem
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'
import IconEntypo from 'react-native-vector-icons/dist/Entypo'
import IconFontisto from 'react-native-vector-icons/dist/Fontisto'
import AddressInfo from '../Common/AddressInfo'

export default class HomeResourceItem extends React.PureComponent {

    static defaultProps = {
        item: '',
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {}

    render() {
        let {item, style, onPress} = this.props;
        let operate_mode = '', payment_type = '';
        if ( item.receipt_type == 1 ) {
            operate_mode = item.shipper_operate_mode == 1 ? '竞价发布' : '定价发布'
        } else {
            operate_mode = item.platform_operate_mode == 1 ? '竞价发布' : '定价发布'
        }
        return (
            <TouchableOpacity
                style={[styles.container, style]}
                onPress={() => RouterHelper.navigate('', 'SupplyDetail', {item, payType: operate_mode})}
            >
                <View style={[Predefine.RCB, styles.itemTitleView]}>
                    <Text style={styles.itemTitleStyle} numberOfLines={1}>{item.goods_name}</Text>
                    <Text style={styles.itemPubTimeStyle}>发布时间：{item.create_time}</Text>
                </View>
                <AddressInfo item={item}/>
                <View style={[styles.itemDetailContent]}>
                    <View style={[Predefine.RCS, styles.itemInfoView]}>
                        <Text style={styles.itemInfoText}>总数：</Text>
                        <Text style={[styles.itemInfoText, styles.itemInfoTextCur]}>{item.goods_weight}吨</Text>
                        <Text style={[Predefine.ML5, styles.itemInfoText]}>剩余：</Text>
                        <Text style={[styles.itemInfoText, styles.itemInfoTextCur]}>{item.remanent_weight}吨</Text>
                    </View>
                    <View style={[Predefine.RCS, styles.itemInfoView]}>
                        <Text style={styles.itemInfoText}>运费单价：</Text>
                        <Text style={[styles.itemInfoText, styles.itemInfoTextCur]}>
                            {
                                (
                                    item.receipt_type == 1 // 结算对象。1：承运商，2：平台
                                    ?   item.shipper_freight_price
                                    :   item.platform_operate_mode == 1 // 平台发布方式. // 1：竞价发布 // 2：定价发布
                                        ? '未输入'
                                        : item.platform_freight_price
                                )
                            }
                        </Text>
                        <View style={styles.itemTagStyle}>
                            <Text style={styles.itemTagTitleStyle}>{item.platform_operate_mode == 1 ? '竞价发布' : '定价发布'}</Text>
                        </View>
                    </View>
                    <View style={[Predefine.RCS, styles.itemInfoView]}>
                        <Text style={styles.itemInfoText}>车辆类型：</Text>
                        <Text style={styles.itemInfoText} numberOfLines={1}>{item.vehicle_type_text}</Text>
                    </View>
                    <View style={[Predefine.RCS, styles.itemInfoView]}>
                        <Text style={styles.itemInfoText}>装货时间：</Text>
                        <Text style={styles.itemInfoText}>{item.loading_time}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
    },
    itemTitleView: {
        flex: 1,
    },
    itemTitleStyle: {
        fontSize: 15,
        color: '#fff',
        paddingVertical: 3,
        paddingHorizontal: 6,
        backgroundColor: Predefine.themeColor,
        maxWidth: Predefine.screenWidth - 200,
    },
    itemPubTimeStyle: {
        fontSize: 14,
        color: '#999',
    },
    
    itemAddressView: {
        marginVertical: 10,
    },
    itemAddressItemView: {
    },
    addressIconViewStyle: {
        width: 30,
        height: 30,
    },
    addressIconStyle: {
        width: 8,
        height: 8,
        borderRadius: 4,
        overflow: 'hidden',
        backgroundColor: Predefine.themeColor,
    },
    addressArrowIconStyle: {
        width: 30,
        height: 15,
        fontSize: 15,
        overflow: 'hidden',
        textAlign: 'center',
        color: Predefine.themeColor,
    },
    addressIconCurStyle: {
        backgroundColor: Predefine.warnColor,
    },
    addressTitleStyle: {
        fontSize: 15,
        color: '#333',
        fontWeight: '700',
    },
   
    itemDetailContent: {

    },
    itemInfoView: {
        marginTop: 10,
    },
    itemInfoText: {
        fontSize: 14,
        color: '#999',
        lineHeight: 20,
    },
    itemInfoTextCur: {
        fontWeight: '700',
        color: Predefine.orangeColor,
    },
    itemTagStyle: {
        height: 16,
        borderRadius: 8,
        marginHorizontal: 5,
        paddingHorizontal: 6,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#007aff',
    },
    itemTagTitleStyle: {
        fontSize: 12,
        color: '#fff',
    },
});