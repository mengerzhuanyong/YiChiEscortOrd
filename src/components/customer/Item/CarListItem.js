/**
 * 易驰护运 - NewsItem
 * http://menger.me
 * 行情资讯的单条
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';

export default class CarListItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

    };

    render() {
        let {item, onRefresh} = this.props;
        return (
            <TouchableOpacity style={styles.item} onPress={() => RouterHelper.navigate('修改车辆', 'EditCar', {item, onRefresh})}>
                <View style={styles.itemLine}>
                    <View style={styles.ball}/>
                    <Text style={styles.itemTitle} numberOfLines={1}>{item.status_text}</Text>
                </View>
                <View style={styles.itemLine}>
                    <Text style={styles.itemText} numberOfLines={1}>车牌号：{item.vehicle_number}</Text>
                    <Text style={styles.itemText} numberOfLines={1}>司机姓名： {item.driver_name}</Text>
                </View>
                <View style={styles.itemLine}>
                    <Text style={styles.itemText} numberOfLines={1}>车辆类型：{item.vehicle_type_text}</Text>
                    <Text style={styles.itemText} numberOfLines={1}>车辆载重：<Text style={{color: '#FF651D'}}>{item.vehicle_weight}</Text>吨</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    item: {
        justifyContent: 'space-between',
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        marginHorizontal: 15,
        marginTop: 10,
        padding: 10,
        borderRadius: 5,
        height: 84,
    },
    itemLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    ball: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginRight: 6,
        backgroundColor: DominantColor,
    },
    itemTitle: {
        flex: 1,
        color: DominantColor,
        fontSize: 12,
    },
    itemText: {
        flex: 1,
        fontSize: 12,
        color: '#333',
    },
});