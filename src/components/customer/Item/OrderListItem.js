/**
 * 易驰护运 - OrderListItem
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    DeviceEventEmitter
} from 'react-native';
import {
    AddressInfo, // 封装的组件
} from '../../../components'

export default class OrderListItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    carList = (data) => {
        let contain = data.map((item, index) => {
            if (index > 4) {
                return null;
            }
            return (
                <TouchableOpacity
                    key={index}
                    style={styles.carLine}
                    onPress={() => {
                        if (item.vehicle_order_id !== 0) {
                            RouterHelper.navigate('', 'OrderInTransitDetail', {
                                type: 'orderkistitem',
                                item: {id: item.vehicle_order_id}
                            })
                        }
                    }}
                >
                    <Text style={styles.carLineText} numberOfLines={1}>{item.vehicle_number}</Text>
                    <Text style={[styles.carLineText, {color: DominantColor}]} numberOfLines={1}>{item.status_text}</Text>
                </TouchableOpacity>
            )
        })
        return contain
    }

    render() {
        let {listItem, onRefresh, id} = this.props;
        let orderStatus = ''
        if (listItem.receipt_type === 1) { // 没转运
            if (listItem.service_pay_status === 0) {
                orderStatus = listItem.service_pay_status_text
            } else if (listItem.service_pay_status === 1) {
                if (listItem.is_pass == -1 || listItem.is_pass == 0 || listItem.is_pass == 10 || listItem.is_pass == 20 || listItem.is_pass == 30) {
                    orderStatus = listItem.is_pass_text
                } else {
                    orderStatus = listItem.carrier_order_status_text
                }
            }
        } else if (listItem.receipt_type === 2) { // 转运
            if (listItem.platform_operate_mode === 1) { // 竞价单
                if (listItem.is_pass == -1 || listItem.is_pass == 0 || listItem.is_pass == 10 || listItem.is_pass == 20 || listItem.is_pass == 30) {
                    orderStatus = listItem.is_pass_text
                } else if (listItem.is_pass == 2) { // 竞价单通过后，需要承运商缴纳保证金
                    if (listItem.service_pay_status === 0) { // 没交保证金
                        orderStatus = listItem.service_pay_status_text
                    } else if (listItem.service_pay_status === 1) { // 缴纳保证金
                        orderStatus = listItem.carrier_order_status_text
                    }
                } else if (listItem.is_pass == 1) { // 已通过（竞价单不会出现已通过状态）
                    orderStatus = listItem.carrier_order_status_text
                }
            } else if (listItem.platform_operate_mode === 2) { // 定价单
                if (listItem.service_pay_status === 0) {
                    orderStatus = listItem.service_pay_status_text
                } else {
                    if (listItem.is_pass == -1 || listItem.is_pass == 0 || listItem.is_pass == 10 || listItem.is_pass == 20 || listItem.is_pass == 30) {
                        orderStatus = listItem.is_pass_text
                    } else {
                        orderStatus = listItem.carrier_order_status_text
                    }
                }
            }
        }
        if (id == 3) {
            return (
                <TouchableOpacity 
                    style={styles.item}
                    onPress={() => RouterHelper.navigate('', 'OrderDetail', {
                        item: listItem,
                        onRefresh,
                        orderStatus
                    })}
                >
                    <View style={styles.itemHeader}>
                        <Text style={styles.itemHeaderLeft}>发布时间: {listItem.create_time}</Text>
                        <Text style={styles.itemHeaderRight} numberOfLines={1}>{orderStatus}</Text>
                        {listItem.carrier_is_read === 0 && <View style={styles.unread}/>}
                    </View>
                    <View style={styles.itemCenter}>
                        <AddressInfo item={listItem} style={styles.addressInfoStyle}/>
                    </View>
                    {this.carList(listItem.vehicle_list)}
                    {
                        listItem.vehicle_list.length > 5 &&
                        <Text style={styles.more} onPress={() => RouterHelper.navigate('', 'OrderMoreCars', {data: listItem.vehicle_list})}>查看更多车辆</Text>
                    }
                </TouchableOpacity>
            );
        } else {
            return (
                <TouchableOpacity 
                    style={styles.item} 
                    onPress={() => RouterHelper.navigate('', 'OrderDetail', {
                        item: listItem,
                        onRefresh,
                        orderStatus
                    })}
                >
                    <View style={styles.itemHeader}>
                        <Text style={styles.itemHeaderLeft}>发布时间: {listItem.create_time}</Text>
                        <Text style={styles.itemHeaderRight} numberOfLines={1}>{orderStatus}</Text>
                        {listItem.carrier_is_read === 0 && <View style={styles.unread}/>}
                    </View>
                    <View style={styles.itemCenter}>
                        <AddressInfo item={listItem} style={styles.addressInfoStyle}/>
                    </View>
                    <ImageBackground style={styles.itemFooter} source={Images.img_driver_card_bg}>
                        <View style={styles.itemFooterLine}>
                            <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>物品名称：</Text><Text style={{color: '#00BFCC', fontWeight: 'bold'}}>{listItem.goods_name}</Text></Text>
                            <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>运输吨数：</Text><Text style={{color: '#FF651D'}}>{listItem.goods_weight}</Text>吨</Text>
                        </View>
                        <View style={styles.itemFooterLine}>
                            <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>排放量：</Text>{listItem.vehicle_discharge_text}</Text>
                        </View>
                        <View style={styles.itemFooterLine}>
                            <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>装货时间：</Text>{listItem.loading_time}至{listItem.loading_end_time}</Text>
                        </View>
                        <View style={styles.itemFooterLine}>
                            <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>车辆类型：</Text>{listItem.vehicle_type_text}</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
            );
        }
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    item: {
        marginTop: 10,
        backgroundColor: '#fff',
    },

    unread: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: 'red',
        position: 'absolute',
        top: 4,
        right: 10,
    },
    itemHeader: {
        height: 50,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemHeaderLeft: {
        color: '#999',
        fontSize: 14,
    },
    itemHeaderRight: {
        color: DominantColor,
        fontSize: 14,
        paddingHorizontal: 6,
        paddingVertical: 3,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#DBF9F8',
    },
    itemCenter: {
        paddingHorizontal: 15,
    },
    itemCenterLine: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemCenterLineLeft: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
    },
    itemCenterLineText: {
        flex: 1,
        color: '#333',
        fontSize: 14,
        fontWeight: 'bold',
    },
    itemCenterImg: {
        width: 5,
        height: 12,
        marginVertical: 5,
        marginLeft: 2,
    },
    itemFooter: {
        padding: 10,
        marginHorizontal: 15,
        marginBottom: 15,
        borderRadius: 5,
        overflow: 'hidden',
    },
    itemFooterLine: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 30,
    },
    itemFooterLineItem: {
        flex: 1,
        fontSize: 14,
        color: '#666',
    },

    carLine: {
        borderWidth: 1,
        borderColor: DominantColor,
        borderRadius: 5,
        height: 30,
        marginHorizontal: 15,
        marginBottom: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carLineText: {
        fontSize: 13,
        color: '#333',
    },
    more: {
        height: 30,
        lineHeight: 30,
        textAlign: 'right',
        marginHorizontal: 15,
        marginBottom: 10,
        color: DominantColor,
    },
});