/**
 * 易驰护运 - DriverOrderListItem
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import {
    AddressInfo, // 封装的组件
} from '../../../components'

export default class SupplyListItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {

        };
    }

    render() {
        let {loginStore, item, onRefresh} = this.props;
        let {userInfo} = loginStore;
        let operate_mode = '', payment_type = '';
        if ( item.receipt_type == 1 ) {
            operate_mode = item.shipper_operate_mode == 1 ? '竞价发布' : '定价发布'
            payment_type = item.shipper_payment_type_text
        } else {
            operate_mode = item.platform_operate_mode == 1 ? '竞价发布' : '定价发布'
            payment_type = item.platform_payment_type_text
        };
        return (
            <TouchableOpacity style={styles.item} onPress={() => RouterHelper.navigate('', 'SupplyDetail', {item, onRefresh, payType: operate_mode})}>
                <View style={styles.itemHeader}>
                    <Text style={styles.itemHeaderLeft}>发布时间: {item.create_time}</Text>
                    <Text style={styles.itemHeaderRight}>{operate_mode} - {item.receipt_type == 1 ? '承运商' : '平台'}</Text>
                </View>
                <AddressInfo item={item} style={styles.addressInfoStyle} />
                <ImageBackground style={styles.itemFooter} source={Images.img_driver_card_bg}>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>物品名称：</Text><Text style={{color: '#00BFCC', fontWeight: 'bold'}}>{item.goods_name}</Text></Text>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>运输吨数：</Text><Text style={{color: '#FF651D'}}>{item.goods_weight}</Text>吨</Text>
                    </View>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>剩余吨数：</Text><Text style={{color: '#FF651D'}}>{item.remanent_weight}</Text>吨</Text>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>运输单价：</Text><Text style={{color: '#FF1D1D'}}>
                            ￥{item.receipt_type == 1 ? item.shipper_freight_price : item.platform_freight_price}
                        </Text>/吨</Text>
                    </View>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>排放量：</Text>{item.vehicle_discharge_text}</Text>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>支付方式：</Text>{payment_type}</Text>
                    </View>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>装货时间：</Text>{item.loading_time}</Text>
                    </View>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}><Text style={{color: '#333'}}>车辆类型：</Text>{item.vehicle_type_text}</Text>
                    </View>
                </ImageBackground>
                <View style={styles.btnBox}>
                    {
                        userInfo.id
                        ?
                        (
                            userInfo.is_pass == 1
                            ?
                            <Button
                                style={styles.btn}
                                title={'申请接单'}
                                titleStyle={styles.btnTitle}
                                resizeMode={'cover'}
                                backgroundImage={Images.img_btn_bg1}
                                onPress={() => RouterHelper.navigate('', 'SupplyApply', {item, payType: operate_mode})}
                            />
                            :
                            <Button
                                style={styles.btn}
                                title={'认证通过的用户才能申请接单'}
                                titleStyle={styles.btnTitle}
                                resizeMode={'cover'}
                                backgroundImage={Images.img_btn_bg1}
                                onPress={() => RouterHelper.navigate('', 'ApproveChoice')}
                            />
                        )
                        :
                        <Button
                            style={styles.btn}
                            title={'承运'}
                            titleStyle={styles.btnTitle}
                            resizeMode={'cover'}
                            backgroundImage={Images.img_btn_bg1}
                            onPress={() => RouterHelper.navigate('', 'Login')}
                        />
                    }
                </View>
            </TouchableOpacity>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    item: {
        paddingBottom: 15,
        marginBottom: 10,
        backgroundColor: '#fff',
    },

    itemHeader: {
        height: 50,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemHeaderLeft: {
        color: '#666',
        fontSize: 14,
    },
    itemHeaderRight: {
        color: DominantColor,
        fontSize: 14,
        paddingHorizontal: 6,
        paddingVertical: 3,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#DBF9F8',
    },

    addressInfoStyle: {
        marginHorizontal: 15,
    },
    itemFooter: {
        padding: 10,
        marginHorizontal: 15,
        borderRadius: 5,
        overflow: 'hidden',
    },
    itemFooterLine: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 30,
    },
    itemFooterLineItem: {
        flex: 1,
        fontSize: 14,
        color: '#666',
    },

    btnBox: {

    },
    btn: {
        height: 35,
        backgroundColor: DominantColor,
        paddingHorizontal: 0,
        paddingVertical: 0,
        marginHorizontal: 30,
        marginTop: 15,
    },
    btnTitle: {
        fontSize: 15,
    },
});