/**
 * 易驰护运 - DriverOrderListItem
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import {
    AddressInfo, // 封装的组件
} from '../../../components'

export default class DriverOrderListItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let {listItem, onRefresh, status} = this.props;
        return (
            <TouchableOpacity style={styles.item} onPress={() => RouterHelper.navigate('', 'DriverOrderDetail', {item: listItem, status, onRefresh})}>
                <View style={styles.itemHeader}>
                    <Text style={styles.itemHeaderLeft}>发布时间: {listItem.create_time}</Text>
                    <Text style={styles.itemHeaderRight} numberOfLines={1}>{status.name}</Text>
                </View>
                <View style={styles.itemCenter}>
                    <AddressInfo item={listItem} style={styles.addressInfoStyle}/>
                </View>
                <ImageBackground style={styles.itemFooter} source={Images.img_driver_card_bg}>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}>物品名称： <Text style={{color: '#00BFCC', fontWeight: 'bold'}}>{listItem.goods_name}</Text></Text>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}>运输吨数： <Text style={{color: '#FF651D'}}>{listItem.goods_weight}</Text>吨</Text>
                    </View>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}>排放量：{listItem.vehicle_discharge_text}</Text>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}>装货时间：{listItem.loading_time}</Text>
                    </View>
                    <View style={styles.itemFooterLine}>
                        <Text style={styles.itemFooterLineItem} numberOfLines={1}>车辆类型：{listItem.vehicle_type_text}</Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    item: {
        marginBottom: 10,
        backgroundColor: '#fff',
    },

    itemHeader: {
        height: 40,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemHeaderLeft: {
        color: '#999',
        fontSize: 12,
    },
    itemHeaderRight: {
        color: DominantColor,
        fontSize: 12,
        paddingHorizontal: 6,
        paddingVertical: 3,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#DBF9F8',
    },
    itemCenter: {
        paddingHorizontal: 15,
    },
    itemCenterLine: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemCenterLineLeft: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
    },
    itemCenterLineText: {
        flex: 1,
        color: '#333',
        fontSize: 12,
        fontWeight: 'bold',
    },
    itemCenterImg: {
        width: 5,
        height: 12,
        marginVertical: 5,
        marginLeft: 2,
    },
    itemFooter: {
        padding: 10,
        marginHorizontal: 15,
        marginBottom: 15,
        borderRadius: 5,
        overflow: 'hidden',
    },
    itemFooterLine: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 25,
    },
    itemFooterLineItem: {
        flex: 1,
        fontSize: 12,
        color: '#666',
    },
});