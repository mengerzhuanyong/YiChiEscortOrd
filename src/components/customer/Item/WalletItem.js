/**
 * 易驰护运 - WalletItem
 * http://menger.me
 * 钱包交易记录
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';

export default class WalletItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

    };

    render() {
        let {item} = this.props; 
        return (
            <TouchableOpacity style={styles.item}>
                <View style={styles.itemLeft}>
                    <Text style={styles.itemTitle} numberOfLines={1}>{item.type_text}</Text>
                    {
                        item.type == 12 &&
                        <Text style={styles.itemText}>
                            <Text style={[styles.reasons, item.is_pass == 1 ? {color: DominantColor} : {color: '#EE0000'}]}>{item.is_pass_text}</Text>
                            {item.reasons}
                        </Text>    
                    }
                    <Text style={styles.itemTime} numberOfLines={1}>{item.create_time}</Text>
                </View>
                <Text style={styles.itemPrice} numberOfLines={1}>￥{item.money || 0}</Text>
            </TouchableOpacity>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    itemLeft: {
        flex: 1,
    },
    itemTitle: {
        fontSize: 14,
        color: '#333',
        marginBottom: 8,
    },
    itemText: {
        fontSize: 13,
        color: '#666',
        marginBottom: 8,
    },
    itemTime: {
        fontSize: 12,
        color: '#999',
    },
    reasons: {
        lineHeight: 16,
    },
    itemPrice: {
        fontSize: 12,
        color: '#666',
    },
});