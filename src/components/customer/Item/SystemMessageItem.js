/**
 * 易驰护运 - SystemMessageItem
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'


export default class SystemMessageItem extends React.PureComponent {

    static defaultProps = {
        item: {},
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {

    }

    render() {
        let {item, style, onPress} = this.props;
        return (
            <TouchableOpacity
                style={[styles.container, style]}
                onPress={onPress}
            >
                { item.is_read == 0 && <View style={styles.unread}></View> }
                <View style={[Predefine.RCB, styles.itemTitleView]}>
                    <View style={[Predefine.RCS, styles.itemTitleLeftView]}>
                        <ImageView
                            style={styles.itemIconStyle}
                            source={item.type === 1 ? Images.icon_notice_horn : Images.icon_notice_bell}
                        />
                        <Text style={styles.itemTitleStyle} numberOfLines={1}>{item.title}</Text>
                    </View>
                    <Text style={styles.itemTimeStyle}>{item.create_time}</Text>
                </View>
                <Text style={styles.itemContextStyle}>{item.content}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 15,
        borderRadius: 8,
        paddingVertical: 10,
        marginHorizontal: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
    },
    unread: {
        position: 'absolute',
        right: 10,
        top: 10,
        height: 8,
        width: 8,
        borderRadius: 4,
        backgroundColor: '#FF0000',
    },
    itemIconStyle: {
        width: 30,
        height: 30,
        marginRight: 5,
        borderRadius: 15,
    },
    itemContentStyle: {
        flex: 1,
        marginLeft: 15,
    },
    itemTitleView: {
    },
    itemTitleLeftView: {
        flex: 1,
    },
    itemTitleStyle: {
        flex: 1,
        marginRight: 10,
        fontSize: 16,
        color: '#333',
    },
    itemTimeStyle: {
        fontSize: 13,
        color: '#666',
    },
    itemContextStyle: {
        fontSize: 13,
        color: '#666',
        marginTop: 10,
        lineHeight: 18,
        textAlign: 'justify',
    },
});