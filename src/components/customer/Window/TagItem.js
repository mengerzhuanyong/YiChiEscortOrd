/**
 * 易驰护运 - TagItem
 * http://menger.me
 * @huanhuan
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

import { IconFont } from '../../default/IconFont'

export default class Window extends React.PureComponent {

    static defaultProps = {
        title: '',
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {}

    render() {
        let {title, style, titleStyle, onPress} = this.props;
        return (
            <View style={[Predefine.CCC, styles.container, style]}>
                <Text style={[styles.titleStyle, titleStyle]}>{title}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 5,
        borderRadius: 10,
        paddingVertical: 3,
        paddingHorizontal: 6,
        backgroundColor: Predefine.themeColor,
    },
    titleStyle: {
        fontSize: 11,
        color: '#fff',
    },
});