/**
 * 易驰护运 - SupplyList
 * http://menger.me
 * 货源大厅的列表
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import SupplyListItem from '../Item/SupplyListItem';
import LoadingHint from '../../default/Loading/LoadingHint';

export default class SupplyList extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            dataSource: [],

            vehicleDischargeArray: [],
            AppPlatformPaymentTypeArray: [],
        };
        this.page = 1;
        this.limit = 10;
    }

    componentDidMount() {
        this.requestData();
        this.requestSetupConfig();
    };

    requestSetupConfig = async() => {
        let url = ServicesApi.SETUP_CONFIG_LIST;
        let data = {};
        const result = await Services.post(url, data);
        if (result.code === StatusCode.SUCCESS_CODE) {
            this.setState({
                vehicleDischargeArray: result.data.vehicle_discharge_text, // 车辆排放类型
                AppPlatformPaymentTypeArray: result.data.app_platform_payment_type_text, // 支付方式
            });
        }
    }

    requestData = async () => { // 数据列表
        let {address, filter} = this.props;
        let {dataSource} = this.state;
        let url = ServicesApi.GOODS_GOODS_LIST;
        let data = {
            from_province: address.startAddress.startProvince,
            from_city: address.startAddress.startCity,
            from_district: address.startAddress.startDistrict,
            to_province: address.endAddress.endProvince,
            to_city: address.endAddress.endCity,
            to_district: address.endAddress.endDistrict,
            vehicle_type: filter.vehicleTypeArrayText,
            vehicle_length: filter.vehicleLengthArrayText,
            vehicle_discharge: filter.vehicleDischargeTextArrayText,
            vehicle_tank_type: filter.vehicleTankTypeArrayText,
            goods_dangerous_level: filter.goodsDangerousLevelArrayText,
            platform_payment_type: filter.appPlatformPaymentTypeTextArrayText,
            remanent_weight: filter.remanentWeightArrayText,

            // goods_reasonable_loss // 最大合理损耗
            // platform_account_period // 货物结算周期
            // platform_operate_mode // 平台 - 发布方式
            page: this.page,
            limit: this.limit,
        };
        try {
            const result = await Services.post(url, data);
            let dataSourceTemp = dataSource.slice();
            let dataList = [];
            if (result.code === StatusCode.SUCCESS_CODE) {
                dataList = result.data.dataList;
                if (parseInt(data.page) === 1) {
                    dataSourceTemp = dataList
                } else {
                    if (dataList.length !== 0) {
                        dataSourceTemp = dataSourceTemp.concat(dataList);
                    }
                }
            }
            this.setState({
                dataSource: dataSourceTemp,
                loading: false
            });
            this._onStopLoading(dataList.length < this.limit);
        } catch (error) {
            this._onStopLoading(true);
        }
    };

    _onStopLoading = (status) => {
        this.setState({ready: true});
        this._listRef && this._listRef.stopRefresh();
        this._listRef && this._listRef.stopEndReached({allLoad: status});
    };

    renderItem = ({item, index}) => {
        return (
            <SupplyListItem
                item={item}
                onRefresh={() => this._onRefresh()}
                {...this.props}
            />
        )
    };

    _captureRef = (v) => {
        this._listRef = v
    };

    _keyExtractor = (item, index) => {
        return `item_${index}`
    };

    _onRefresh = () => {
        this.page = 1;
        this.requestData()
    };

    _onEndReached = () => {
        this.page++;
        this.requestData()
    };

    renderItemSeparator = (info) => {
        return null;
    };

    render() {
        let {loading, dataSource} = this.state;
        return (
            loading == true
            ?
            <LoadingHint style={styles.loading} loading={loading}/>
            :
            <ListView
                style={styles.listView}
                initialRefresh={false}
                data={dataSource}
                ref={this._captureRef}
                onRefresh={this._onRefresh}
                renderItem={this.renderItem}
                keyExtractor={this._keyExtractor}
                onEndReached={this._onEndReached}
                ItemSeparatorComponent={this.renderItemSeparator}
            />
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    loading: {
        marginTop: - (-Predefine.statusBarHeight + Predefine.navBarHeight) - 40,
    },
});