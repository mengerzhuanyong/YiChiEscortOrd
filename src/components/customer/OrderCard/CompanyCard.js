/**
 * 易驰护运 - CompanyCard
 * http://menger.me
 * 公司信息卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    Linking,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ImageStore,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

export default class CompanyCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    // 打电话
    onMakeCall = (item) => {
        let tel = item.receipt_type == 1 
            ? item.shipper_info && item.shipper_info.mobile 
            : item.platfrom_info && item.platfrom_info.transport_shipper_mobile
        Linking.openURL('tel:' + tel).catch(e => console.log(e));
    }

    render() {
        let {item} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText} numberOfLines={1}>货主公司信息</Text>
                </View>
                <View style={styles.cardCompany}>
                    {
                        item.receipt_type == 1 // 1承运商 2平台 PS：如果是1，代表没有被转运，展示承运商信息，转运了就展示平台信息
                        ?
                        <ImageView
                            resizeMode={'cover'}
                            style={styles.cardCompanyImg}
                            source={item.shipper_info ? {uri: item.shipper_info.avatar} : Images.img_nopicture1}
                        />
                        :
                        <ImageView
                            resizeMode={'cover'}
                            style={styles.cardCompanyImg}
                            source={item.platform_info ? {uri: item.platform_info.transport_shipper_avatar} : Images.img_nopicture1}
                        />
                    }
                    <View style={styles.cardCompanyRight}>
                        <Text style={styles.cardCompanyRightTitle} numberOfLines={1}>
                            {
                                item.receipt_type == 1 
                                ? item.shipper_info && item.shipper_info.company_name 
                                : item.platform_info && item.platform_info.company_name
                            }
                        </Text>
                        <View style={styles.cardCompanyRightBottom} numberOfLines={1}>
                            <Text style={styles.cardCompanyRightBottomText} numberOfLines={1}>
                                { 
                                    item.receipt_type == 1 
                                    ? item.shipper_info && item.shipper_info.name 
                                    : item.platform_info && item.platform_info.transport_shipper_name 
                                }
                            </Text>
                            <TouchableOpacity style={styles.cardCompanyRightTag} onPress={() => this.onMakeCall(item)}>
                                <Text style={styles.cardCompanyRightTagText} numberOfLines={1}>拨打电话</Text>    
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
     // 卡片
     card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },
    // 公司小卡片
    cardCompany: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    cardCompanyImg: {
        height: 50,
        width: 50,
        borderRadius: 25,
        backgroundColor: '#e2e2e2',
        marginRight: 10,
    },
    cardCompanyRight: {
        flex: 1,
        height: 50,
        paddingVertical: 5,
        justifyContent: 'space-between',
    },
    cardCompanyRightTitle: {
        color: DominantColor,
        fontSize: 15,
        flex: 1,
    },
    cardCompanyRightLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 20,
        marginBottom: 10,
    },
    cardCompanyRightLineLeft: {
        fontSize: 14,
        color: '#333',
    },
    cardCompanyRightLineRight: {
        fontSize: 14,
        color: '#666',
    },
    cardCompanyRightBottom: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    cardCompanyRightBottomText: {
        fontSize: 15,
        color: '#333',
        maxWidth: Predefine.screenWidth - 145,
    },
    cardCompanyRightTag: {
        backgroundColor: '#3984FF',
        paddingVertical: 1,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 13,
        marginLeft: 6,
    },
    cardCompanyRightTagText: {
        color: '#fff',
        fontSize: 12,
    },
});