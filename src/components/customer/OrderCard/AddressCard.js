/**
 * 易驰护运 - OrderCard
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

export default class AddressCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let {item} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText} numberOfLines={1}>详细地址</Text>
                </View>
                <View style={styles.cardLineBox}>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>姓名：</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}>{item.from_contact}</Text>
                    </View>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>联系方式：</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}>{item.from_mobile}</Text>
                    </View>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>地点名称：</Text>
                        <Text style={[styles.cardLineRight, {color: '#00BFCC'}]} numberOfLines={1}>{item.from_company}</Text>
                    </View>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>详细地址：</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}>{item.from_province+item.from_city+item.from_district+item.from_address}</Text>
                    </View>
                </View>
                <View style={styles.cardLineBox}>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>姓名：</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}>{item.to_contact}</Text>
                    </View>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>联系方式：</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}>{item.to_mobile}</Text>
                    </View>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>地点名称：</Text>
                        <Text style={[styles.cardLineRight, {color: '#00BFCC'}]} numberOfLines={1}>{item.to_company}</Text>
                    </View>
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>详细地址：</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}>{item.to_province+item.to_city+item.to_district+item.to_address}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLineBox: {
        borderTopWidth: Predefine.minPixel,
        borderTopColor: '#F0F0F0',
    },
    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
    }, 
});