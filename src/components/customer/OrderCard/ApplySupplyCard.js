/**
 * 易驰护运 - ApplySupplyCard
 * http://menger.me
 * 接单统计卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

export default class ApplySupplyCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let {item} = this.props;
        let percentage = item.percentage || 0;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText}>接单总计</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>预计承接总吨数：</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}><Text style={{color: '#FF651D'}}>{item.weight}</Text>吨</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>运费单价：</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}><Text style={{color: '#E71426'}}>￥{item.price}</Text>/吨</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>运输距离：</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.distance}km</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>总运费预计：</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}><Text style={{color: '#E71426'}}>￥{item.total}</Text></Text>
                </View>
                {
                    item.showPercentage == true &&
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft}>需支付服务费用：</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}><Text style={{color: '#E71426'}}>￥{item.percentage.toFixed(2)}</Text></Text>
                    </View>
                }
                <View style={styles.prompt}>
                    <ImageView
                        resizeMode={'contain'}
                        style={styles.promptImg}
                        source={Images.icon_warring_logo1}
                    />
                    <Text style={styles.promptText} numberOfLines={1}>温馨提示：最终运费将根据实际承运吨数来计算</Text>
                </View>
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
    }, 

    prompt: {
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    promptImg: {
        width: 12,
        height: 12,
        marginRight: 6,
    },
    promptText: {
        color: '#999',
        fontSize: 14,
    },
});