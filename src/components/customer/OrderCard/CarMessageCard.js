/**
 * 易驰护运 - CarMessageCard
 * http://menger.me
 * 运输信息
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {
    TagItem,
} from '../../../components'
import {Predefine} from '../../../config/predefine'

export default class CarMessageCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    componentDidMount() {
        
    };

    _renderCarTypeComponent1 = (item) => {
        if (!item.vehicle_type_text || item.vehicle_type_text == "") return null
        let arr = item.vehicle_type_text.split("|")
        let contain = arr.map((item, index) => {
            return <TagItem key={index} title={item} style={styles.detailTagItem} titleStyle={styles.detailTagItemTitle}/>
        })
        return <View style={[Predefine.WARP, Predefine.RCE, styles.detailTagView]}>{contain}</View>
    };

    _renderCarTypeComponent2 = (item) => {
        if (!item.vehicle_tank_type_text || item.vehicle_tank_type_text == "") return null
        let arr = item.vehicle_tank_type_text.split("|")
        let contain = arr.map((item, index) => {
            return <TagItem key={index} title={item} style={styles.detailTagItem} titleStyle={styles.detailTagItemTitle}/>
        })
        return <View style={[Predefine.WARP, Predefine.RCE, styles.detailTagView]}>{contain}</View>
    };

    _renderCarTypeComponent3 = (item) => {
        if (!item.vehicle_length_text || item.vehicle_length_text == "") return null
        let arr = item.vehicle_length_text.split("|")
        let contain = arr.map((item, index) => {
            return <TagItem key={index} title={item} style={styles.detailTagItem} titleStyle={styles.detailTagItemTitle}/>
        })
        return <View style={[Predefine.WARP, Predefine.RCE, styles.detailTagView]}>{contain}</View>
    };

    render() {
        let {item} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText} numberOfLines={1}>运输信息</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>车辆排放标准:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.vehicle_discharge_text}</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>最大合理损耗:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.goods_reasonable_loss_text}</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>车辆类型:</Text>
                    {this._renderCarTypeComponent1(item)}
                </View>
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
     // 卡片
     card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 6,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
    },
    detailTagView: {
        flex: 1,
        flexWrap: 'wrap',
        marginTop: -5,
    },
    detailTagItem: {
        marginBottom: 5,
    },
});