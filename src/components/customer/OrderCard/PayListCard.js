/**
 * 易驰护运 - PayListCard
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

export default class PayListCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    cardListView = (dataSource) => {
        let contain = dataSource && dataSource.map((item, index) => {
            if (item.name == '3') { // 后付款
                return (
                    <TouchableOpacity
                        key={index} 
                        style={[styles.carCard, item.leisure == true && {backgroundColor: '#CAF2F7'}]}
                    >
                        <View style={styles.carCardLine}>
                            <View style={styles.carCardHeader}>
                                <View style={styles.carCardHeaderBall}/>
                                <Text style={styles.carCardHeaderText}>{item.name_text}</Text>
                            </View>    
                            <Text style={styles.carCardHeaderText}>{item.shipper_status_text}</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>预估运费金额</Text>
                            <Text style={styles.carCardLineText}>{item.estimate_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>预估装货吨数</Text>
                            <Text style={styles.carCardLineText}>{item.loading_weight}吨</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实际运费金额</Text>
                            <Text style={styles.carCardLineText}>{item.real_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实际装货吨数</Text>
                            <Text style={styles.carCardLineText}>{item.real_loading_weight}吨</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实际卸货吨数</Text>
                            <Text style={styles.carCardLineText}>{item.real_discharge_weight}吨</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>应收金额</Text>
                            <Text style={styles.carCardLineText}>{item.actual_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>货损赔偿金额</Text>
                            <Text style={styles.carCardLineText}>{item.compensate_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实收金额</Text>
                            <Text style={styles.carCardLineText}><Text style={{color: DominantColor}}>{item.actual_money}</Text>元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>支付时间</Text>
                            <Text style={styles.carCardLineText}>{item.pay_time}</Text>
                        </View>
                        {
                            item.leisure == true &&
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.carCardImg}
                                source={Images.icon_choice_car}
                            />
                        }
                    </TouchableOpacity>
                )   
            } else if (item.name == '1') { // 预付款（预付费）
                return (
                    <TouchableOpacity
                        key={index} 
                        style={[styles.carCard, item.leisure == true && {backgroundColor: '#CAF2F7'}]}
                    >
                        <View style={styles.carCardLine}>
                            <View style={styles.carCardHeader}>
                                <View style={styles.carCardHeaderBall}/>
                                <Text style={styles.carCardHeaderText}>{item.name_text}</Text>
                            </View>    
                            <Text style={styles.carCardHeaderText}>{item.shipper_status_text}</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>预估装货吨数</Text>
                            <Text style={styles.carCardLineText}>{item.loading_weight}吨</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>预估预付款金额</Text>
                            <Text style={styles.carCardLineText}>{item.estimate_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实收金额</Text>
                            <Text style={styles.carCardLineText}><Text style={{color: DominantColor}}>{item.actual_money}</Text>元</Text>
                        </View>
                        {
                            item.leisure == true &&
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.carCardImg}
                                source={Images.icon_choice_car}
                            />
                        }
                    </TouchableOpacity>
                )
            } else if (item.name == '2') { // 预付款(尾款)
                return (
                    <TouchableOpacity
                        key={index} 
                        style={[styles.carCard, item.leisure == true && {backgroundColor: '#CAF2F7'}]}
                    >
                        <View style={styles.carCardLine}>
                            <View style={styles.carCardHeader}>
                                <View style={styles.carCardHeaderBall}/>
                                <Text style={styles.carCardHeaderText}>{item.name_text}</Text>
                            </View>   
                            <Text style={styles.carCardHeaderText}>{item.shipper_status_text}</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>预估尾款金额</Text>
                            <Text style={styles.carCardLineText}>{item.estimate_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实际运费金额</Text>
                            <Text style={styles.carCardLineText}>{item.real_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实际装货吨数</Text>
                            <Text style={styles.carCardLineText}>{item.real_loading_weight}吨</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实际卸货吨数</Text>
                            <Text style={styles.carCardLineText}>{item.real_discharge_weight}吨</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>应收金额</Text>
                            <Text style={styles.carCardLineText}>{item.actual_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>货损赔偿金额</Text>
                            <Text style={styles.carCardLineText}>{item.compensate_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>退还预付款</Text>
                            <Text style={styles.carCardLineText}>{item.refund_money}元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>实收金额</Text>
                            <Text style={styles.carCardLineText}><Text style={{color: DominantColor}}>{item.actual_money}</Text>元</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText}>支付时间</Text>
                            <Text style={styles.carCardLineText}>{item.pay_time}</Text>
                        </View>
                        {
                            item.leisure == true &&
                            <ImageView
                                resizeMode={'contain'}
                                style={styles.carCardImg}
                                source={Images.icon_choice_car}
                            />
                        }
                    </TouchableOpacity>
                )
            }
        })
        return contain
    }

    render() {
        let {item} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText}>付费列表</Text>
                </View>
                {this.cardListView(item.order_pay_list)}
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    carCard: {
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        marginBottom: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    carCardHeader: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 25,
    },
    carCardHeaderBall: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
        backgroundColor: DominantColor,
    },
    carCardHeaderText: {
        color: DominantColor,
        fontSize: 14,
    },
    carCardLine: {
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineText: {
        fontSize: 14,
        color: '#333',
    },
    carCardImg: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: 28,
        height: 30,
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
    }, 
});