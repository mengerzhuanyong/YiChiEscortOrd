/**
 * 易驰护运 - EvaluateViewCard
 * http://menger.me
 * 评价卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'
import {
    IconFont,
} from '../../../components'
export default class EvaluateViewCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    iconView = (val) => {
        return (
            <View style={styles.cardLineRight}>
                <IconFont.AntDesign style={styles.icon} name={val >= 1 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 2 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 3 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 4 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 5 ? 'star' : 'staro'}/>
            </View>
        )
    }

    render() {
        let {item} = this.props;
        let shipper = item.shipper_evaluate ? item.shipper_evaluate.split(",") : null,
            carrier = item.carrier_evaluate ? item.carrier_evaluate.split(",") : null;
        let shipper_view = Array.isArray(shipper) 
            ? shipper.map((item, index) => {
                return (
                    <View style={styles.cardLine} key={index}>
                        <Text style={styles.cardLineLeft}>
                            {index === 0 && '运力保障：'}
                            {index === 1 && '运输损耗：'}
                            {index === 2 && '服务质量：'}
                            {index === 3 && '车辆状况：'}
                        </Text>
                        {this.iconView(item)}
                    </View>
                )
            })
            :
            <Text style={styles.cardNone}>暂无评价信息</Text>
        let carrier_view = Array.isArray(carrier) 
            ?
            carrier.map((item, index) => {
                return (
                    <View style={styles.cardLine} key={index}>
                        <Text style={styles.cardLineLeft}>
                            {index === 0 && '运力保障：'}
                            {index === 1 && '运输损耗：'}
                            {index === 2 && '服务质量：'}
                            {index === 3 && '车辆状况：'}
                        </Text>
                        {this.iconView(item)}
                    </View>
                )
            })
            :
            <Text style={styles.cardNone}>暂无评价信息</Text>
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText}>评价</Text>
                </View>
                {carrier_view}
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText}>货主评价</Text>
                </View>
                {shipper_view}
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        flexDirection: 'row',
    },
    icon: {
        color: DominantColor,
        fontSize: 20,
        marginRight: 10,
    },
    cardNone: {
        fontSize: 14,
        color: '#999',
        marginBottom: 15,
    },
});