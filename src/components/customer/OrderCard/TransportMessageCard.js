/**
 * 易驰护运 - ApplySupplyCard
 * http://menger.me
 * 运输信息卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

export default class TransportMessageCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        let {item} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText} numberOfLines={1}>运输信息</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>运输距离:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.distance}km</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>发布方式:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}><Text style={{color: '#00BFCC'}}>
                    {
                        item.receipt_type == 1 // 结算对象 1：承运商，2：平台
                        ?
                        item.shipper_operate_mode == 1 ? '竞价发布' : '定价发布'
                        :
                        item.platform_operate_mode == 1 ? '竞价发布' : '定价发布'
                    }
                    </Text></Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>支付方式:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}><Text style={{color: '#00BFCC'}}>
                    {
                        item.receipt_type == 1 // 结算对象 1：承运商，2：平台
                        ?
                        item.shipper_payment_type_text
                        :
                        item.platform_payment_type_text
                    }
                    </Text></Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>结算周期:</Text>
                    <Text style={styles.cardLineRight}><Text style={{color: '#00BFCC'}}>
                    {
                        item.receipt_type == 1 // 结算对象 1：承运商，2：平台
                        ?
                        item.shipper_account_period_text
                        :
                        item.platform_account_period_text
                    }   
                    </Text></Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>结算对象:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.receipt_type == 1 ? '承运商' : '平台'}</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>运费单价:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>
                        {
                            item.receipt_type == 1 // 结算对象 1：承运商，2：平台
                            ?
                            item.shipper_freight_price
                            :
                            item.platform_freight_price
                        }
                        元/吨
                    </Text>
                </View>
                {
                    item.platform_oil_card_ratio_text !== '' &&
                    <View style={styles.cardLine}>
                        <Text style={styles.cardLineLeft} numberOfLines={1}>油卡比例:</Text>
                        <Text style={styles.cardLineRight} numberOfLines={1}>{item.platform_oil_card_ratio_text}</Text>
                    </View>    
                }
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 6,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
    },
});