/**
 * 易驰护运 - OrderCard
 * http://menger.me
 * 时间轴
 * @桓桓
 */

'use strict';

import React, { version } from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import { LargePicture } from '../../../components';

export default class TimeCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    imageListView = (data) => {
        let imageDataSource = data.weigh_img.split('|');
        let contain = Array.isArray(imageDataSource) && imageDataSource.map((item, index) => {
            return (
                <LargePicture
                    key={index}
                    resizeMode={'cover'}
                    style={styles.statusImg}
                    imageStyle={styles.statusImg}
                    source={{uri: item}}
                />
            ) 
        })
        return (
            <ScrollView 
                style={styles.statusImgBox} 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
                {contain}
            </ScrollView>
        )
    }

    cardListView = (data) => {
        let contain = data.map((item, index) => {
            console.log('item------------->', item)
            return (
                item.is_complete == 0 
                ?
                <View key={index} style={[styles.cardTimeLine, index == 0 && {marginTop: 0}]}>
                    <Text style={styles.cardTimeLineDay} numberOfLines={1}>暂未到达</Text>
                    <View style={styles.cardTimeLineImgBox}>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.cardTimeLineImg}
                            source={Images.icon_time_card_logo3}
                        />
                    </View>
                    <ImageView
                        resizeMode={'contain'}
                        style={styles.cardTimeLineRightArrow}
                    />
                    <View style={styles.cardTimeLineRight}>
                        <Text style={styles.cardTimeLineRightText} numberOfLines={1}>{item.vehicle_order_log_status_text}</Text>
                    </View>
                </View>
                :
                <View key={index} style={[styles.cardTimeLine, index == 0 && {marginTop: 0}]}>
                    <Text style={styles.cardTimeLineDayCur} numberOfLines={2}>{item.update_time}</Text>
                    <View style={styles.cardTimeLineImgBoxCur}>
                        <ImageView
                            resizeMode={'contain'}
                            style={styles.cardTimeLineImgCur}
                            source={Images.icon_time_card_logo1}
                        />
                    </View>
                    <ImageView
                        resizeMode={'contain'}
                        style={styles.cardTimeLineRightArrow}
                        source={Images.icon_time_card_logo2}
                    />
                    <View style={[styles.cardTimeLineRightCur]}>
                        <Text style={styles.cardTimeLineRightTextCur} numberOfLines={1}>{item.vehicle_order_log_status_text}</Text>
                        {
                            (item.type == 1 || item.type == 2) &&
                            <View>
                                {this.imageListView(item)}
                                <View style={styles.smallStatusTextBox}>
                                    <Text style={styles.statusText} numberOfLines={1}>{item.is_pass_text}</Text>
                                </View>
                                <Text style={styles.statusWeigh}>装货吨数:<Text style={{color: '#FC6911'}}>{item.weigh}</Text>吨</Text>
                                {item.reasons && <Text style={styles.reasons}>原因：{item.reasons}</Text>}
                            </View>
                        }
                    </View>
                </View>
            )
        })
        return (
            <View style={styles.cardTime}>
                <View style={styles.line}/>
                {contain}
            </View>
        )
    }

    render() {
        let {item} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText} numberOfLines={1}>时间轴</Text>
                </View>
                {this.cardListView(item)}
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        overflow: 'hidden',
        paddingBottom: 10,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },


    cardTime: {
        overflow: 'hidden',
    },
    line: {
        width: 1,
        backgroundColor: '#DCDCDC',
        height: '100%',
        position: 'absolute',
        left: 105,
        top: 0,
        zIndex: -1,
    },
    cardTimeLine: {
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: '#fff',
        marginTop: 10,
    },
    cardTimeLineDay: {
        width: 80,
        textAlign: 'center',
        fontSize: 14,
        color: '#999',
    },
    cardTimeLineDayCur: {
        width: 80,
        textAlign: 'center',
        fontSize: 14,
        color: '#666',
    },
    cardTimeLineImgBoxCur: {
        backgroundColor: '#fff',
        height: 30,
        width: 30,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardTimeLineImgCur: {
        width: 24,
        height: 24,
    },
    cardTimeLineImgBox: {
        backgroundColor: '#fff',
        height: 20,
        width: 30,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardTimeLineImg: {
        width: 10,
        height: 10,
    },
    cardTimeLineRightArrow: {
        width: 10,
        marginRight: -3,
    },
    cardTimeLineRight: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        padding: 10,
        borderRadius: 5,
        marginLeft: 7,
    },
    cardTimeLineRightText: {
        color: '#999',
        fontSize: 14,
    },
    cardTimeLineRightCur: {
        flex: 1,
        backgroundColor: DominantColor,
        padding: 10,
        borderRadius: 5,
    },
    cardTimeLineRightTextCur: {
        color: '#fff',
        fontSize: 14,
    },

    statusImgBox: {
        marginTop: 10,
    },
    statusImg: {
        width: 60,
        height: 60,
        marginRight: 10,
    },
    
    smallStatusTextBox: {
        marginTop: 10,
        width: 60,
        alignItems: 'center',
        padding: 4,
        borderRadius: 3,
        backgroundColor: '#82DFE4',
    },
    statusText: {
        color: '#fff',
        fontSize: 14,
    },

    statusWeigh: {
        marginTop: 10,
        fontSize: 15,
        color: '#333',
    },

    reasons: {
        marginTop: 10,
        fontSize: 14,
        color: '#666',
        lineHeight: 18,
    },
});