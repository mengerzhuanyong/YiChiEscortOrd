/**
 * 易驰护运 - EvaluateCard
 * http://menger.me
 * 评价卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'
import {
    IconFont,
} from '../../../components'
export default class EvaluateCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    iconView = (val) => {
        return (
            <View style={styles.cardLineRight}>
                <IconFont.AntDesign style={styles.icon} name={val >= 1 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 2 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 3 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 4 ? 'star' : 'staro'}/>
                <IconFont.AntDesign style={styles.icon} name={val >= 5 ? 'star' : 'staro'}/>
            </View>
        )
    }

    render() {
        let {item} = this.props;
        let star1 = item.shipper_evaluate && item.shipper_evaluate.split(",")[0];
        let star2 = item.shipper_evaluate && item.shipper_evaluate.split(",")[1];
        let star3 = item.shipper_evaluate && item.shipper_evaluate.split(",")[2];
        let star4 = item.shipper_evaluate && item.shipper_evaluate.split(",")[3];
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText}>评价</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>运力保障：</Text>
                    {this.iconView(star1)}
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>运输损耗：</Text>
                    {this.iconView(star2)}
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>服务质量：</Text>
                    {this.iconView(star3)}
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>车辆状况：</Text>
                    {this.iconView(star4)}
                </View>
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        flexDirection: 'row',
    },
    icon: {
        color: DominantColor,
        fontSize: 20,
        marginRight: 10,
    },
});