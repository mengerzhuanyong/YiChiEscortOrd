/**
 * 易驰护运 - CarsCard
 * http://menger.me
 * 车辆列表卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

export default class CarsCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    _onRefresh = async () => {
        let {onRefresh} = this.props;
        onRefresh && onRefresh()
    };

    cardListView = (dataSource) => {
        let contain = dataSource && dataSource.map((item, index) => {
            console.log('item.change_is_pass---->', item.change_is_pass);
            if (index < 5) {
                return (
                    <TouchableOpacity 
                        key={index} 
                        style={[styles.carCard, item.leisure == true && {backgroundColor: '#CAF2F7'}]}
                        onPress={() => {
                            if (item.vehicle_order_id !== 0) {
                                RouterHelper.navigate('', 'OrderDriverDetail', {
                                    item: {id: item.vehicle_order_id}
                                })
                            }
                        }}
                    >
                        <View style={styles.carCardLine}>
                            <View style={styles.carCardHeader}>
                                <View style={styles.carCardHeaderBall}/>
                                <Text style={styles.carCardHeaderText}>{item.status_text}</Text>
                            </View>
                            <Text style={styles.s} numberOfLines={1}>{item.carrier_vehicle_arrival_time}</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText} numberOfLines={1}>车牌号：{item.vehicle_number}</Text>
                            <Text style={styles.carCardLineText} numberOfLines={1}>司机姓名：{item.driver_name}</Text>
                        </View>
                        <View style={styles.carCardLine}>
                            <Text style={styles.carCardLineText} numberOfLines={1}>车辆类型：{item.vehicle_type_text}</Text>
                            <Text style={styles.carCardLineText} numberOfLines={1}>车辆载重：{item.vehicle_weight}吨</Text>
                        </View>
                        {item.leisure == true && <ImageView
                            resizeMode={'contain'}
                            style={styles.carCardImg}
                            source={Images.icon_choice_car}
                        />}
                        {item.change_is_pass === 0 ? <Text style={styles.reduseTextStyle}>{item.change_refuse_reasons}</Text> : null}
                        {item.cancel_is_pass === 0 ? <Text style={styles.reduseTextStyle}>{item.cancel_refuse_reasons}</Text> : null}
                    </TouchableOpacity>
                )
            }
        })
        return contain
    }

    render() {
        let {item, dataSource} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText}>承运车辆列表</Text>
                </View>
                {this.cardListView(item.vehicle_list)}
                {item.vehicle_list && item.vehicle_list.length > 5 && <Text style={styles.moreCar} numberOfLines={1} onPress={() => RouterHelper.navigate('', 'OrderMoreCars', {data: item.vehicle_list})}>查看更多车辆...</Text>}
                {
                    ( (item.is_pass == 1 || item.is_pass == 2 ) && item.change_button == 1 ) &&
                    <View style={styles.bottomBtnBox}>
                        <Button
                            style={[styles.bottomBtn, styles.bottomBtn1]}
                            title={'取消车辆'}
                            titleStyle={[styles.bottomBtnTitle, styles.bottomBtnTitle1]}
                            onPress={() => RouterHelper.navigate('', 'DeleteCar', {
                                item,
                                onRefresh: () => this._onRefresh()
                            })}
                        />  
                        <Button
                            title={'换车申请'}
                            style={[styles.bottomBtn, styles.bottomBtn2]}
                            titleStyle={[styles.bottomBtnTitle, styles.bottomBtnTitle2]}
                            onPress={() => RouterHelper.navigate('', 'ChangeCar', {
                                item,
                                onRefresh: () => this._onRefresh()
                            })}
                        />
                    </View>
                }
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    carCard: {
        borderWidth: Predefine.minPixel,
        borderColor: '#e1e1e1',
        borderRadius: 5,
        marginBottom: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    carCardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 25,
    },
    carCardHeaderBall: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginRight: 6,
        backgroundColor: DominantColor,
    },
    carCardHeaderText: {
        color: DominantColor,
        fontSize: 14,
    },
    carCardLine: {
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    carCardLineText: {
        flex: 1,
        fontSize: 14,
        color: '#333',
    },
    carCardImg: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: 28,
        height: 30,
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
    }, 
    moreCar: {
        fontSize: 16,
        color: DominantColor,
        marginBottom: 10,
        textAlign: 'right',
    },

    bottomBtnBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    bottomBtn: {
        // width: Predefine.screenWidth - 30,
        borderRadius: 5,
        marginBottom: 10,
        flex: 1,
    },
    bottomBtn1: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: DominantColor,
        marginRight: 10,
    },
    bottomBtn2: {
        backgroundColor: DominantColor,
    },
    bottomBtnTitle: {
        fontSize: 14,
    },
    bottomBtnTitle1: {
        color: DominantColor,
    },
    bottomBtnTitle2: {
        color: '#fff',
    },

    reduseTextStyle: {
        color: '#f00',
        marginBottom: 2,
    },

});