/**
 * 易驰护运 - EvaluateCard
 * http://menger.me
 * 评价卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'
import {
    IconFont,
} from '../../../components'
export default class EvaluateCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            Evaluate1: 3,
            Evaluate2: 3,
            Evaluate3: 3,
            Evaluate4: 3,
        };
    }

    // state变化吧星星数给父页面
    componentDidUpdate () {
        let {getEvaluate} = this.props;
        let {Evaluate1, Evaluate2, Evaluate3, Evaluate4} = this.state;
        getEvaluate && getEvaluate(Evaluate1, Evaluate2, Evaluate3, Evaluate4)
    }

    // 星星的视图
    iconView = (type, val) => {
        return (
            <View style={styles.cardLineRight}>
                <IconFont.AntDesign 
                    style={styles.icon} 
                    name={val >= 1 ? 'star' : 'staro'}
                    onPress={() => {
                        type == 1 && this.setState({Evaluate1: 1})
                        type == 2 && this.setState({Evaluate2: 1})
                        type == 3 && this.setState({Evaluate3: 1})
                        type == 4 && this.setState({Evaluate4: 1})
                    }}
                />
                <IconFont.AntDesign 
                    style={styles.icon} 
                    name={val >= 2 ? 'star' : 'staro'} 
                    onPress={() => {
                        type == 1 && this.setState({Evaluate1: 2})
                        type == 2 && this.setState({Evaluate2: 2})
                        type == 3 && this.setState({Evaluate3: 2})
                        type == 4 && this.setState({Evaluate4: 2})
                    }}
                />
                <IconFont.AntDesign 
                    style={styles.icon} 
                    name={val >= 3 ? 'star' : 'staro'} 
                    onPress={() => {
                        type == 1 && this.setState({Evaluate1: 3})
                        type == 2 && this.setState({Evaluate2: 3})
                        type == 3 && this.setState({Evaluate3: 3})
                        type == 4 && this.setState({Evaluate4: 3})
                    }}
                />
                <IconFont.AntDesign 
                    style={styles.icon} 
                    name={val >= 4 ? 'star' : 'staro'}
                    onPress={() => {
                        type == 1 && this.setState({Evaluate1: 4})
                        type == 2 && this.setState({Evaluate2: 4})
                        type == 3 && this.setState({Evaluate3: 4})
                        type == 4 && this.setState({Evaluate4: 4})
                    }}
                />
                <IconFont.AntDesign 
                    style={styles.icon} 
                    name={val >= 5 ? 'star' : 'staro'} 
                    onPress={() => {
                        type == 1 && this.setState({Evaluate1: 5})
                        type == 2 && this.setState({Evaluate2: 5})
                        type == 3 && this.setState({Evaluate3: 5})
                        type == 4 && this.setState({Evaluate4: 5})
                    }}
                />
            </View>
        )
    }

    render() {
        let {Evaluate1, Evaluate2, Evaluate3, Evaluate4} = this.state;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText}>评价</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>运力保障：</Text>
                    {this.iconView(1, Evaluate1)}
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>运输损耗：</Text>
                    {this.iconView(2, Evaluate2)}
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>服务质量：</Text>
                    {this.iconView(3, Evaluate3)}
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft}>车辆状况：</Text>
                    {this.iconView(4, Evaluate4)}
                </View>
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        flexDirection: 'row',
    },
    icon: {
        color: DominantColor,
        fontSize: 20,
        marginRight: 10,
    },
});