/**
 * 易驰护运 - CargoMessageCard
 * http://menger.me
 * 货物信息卡片
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

export default class CargoMessageCard extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        let {item} = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderBall}><View style={styles.cardHeaderBallCenter}/></View>
                    <Text style={styles.cardHeaderText} numberOfLines={1}>货物信息</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>货物密度:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.goods_density}吨/m³</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>发货总吨数:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.goods_weight}吨</Text>
                </View>
                <View style={styles.cardLine}>
                    <Text style={styles.cardLineLeft} numberOfLines={1}>货物单价:</Text>
                    <Text style={styles.cardLineRight} numberOfLines={1}>{item.goods_price}元/吨</Text>
                </View>
            </View>
        );
    }
}

const DominantColor = '#00BFCC';
const styles = StyleSheet.create({
    card: {
        marginBottom: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
    },
    cardHeaderBall: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: DominantColor,
        marginRight: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHeaderBallCenter: {
        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    cardHeaderText: {
        color: '#333',
        fontSize: 15,
        fontWeight: 'bold',
    },

    cardLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
    },
    cardLineLeft: {
        color: '#333',
        fontSize: 14,
    },
    cardLineRight: {
        color: '#666',
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
    },
});