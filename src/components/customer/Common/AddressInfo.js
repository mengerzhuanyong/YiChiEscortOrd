/**
 * 易驰护运 - AddressInfo
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    DeviceEventEmitter
} from 'react-native'
import {Predefine} from '../../../config/predefine'

import {IconFont} from '../../default/IconFont'

export default class AddressInfo extends React.PureComponent {

    static defaultProps = {
        item: '',
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    render() {
        let {item, style, onPress} = this.props;
        return (
            <View style={[styles.container, style]}>
                <View style={[Predefine.RCS, styles.itemAddressItemView]}>
                    <Text style={styles.addressIconStyle}/>
                    <Text style={styles.addressTitleStyle}
                          numberOfLines={2}>{item.from_province + item.from_city + item.from_district}</Text>
                </View>
                <IconFont.Fontisto name={'arrow-down-l'} style={[styles.addressArrowIconStyle]}/>
                <View style={[Predefine.RCB, styles.itemAddressItemView]}>
                    <View style={[Predefine.RCS]}>
                        <Text style={[styles.addressIconStyle, styles.addressIconCurStyle]}/>
                        <Text style={styles.addressTitleStyle}
                              numberOfLines={2}>{item.to_province + item.to_city + item.to_district}</Text>
                    </View>

                    {/* <Text style={{color: '#666'}}
                          onPress={() => {
                              DeviceEventEmitter.emit('Morevehicle_list', true);
                          }}
                    >更多</Text> */}


                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        alignItems: 'flex-start',
    },
    itemAddressItemView: {
        paddingRight: 25
    },
    addressIconStyle: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginRight: 6,
        overflow: 'hidden',
        backgroundColor: Predefine.themeColor,
    },
    addressArrowIconStyle: {
        fontSize: 15,
        overflow: 'hidden',
        textAlign: 'center',
        color: Predefine.themeColor,
        marginLeft: -3,
        marginVertical: 5,
    },
    addressIconCurStyle: {
        backgroundColor: Predefine.warnColor,
    },
    addressTitleStyle: {
        fontSize: 14,
        color: '#333',
        fontWeight: '700',
        flex: 1,
    },

});