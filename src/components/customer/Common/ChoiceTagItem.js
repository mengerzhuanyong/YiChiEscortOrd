/**
 * 易驰护运 - ChoiceTagItem
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {Predefine} from '../../../config/predefine'

import { IconFont } from '../../default/IconFont'

export default class ChoiceTagItem extends React.PureComponent {

    static defaultProps = {
        title: '',
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {}

    render() {
        let {title, style, titleStyle, onPress} = this.props;
        return (
            <View style={[Predefine.CCC, styles.container, style]}>
                <Text style={[styles.titleStyle, titleStyle]} numberOfLines={2}>{title}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 3,
        paddingVertical: 3,
        paddingHorizontal: 6,
        backgroundColor: '#F4F4F4',
    },
    titleStyle: {
        fontSize: 13,
        color: '#666666',
    },
});