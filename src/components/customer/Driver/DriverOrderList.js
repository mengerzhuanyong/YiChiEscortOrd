/**
 * 易驰护运 - DriverOrderList
 * http://menger.me
 * @桓桓
 */

'use strict';

import React from 'react'
import {
    Text,
    View,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import DriverOrderListItem from '../Item/DriverOrderListItem';
import LoadingHint from '../../default/Loading/LoadingHint';

export default class DriverOrderList extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            dataSource: [],
        };
        this.page = 1;
        this.limit = 10;
        this.timer1 = setInterval(() => {
            this.requestData()
        }, 30000);
    }

    componentDidMount() {
        this.requestData()
    };

    componentWillUnmount () {
        let timers = [this.timer1];
        ClearTimeOut(timers);
    }

    requestData = async () => { // 数据列表
        let {item} = this.props;
        let {dataSource} = this.state;
        let url = ServicesApi.VEHICLE_ORDER_LIST;
        let data = {
            vehicle_order_status: item.id,
            page: this.page,
            limit: this.limit,
        };
        try {
            const result = await Services.post(url, data);
            let dataSourceTemp = dataSource.slice();
            let dataList = [];
            if (result.code === StatusCode.SUCCESS_CODE) {
                dataList = result.data.dataList;
                if (parseInt(data.page) === 1) {
                    dataSourceTemp = dataList
                } else {
                    if (dataList.length !== 0) {
                        dataSourceTemp = dataSourceTemp.concat(dataList);
                    }
                }
            }
            this.setState({
                dataSource: dataSourceTemp,
                loading: false
            });
            this._onStopLoading(dataList.length < this.limit);
        } catch (error) {
            this._onStopLoading(true);
        }
    };

    _onStopLoading = (status) => {
        this.setState({ready: true});
        this._listRef && this._listRef.stopRefresh();
        this._listRef && this._listRef.stopEndReached({allLoad: status});
    };

    renderItem = ({item}) => {
        let {status} = this.props;
        return (
            <DriverOrderListItem
                listItem={item}
                status={status}
                onRefresh={() => this._onRefresh()}
                {...this.props}
            />
        )
    };

    _captureRef = (v) => {
        this._listRef = v
    };

    _keyExtractor = (item, index) => {
        return `item_${index}`
    };

    _onRefresh = () => {
        this.page = 1;
        this.requestData()
    };

    _onEndReached = () => {
        this.page++;
        this.requestData()
    };

    renderItemSeparator = (info) => {
        return null;
    };

    render() {
        let {loading, dataSource} = this.state;
        return (
            loading == true
            ?
            <LoadingHint style={styles.loading} loading={loading}/>
            :
            <ListView
                initialRefresh={false}
                data={dataSource}
                ref={this._captureRef}
                onRefresh={this._onRefresh}
                renderItem={this.renderItem}
                keyExtractor={this._keyExtractor}
                onEndReached={this._onEndReached}
                ItemSeparatorComponent={this.renderItemSeparator}
            />
        );
    }
}

const styles = StyleSheet.create({
    loading: {
        marginTop: -(Predefine.statusBarHeight + Predefine.navBarHeight),
    },
});