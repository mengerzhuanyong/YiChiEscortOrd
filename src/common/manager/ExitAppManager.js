/**
 * 悬赏任务平台 - 媒体库管理
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {
    NativeModules
} from 'react-native'

export default class ExitAppManager {
    
    static exitApp() {
        NativeModules.RNExitApp.exitApp();
    }
}